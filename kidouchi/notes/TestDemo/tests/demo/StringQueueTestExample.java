package demo;

import junit.framework.Assert;

import org.junit.Test;

public class StringQueueTestExample {
	@Test
	public void testOneTwo() {
		Queue q = new StringQueue();
		q.add("1");
		q.add("2");
		String first = q.getFirstAlphabetically();
		Assert.assertEquals("1",first);
		String second = q.getFirstAlphabetically();
		Assert.assertEquals("2",second);		
	}

	@Test
	public void testOneEmpty() {
		Queue q = new StringQueue();
		q.add("1");
		q.add("");
		String first = q.getFirstAlphabetically();
		Assert.assertEquals("",first);
		String second = q.getFirstAlphabetically();
		Assert.assertEquals("1",second);		
	}

	@Test(expected=IllegalArgumentException.class)
	public void testNull() {
		Queue q = new StringQueue();
		q.add(null);
	}

	@Test
	public void testTwoOne() {
		Queue q = new StringQueue();
		q.add("2");
		q.add("1");
		String first = q.getFirstAlphabetically();
		Assert.assertEquals("1",first);
		String second = q.getFirstAlphabetically();
		Assert.assertEquals("2",second);		
	}

	@Test
	public void testOneFourThree() {
		Queue q = new StringQueue();
		q.add("1");
		q.add("4");
		q.add("3");
		String first = q.getFirstAlphabetically();
		Assert.assertEquals("1",first);
	}

	@Test
	public void testMany() {
		Queue q = new StringQueue();
		q.add("1");
		q.add("2");
		q.add("3");
		q.add("4");
		q.add("5");
		q.add("6");
		q.add("7");
		q.add("8");
		q.add("9");
		q.add("10");
		q.add("11");
	}
	@Test
	public void testFunny() {
		Queue q = new StringQueue();
		q.add("2");
		q.add("3");
		q.add("0");
		q.add("1");
		String first = q.getFirstAlphabetically();
		Assert.assertEquals("0",first);
		String second = q.getFirstAlphabetically();
		Assert.assertEquals("1",second);		
	}
	@Test
	public void testEmpty() {
		Queue q = new StringQueue();
		String first = q.getFirstAlphabetically();
		Assert.assertEquals("1",first);
	}
}
