package demo;

import junit.framework.Assert;

import org.junit.Test;

public class StringQueueTest {
	@Test
	public void testSingleElement() {
		Queue q = new StringQueue();
		q.add("test");
		Assert.assertEquals("test",q.getFirstAlphabetically());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testNull() {
		Queue q = new StringQueue();
		q.add(null);
		Assert.assertEquals(null,q.getFirstAlphabetically());
	}
	
	@Test
	public void testEmptyString() {
		Queue q = new StringQueue();
		q.add("");
		Assert.assertEquals("",q.getFirstAlphabetically());
	}
	
	@Test(expected=IllegalStateException.class)
	public void testEmpty() {
		Queue q = new StringQueue();
		Assert.assertEquals("test",q.getFirstAlphabetically());
	}
	
	@Test
	public void testEmpty2() {
		Queue q = new StringQueue();
		try {
			q.getFirstAlphabetically();
			Assert.fail("should not be able to get an element");
		} catch (IllegalStateException e) {
		}
	}

	@Test
	public void testTwoElement() {
		Queue q = new StringQueue();
		q.add("a");
		q.add("b");
		Assert.assertEquals("a",q.getFirstAlphabetically());
		Assert.assertEquals("b",q.getFirstAlphabetically());
	}
	
	@Test
	public void testTwice() {
		Queue q = new StringQueue();
		q.add("a");
		q.add("a");
		Assert.assertEquals("a",q.getFirstAlphabetically());
		Assert.assertEquals("a",q.getFirstAlphabetically());
	}
	
	@Test
	public void testTwoElementReversed() {
		Queue q = new StringQueue();
		q.add("b");
		q.add("a");
		Assert.assertEquals("a",q.getFirstAlphabetically());
		Assert.assertEquals("b",q.getFirstAlphabetically());
	}
	

	//@Test(expected=Exception.class)
	//@Test
	public void aTest1() {
		//Assert.assertEquals(expected,result);
	}
	//@Test(expected=Exception.class)
	//@Test
	public void aTest2() {
		//Assert.assertEquals(expected,result);
	}
}
