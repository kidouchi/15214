package demo;

/** A priority queue interface */
public interface Queue {
	void add(String s);
	String getFirstAlphabetically();
}
