package demo;

/** A priority queue implementation */
public class StringQueue implements Queue {
	private final int QUEUE_SIZE = 10;
	private String[] data = new String[QUEUE_SIZE];
	
	@Override
	public void add(String s) {
		if (s == null)
			throw new IllegalArgumentException("argument may not be null");
		// empty strings go first
		if (s.length() == 0)
			data[0] = s;
		// put the string in the first spot
		for (int i = 0; i < QUEUE_SIZE; ++i) {
			if (data[i] == null) {
				data[i] = s;
				return;
			}
		}
		throw new IllegalStateException("Queue full");
	}
	
	@Override
	public String getFirstAlphabetically() {
		int candidate = -1;
		// find the smallest string
		for (int i = 0; i < QUEUE_SIZE; ++i) {
			if (data[i] != null) {
				// no candidate yet
				if (candidate == -1)
					candidate = i;
				// smaller than the previous string
				else if (i > 0 && data[i] != null && data[i-1] != null && isLess(data[i],data[i-1]))
					candidate = i;
			} else {
				break;	// didn't find it
			}
		}
		// no strings
		if (candidate == -1)
			throw new IllegalStateException("Queue empty");
		String result = data[candidate];
		data[candidate] = null;
		return result;
	}
	
	boolean isLess(String s1, String s2) {
		return s1.compareTo(s2) < 0;
	}
}
