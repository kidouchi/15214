package edu.cmu.cs.cs214.staticfield;

public class Counter {
	int count;
	static int overallCount = 0;
		
	public Counter() {
		count = 0;
	}
	
	public void increment() {
		count += 1;
		overallCount += 1;
	}
	
	public int getCount() {
		return count;
	}
	
	public int getOverallCountV1() {  // This is poor design.  This method should be static
		return overallCount;          // because it does not depend on any instance properties.
	}
	
	public static int getOverallCountV2() {
		return overallCount;
	}
}
