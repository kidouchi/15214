package edu.cmu.cs.cs214.staticfield;

public class Main {
	public static void main(String[] args) {
		Counter redFish = new Counter();
		Counter blueFish = new Counter();
		
		redFish.increment();
		redFish.increment();
		blueFish.increment();
		
		System.out.println("Red fish getCount:  " + String.valueOf(redFish.getCount()));
		System.out.println("Blue fish getCount: " + String.valueOf(blueFish.getCount()));
		System.out.println("Red fish overall:   " + String.valueOf(redFish.getOverallCountV1()));
		System.out.println("Blue fish overall:  " + String.valueOf(blueFish.getOverallCountV1()));
		System.out.println("Counter overall:    " + String.valueOf(Counter.getOverallCountV2()));
	}
}
