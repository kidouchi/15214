package edu.cmu.cs.cs214.generics;

/**
 * An interface which details methods for a generic dog.
 * 
 * @author 
 *
 */
public interface Dog {
	
	/**
	 * Returns the name of the dog
	 * 
	 * @return String The name of the dog
	 */
	public String getName();
	
	/**
	 * Sets the name of the dog.
	 * 
	 * @param name the new name
	 */
	public void setName(String name);
	
	/**
	 * returns the breed of the dog.
	 * @return String the breed of the dog.
	 */
	public String getBreed();
	
	/**
	 * The prints out the dog's bark to console.
	 */
	public void bark();
	
	/**
	 * returns a string which contains the name and the breed as follows
	 * 
	 * Breed: __ Name: __
	 * 
	 * @return
	 */
	public String toString();
	
}
