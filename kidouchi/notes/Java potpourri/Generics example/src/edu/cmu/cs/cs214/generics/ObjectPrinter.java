package edu.cmu.cs.cs214.generics;

import java.util.*;

public class ObjectPrinter<T> {
	private final List<T> items;
	
	public ObjectPrinter(List<T> items) {
		this.items = new LinkedList<T>(items);  // Defensive copy 
	}

	public void printAll() {
		for (T t : items) {
			System.out.println(t.toString());
		}
	}
	
	
	public static void main(String[] args) {
		String[] strings = {"To", "be", "or", "not", "to", "be"};
	
		List<String> stringList = new LinkedList<String>();
		for (String s : strings) 
			stringList.add(s);
	
		ObjectPrinter<String> op = new ObjectPrinter<String>(stringList);
		op.printAll();
	}
}
