package edu.cmu.cs.cs214.shadowing;

public class Cat {
    protected String noise = "Cat noise";
    
    public String getNoise() {
    	    String noise = "Local cat noise";
    	    return noise;
    }
    
    public static void main(String[] args) {
    	    Cat d = new Cat();
    	    System.out.println(d.getNoise());
    }
}
