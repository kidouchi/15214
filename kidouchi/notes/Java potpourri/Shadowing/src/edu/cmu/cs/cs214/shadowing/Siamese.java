package edu.cmu.cs.cs214.shadowing;

public class Siamese extends Cat {
	private static final boolean VERBOSE = false;
	
    protected String noise = "Siamese noise";
    protected String[] otherNoises = {"Bark", "Oink", "Moo"};
    
    public String getNoise() {
    	    {
    	    	    String noise = "Very local siamese noise";
    	    	    if (VERBOSE) System.out.println(noise);
    	    }
    	    
    	    { 
    	    	    String noise = "Another very local siamese noise";
    	    	    if (VERBOSE) System.out.println(noise);
    	    }
    	        	    
    	    for (String noise : otherNoises) {
    	    	    noise = noise.concat(noise);
    	    	    if (VERBOSE) System.out.println(noise);
    	    }
    	    
    	    return noise;
    }
    
    public static void main(String[] args) {
    		Siamese bob = new Siamese();
    	    System.out.println(bob.getNoise());
    }
}
