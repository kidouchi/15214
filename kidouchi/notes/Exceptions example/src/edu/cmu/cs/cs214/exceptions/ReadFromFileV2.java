package edu.cmu.cs.cs214.exceptions;

import java.io.*;

public class ReadFromFileV2 {
	
	static int getIntFromFile(String filename) {
		try {
			FileInputStream fileInput = new FileInputStream(filename);
			DataInput dataInput = new DataInputStream(fileInput);
			int i = dataInput.readInt();
			fileInput.close();
			return i;
		} catch (FileNotFoundException e) {
			System.out.println("Could not open file " + filename);
			return -1;  // Danger!  Returns a magic value
		} catch (IOException e) {
			System.out.println("Error reading binary data from file " + filename);
			return -1;  // Danger!  Returns a magic value
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String fileName = "badFileName.txt";
		int i = getIntFromFile(fileName);
		System.out.println("read " + i + " from file " + fileName);
	}

}
