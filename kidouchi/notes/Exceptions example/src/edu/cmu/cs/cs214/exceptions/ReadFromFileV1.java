package edu.cmu.cs.cs214.exceptions;

import java.io.*;

public class ReadFromFileV1 {
	
	static int getIntFromFile(String filename) throws IOException {
		FileInputStream fileInput = new FileInputStream(filename);
		DataInput dataInput = new DataInputStream(fileInput);
		int i = dataInput.readInt();
		fileInput.close();
		return i;
	}

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		String fileName = "bowling_sces.bin";
		int i = getIntFromFile(fileName);
		System.out.println("read " + i + " from file " + fileName);
	}

}
