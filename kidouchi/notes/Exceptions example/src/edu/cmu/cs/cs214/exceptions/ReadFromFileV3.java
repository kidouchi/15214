package edu.cmu.cs.cs214.exceptions;

import java.io.*;

public class ReadFromFileV3 {
	
	static int getIntFromFile(String filename) throws IOException {
		FileInputStream fileInput = new FileInputStream(filename);
		DataInput dataInput = new DataInputStream(fileInput);
		int i = dataInput.readInt();
		fileInput.close();
		return i;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String fileName = "badFileName.bin";
		try {
			int i = getIntFromFile(fileName);
			System.out.println("read " + i + " from file " + fileName);
		} catch (FileNotFoundException e) {
			System.err.println("Could not open file " + fileName);
		} catch (IOException e) {
			System.err.println("Error reading binary data from file " + fileName);
		}
	}

}
