package edu.cmu.cs.cs214.exceptions;

import java.io.*;

public class ReadBowlingScore {

	static int getIntFromFile(String filename) throws IOException {
		FileInputStream fileInput = new FileInputStream(filename);
		DataInput dataInput = new DataInputStream(fileInput);
		int i = dataInput.readInt();
		fileInput.close();
		return i;
	}

	static int readBowlingScore(String filename) throws IllegalBowlingScoreException {
		try {
			return getIntFromFile(filename);
		} catch (IOException e) {
			throw new IllegalBowlingScoreException(
					"Could not read score from bowling file: " + filename, 
					e);
		}
	}

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) {
		String fileName = "badFile.txt";
		try {
			int i = readBowlingScore(fileName);
			System.out.println("read " + i + " from file " + fileName);
		} catch (IllegalBowlingScoreException e) {
			//System.out.println("Error:  " + e.getMessage());
			e.printStackTrace();
		}
	}

}
