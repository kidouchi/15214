package edu.cmu.cs.cs214.exceptions;

public class IllegalBowlingScoreException extends Exception {
	public IllegalBowlingScoreException() {
		super();
	}
	
	public IllegalBowlingScoreException(String msg) {
		super(msg);
	}
	
	public IllegalBowlingScoreException(Throwable cause) {
		super(cause);
	}
	
	public IllegalBowlingScoreException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
