package edu.cmu.cs.cs214.dog;

/**
 * Contains an array of dogs, which can be walked.
 * @author
 *
 */
public class DogWalker {
	Dog[] dogs;
	
	public DogWalker(Dog[] dogs) {
		this.dogs = dogs;
	}
	
	/** 
	 *  Walks the array of dogs.
	 */
	public void walkDogs() {
		for (Dog d : dogs) {
			d.bark();  // dogs bark when you walk them
		}
	}
	// Note:  the loop above is exactly the same as:
	//   for (int i = 0; i < dogs.length; i++) 
	//       dogs[i].bark();
	
	public static void main(String[] args){
		Dog lance = new Chiuaua("Lance");
		Dog gary = new GermanShepherd("Gary");
		Dog fido = new FeistyChiuaua("Fido");
		
		Dog[] pack = {lance, gary, fido};
		DogWalker david = new DogWalker(pack);
		david.walkDogs();
	}
}
