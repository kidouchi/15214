package edu.cmu.cs.cs214.dog;

public class FeistyChiuaua extends Chiuaua {

	public FeistyChiuaua(String name) {
		super(name);  // Calls the constructor of our parent class, with the
	}                 // String name as an argument
	
	public void bite() {
		System.out.println("I will bite your ankles.  Chomp! Chomp!");
	}
	
}
