package edu.cmu.cs.cs214.dog;

/**
 * Contains the main function which simply tests chiuaua and germanshepherd
 * @author
 *
 */
public class Main {
	
	public static void main(String[] args){
		Dog d = new Chiuaua("Lance");
		
		// use a chiuaua
		System.out.println(d.toString());
		d.bark();
		AbstractDog foo = new Chiuaua("Bob");
		foo.bark();
		
		// use a german shepherd
		d = new GermanShepherd("Gary");
		System.out.println(d.toString());
		d.bark();
		//d.play();
			
		// play with a german shepherd
		GermanShepherd gs = (GermanShepherd) d;
		gs.play();
		
		d.bark();
		
	}
}
