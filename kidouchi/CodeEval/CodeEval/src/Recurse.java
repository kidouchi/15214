
public class Recurse {

	  public static void main(String[] args) {
		  	System
		  
		  
	        try {
	            File file = new File(args[0]);
	            BufferedReader in = new BufferedReader(new FileReader(file));
	            String s;
	            ArrayList<int[]> al = new ArrayList<int[]>();
	            String[] lineArray;
	            while ((s = in.readLine()) != null) {
	                lineArray = s.split("\n");
	            
	                for (int i = 0; i < lineArray.length; i++) {
	                    System.out.println(lineArray[i]);
	                    String[] row = lineArray[i].split(" ");
	                    int[] hold = new int[row.length];
	                    for(int j = 0; j < row.length; j++) {
	                        hold[j] = Integer.parseInt(row[i]);
	                    }
	                    al.add(hold);
	                }
	            
	                //for(int k = 0; k < al.size(); k++) {
	                //   for(int l = 0; l < al.get(k).length; l++) {
	                 //       System.out.print(al.get(k)[l]);
	                  //  }
	                //    System.out.println("");
	                }
	                in.close();
	            }
	        }
	        catch (IOException e) {
	            System.out.println("File Read Error: " + e.getMessage());
	        }
	    }
	 }
			

	public ArrayList<int[]> makeNew(ArrayList<int[]> al) {
		ArrayList<int[]> hold = new ArrayList<int[]>();
		for (int i = 0; i < al.size(); i++) {
			hold.add(new int[al.get(i).length]);
			for (int j = 0; j < al.get(i).length; j++) {
				hold.get(i)[j] = -1;
			}
		}
		return hold;
	}


	}

	public int pass(ArrayList<int[]> al, ArrayList<int[]> hold, int row, int col) {
		int Rtotal = 0;
		int Ltotal = 0;

	//	if (row == al.size()) {
	//		return 0;
	//	}
		
		if(row == al.size()-1) {
			return al.get(row)[col];
		}
		
		if (row + 1 < al.size()) {
			if (hold.get(row)[col] == -1) {
				int hold1 = pass(al, hold, row+1, col+1);
				hold.get(row)[col] = hold1;
			}
			if (hold.get(row + 1)[col] == -1) {
				int hold2 = pass(al, hold, row + 1, col);
				hold.get(row + 1)[col] = hold2;
			}
			Rtotal += al.get(row)[col] + hold.get(row+1)[col+1];
			Ltotal += al.get(row)[col] + hold.get(row+1)[col];
		}

		if (Rtotal > Ltotal) {
			return Rtotal;
		}
		return Ltotal;

	}

	public static void main(String[] args) {
		Recurse test = new Recurse();

		 ArrayList<int[]> al = new ArrayList<int[]>();
		 int[] r1 = { 5 };
		 int[] r2 = { 9, 6 };
		 int[] r3 = { 4, 6, 8 };
		 int[] r4 = { 0, 7, 1, 5 };
		 al.add(r1);
		 al.add(r2);
		 al.add(r3);
		 al.add(r4);
		 ArrayList<int[]> hold = test.makeNew(al);
				 
		int test2 = test.pass(al, hold, 0, 0);
		
		for(int i = 0; i < hold.size(); i++) {
			for(int j = 0; j < hold.get(i).length; j++) {
				System.out.println("(" + i + ", " + j + ")" + " " + hold.get(i)[j]);
			}
		}
		
		System.out.println(test2);
		

		// String x = "hello";
		// System.out.println(x);
		// System.out.println("Test1 " + test.reverse(x));

		// System.out.println("Test2 " + test.revRec(x));

		// int[] A = {0, 1, 2, 5};
		// int[] B = {3, 4, 6, 7};
		// int k = 3;

		// int test1 = test.smallUnion(A, B, k);
		// System.out.println(test1);

		;
		// boolean test1 = test.uniqueChar("hello");
		// System.out.println(test1);

		// String test1 = test.reverse(new String("hello"));
		// System.out.println("LOOK: " + test1);

		// String test2 = test.unique("Hello");
		// System.out.println("HEY " + test2);
		// String test3 = test.unique("aaaabbbbbcccc");
		// System.out.println("YO" + test3);
		// String test4 = test.unique("aaaaaaaaaaa");
		// System.out.println("BRO" + test4);
		// String test5 = test.unique("aaaaaaabbbbbccccccaaaa");
		// System.out.println("LE" + test5);

		// System.out.println("test1 " + test.anagram("hello", "ohell"));
		// System.out.println("test2 " + test.anagram("racecar", "carrace"));
		// System.out.println("test3 " + test.anagram("hello", "racecar"));

		

	}

}
