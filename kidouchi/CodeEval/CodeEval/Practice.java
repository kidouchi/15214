package CodeEval;

import CodeEval.src.String;

public class Practice {

	

		public boolean uniqueChar(String word) {
			// boolean[] check_set = new boolean[256];
			// int[] check_set = new int[256];
			// String[] check_set = new String[256];
			// for(int i = 0; i < 256; i++) {
			// int val = word.charAt(i);
			// System.out.println(check_set[i]);
			// if(check_set[val]) {
			// return false;
			// }
			// check_set[val] = true;
			// }

			return true;
		}

		// public String reverse(String orig) {
		// int l = 4;
		// char[] orig_arr = orig.toCharArray();
		// char[] new_arr = new char[orig_arr.length + 1];
		// new_arr[0] = '\0';
		// int j = 1;
		// for (int i = orig_arr.length - 1; i >= 0; i--) {
		// new_arr[j] = orig_arr[i];
		// System.out.println(new_arr[j]);
		// j++;
		// }
		// return new String(new_arr);
		// }

		public String unique(String word) {
			if (word == null) {
				return "";
			}
			char[] arr = word.toCharArray();
			char hold;

			for (int i = 0; i < arr.length; i++) {
				hold = arr[i];
				for (int j = i + 1; j < arr.length; j++) {
					if (arr[j] != '\0') {
						if (hold == arr[j]) {
							arr[j] = '\0';
						}
					}
				}
			}
			return new String(arr);
		}

		public boolean anagram(String word1, String word2) {
			char[] w1 = word1.toCharArray();
			char[] w2 = word2.toCharArray();
			char hold;

			if (w1.length != w2.length) {
				return false;
			}

			for (int i = 0; i < w1.length; i++) {
				hold = w1[i];
				for (int j = 0; j < w2.length; j++) {
					if (hold == w2[j]) {
						w2[j] = '\0';
						break;
					} else if (j == w2.length - 1) {
						return false;
					}
				}
			}
			return true;
		}

		public class Node {

			Node next = null;
			int data;

			public Node(int data) {
				this.data = data;
			}

			void appendToTail(int d) {
				Node end = new Node(d);
				Node n = this;

				while (n.next != null) {
					n = n.next;
				}

				n.next = end;
			}
		}

		// public void deldup(Node head) {

		// Node prev = head;

		// while(prev != null) {
		// Node curr = prev.next;
		// while(curr != null) {
		// if(prev.data == curr.data) {
		// Node temp = curr.prev;
		// tmp.next = curr.next;
		// }
		// curr = curr.next;
		// }
		// }

		// }
		
		public String reverse(String word) {
			char[] word_arr = word.toCharArray();
			char[] new_arr = new char[word_arr.length];
			int arr_num = 0;

			for (int i = word_arr.length - 1; i >= 0; i--) {
				new_arr[arr_num] = word_arr[i];
				arr_num++;
			}

			return new String(new_arr);
		}

		public String revRec(String word) {
			if (word.length() == 0) {
				return "";
			}

			return word.charAt(word.length() - 1)
					+ revRec(word.substring(0, word.length() - 1));

		}

		// int revBit (int unum) {
		// String bitstr = ;
		// return Integer.parseInt(bitstr.charAt(0) + revBit(substring(1,
		// bitstr.length()-1) + bitstr.charAt(word.length()-1));
		// }

		public int smallUnion(int[] A, int[] B, int k) {
			int i = 0;
			int p1 = A[i];
			int j = 0;
			int p2 = B[j];

			while (k > 1) {
				if (p1 <= p2) {
					i = i + 1;
					if (i < A.length) {
						p1 = A[i];
					}
				} else {
					j = j + 1;
					if (j < B.length) {
						p2 = B[j];
					}
				}
				k--;
			}
			if (p1 <= p2)
				return p1;
			return p2;

}
