package edu.cmu.cs.cs214.rec13.client;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import edu.cmu.cs.cs214.rec13.WorkerServer;
import edu.cmu.cs.cs214.rec13.tasks.CountWordTask;
import edu.cmu.cs.cs214.rec13.util.WorkerConfig;

/**
 * A simple client that submits a task to three {@link WorkerServer}s, and
 * prints the combined result.
 */
public class CountWordClient extends Thread {
    private final CountWordTask mTask;
    private final WorkerConfig[] mWorkers;
    private final ExecutorService mExecutor;

    public CountWordClient(CountWordTask task, WorkerConfig[] workers) {
        mTask = task;
        mWorkers = workers;
        mExecutor = Executors.newSingleThreadExecutor();
    }

    @Override
    public void run() {
        // TODO: (1) Distribute the task across the available workers.

        // TODO: (2) Wait for each worker to send back the result.

        // TODO: (3) After every worker has completed the task, print the final
        // combined result.
    }

    public static void main(String[] args) {
        WorkerConfig[] workers = WorkerConfig.values();
        new CountWordClient(new CountWordTask("just"), workers).start(); // 75
        new CountWordClient(new CountWordTask("gregor"), workers).start(); // 298
        new CountWordClient(new CountWordTask("the"), workers).start(); // 2656
    }

}
