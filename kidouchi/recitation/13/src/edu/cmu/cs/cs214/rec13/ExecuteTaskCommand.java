package edu.cmu.cs.cs214.rec13;

import java.io.IOException;
import java.net.Socket;

import edu.cmu.cs.cs214.rec13.tasks.Task;

/**
 * A simple {@link WorkerCommand} that executes a {@link Task} and sends
 * the calculated result back to the client.
 */
public class ExecuteTaskCommand<T> extends WorkerCommand {
    private static final long serialVersionUID = -5314076333098679665L;

    private final Task<T> mTask;
    private final String mFileName;

    public ExecuteTaskCommand(Task<T> task, String fileName) {
        mTask = task;
        mFileName = fileName;
    }

    @Override
    public void run() {
        // Get the socket to use to send results back to the client.
        Socket socket = getSocket();

        try {
            // TODO: (1) Open a FileInputStream for mFileName, execute the task,
            // and obtain the task's final result. Don't forget to close the
            // FileInputStream when you're finished using it!

            // TODO: (2) Send the final result back to the client.
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                // Ignore because we're about to exit anyway.
            }
        }
    }

}
