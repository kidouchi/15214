package edu.cmu.cs.cs214.rec13;

import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;

/**
 * Defines a command to be executed by a remote {@link WorkerServer}. A client
 * server can write a {@link WorkerCommand} object over the network to a
 * {@link WorkerServer} with the expectation that the {@link WorkerServer} will,
 * (1) execute the command, and (2) assign a socket for the
 * {@link WorkerCommand} to use to communicate results back to the client
 * server.
 */
public abstract class WorkerCommand implements Runnable, Serializable {
    private static final long serialVersionUID = 3243715720901948359L;

    /**
     * Note that we declare the {@link Socket} as <code>transient</code> to
     * indicate that the field should not be serialized (that is, it will be
     * ignored when Java converts this object into a stream of bytes to be
     * written over an {@link ObjectOutputStream}).
     */
    private transient Socket mSocket;

    /**
     * Set the socket to use to communicate with the server that sent this
     * command.
     */
    protected final void setSocket(Socket socket) {
        mSocket = socket;
    }

    /**
     * Get the socket to use to communicate with the server that sent this
     * command.
     */
    protected final Socket getSocket() {
        return mSocket;
    }

}
