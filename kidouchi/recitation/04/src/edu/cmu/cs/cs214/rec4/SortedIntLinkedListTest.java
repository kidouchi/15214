package edu.cmu.cs.cs214.rec4;

/**
 * Press the following buttons:
 *
 * (1) To begin debugging, press "Run --> Debug".
 *
 * (2) To enable assertions, press "Run --> Run Configurations", click on the "Arguments" tab, type
 * "-ea" in VM arguments, and press execute (err... I mean, 'apply').
 *
 * @author Dr. Pierre Chang
 */
public class SortedIntLinkedListTest {

    public static void main(String args[]) {
        SortedIntLinkedList walt = new SortedIntLinkedList();
        walt.add(4);
        walt.add(4);
        walt.add(42);
        walt.add(8);
        walt.add(108);
        walt.add(16);
        walt.add(15);
        walt.add(8);
        walt.add(15);
        walt.add(23);

        // list should equal [4, 8, 15, 16, 23, 42, 108]
        System.out.println(walt.toString());

        walt.remove(108);
        walt.remove(815);
        walt.remove(7418880);

        // list should equal [4, 8, 15, 16, 23, 42]
        System.out.println(walt.toString());
    }
}
