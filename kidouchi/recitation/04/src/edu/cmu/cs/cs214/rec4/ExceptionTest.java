package edu.cmu.cs.cs214.rec4;

/**
 * A simple class demonstrating how exceptions are thrown/handled in the method call stack. All of
 * the exceptions in this class are "unchecked" (i.e. they all extend {@link RuntimeException}).
 *
 * As a bonus assignments, you might complete the following tasks:
 *
 * <ol>
 * <li>Determine the behavior of the program.</li>
 * <li>Comment out redundant catch statements (if they exist).</li>
 * <li>Write detailed javadocs comments for each method, documenting all exceptions that may be
 * thrown.</li>
 * </ol>
 */
public class ExceptionTest {

    public static void main(String[] args) {
        new ExceptionTest().first();
    }

    private void first() {
        try {
            second();
        } catch (ClassCastException e) {
            System.err.println("first() caught ClassCastException");
        } catch (ArrayIndexOutOfBoundsException e) {
            System.err.println("first() caught ArrayIndexOutOfBoundsException");
        } catch (IndexOutOfBoundsException e) {
            System.err.println("first() caught IndexOutOfBoundsException");
        } finally {
            System.err.println("Finally!");
        }
        System.err.println("Test complete!\n");
    }

    private void second() {
        try {
            // throw new ArrayIndexOutOfBoundsException();
            // throw new NullPointerException();
        } catch (ClassCastException e) {
            System.err.println("second() caught ClassCastException");
        } catch (NullPointerException e) {
            System.err.println("second() caught NullPointerException");
        }
    }

}
