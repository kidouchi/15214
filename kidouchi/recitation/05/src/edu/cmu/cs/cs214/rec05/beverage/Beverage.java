package edu.cmu.cs.cs214.rec05.beverage;

public abstract class Beverage implements SizeFactor {
	
	//private int sizeCost;
	
	//public Beverage(SizeFactor size) {
	//	sizeCost = size.getCost();
	//}
	
	public Beverage() {
	}
	
    /**
     * Calculates and returns the cost of the {@link Beverage}.
     *
     * @return The cost of this {@link Beverage} in cents.
     */
    public int getCost() {
    	//return sizeCost; 
    	return 1;
    }
    	
}
