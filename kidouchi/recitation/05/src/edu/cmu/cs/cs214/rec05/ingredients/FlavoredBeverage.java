package edu.cmu.cs.cs214.rec05.ingredients;

import edu.cmu.cs.cs214.rec05.beverage.Beverage;

public abstract class FlavoredBeverage extends Beverage {

	private Beverage beverage;
	
	public FlavoredBeverage(Beverage beverage) {
		this.beverage = beverage;
	}
	
	public FlavoredBeverage() {
	}
	
    @Override
    public int getCost() {
    	//return beverage.getCost();
    	return 1;
    }

}
