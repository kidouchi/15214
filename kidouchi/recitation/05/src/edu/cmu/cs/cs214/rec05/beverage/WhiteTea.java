package edu.cmu.cs.cs214.rec05.beverage;

public class WhiteTea extends Tea {
	
	private int PRICE_CENTS = 100;
	private TeaSizeFactor teaSize;
	
	public WhiteTea(String size) {
		teaSize = new TeaSizeFactor(size);
	}
	
	@Override
	public int getCost() {
		return PRICE_CENTS + teaSize.getCost();
	}

}
