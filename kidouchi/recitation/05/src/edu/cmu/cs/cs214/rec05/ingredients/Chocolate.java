package edu.cmu.cs.cs214.rec05.ingredients;

import edu.cmu.cs.cs214.rec05.beverage.Beverage;

public class Chocolate extends FlavoredBeverage {
	
	private int PRICE_CENTS = 30;
	private int bvgCost;
	
	public Chocolate(Beverage beverage) {
		bvgCost = beverage.getCost();
	}
	
	@Override
	public int getCost() {
		return PRICE_CENTS + bvgCost;
	}

}
