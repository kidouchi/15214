package edu.cmu.cs.cs214.rec05.beverage;

public class DecafCoffee extends Coffee {
	
	private int PRICE_CENTS = 50;
	private CoffeeSizeFactor coffeeSize;
	
	public DecafCoffee(String size) {
		coffeeSize = new CoffeeSizeFactor(size);
	}
	
	@Override
	public int getCost() {
		return PRICE_CENTS + coffeeSize.getCost();
	}

}
