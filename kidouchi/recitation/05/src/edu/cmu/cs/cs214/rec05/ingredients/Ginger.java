package edu.cmu.cs.cs214.rec05.ingredients;

import edu.cmu.cs.cs214.rec05.beverage.Beverage;

public class Ginger extends FlavoredBeverage {

	private int PRICE_CENTS = 60;
	private int bvgCost;
	
	public Ginger(Beverage beverage) {
		bvgCost = beverage.getCost();
	}

	@Override
	public int getCost() {
		return PRICE_CENTS + bvgCost;
	}
			
}
