package edu.cmu.cs.cs214.rec05.beverage;

public class GreenTea extends Tea {
	
	private int PRICE_CENTS = 100;
	private TeaSizeFactor teaSize;
	
	public GreenTea(String size) {
		teaSize = new TeaSizeFactor(size);
	}
	
	@Override
	public int getCost() {
		return PRICE_CENTS + teaSize.getCost();
	}


}
