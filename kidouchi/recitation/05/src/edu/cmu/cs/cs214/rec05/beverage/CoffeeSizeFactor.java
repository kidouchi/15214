package edu.cmu.cs.cs214.rec05.beverage;


public class CoffeeSizeFactor implements SizeFactor {

	private String size;
	private int PRICE_SMALL = 40;
	private int PRICE_MEDIUM = 70;
	private int PRICE_LARGE = 100;

	public CoffeeSizeFactor(String size) {
		this.size = size;
	}
	
	@Override
	public int getCost() {
		if(size.equals("small")) {
			return PRICE_SMALL;
		}
		else if(size.equals("medium")) {
			return PRICE_MEDIUM;
		}
		else if(size.equals("large")) {
			return PRICE_LARGE;
		}
		
		return 0;
	}
	
}
