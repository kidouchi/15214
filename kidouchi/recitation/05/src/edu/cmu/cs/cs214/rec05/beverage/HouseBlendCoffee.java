package edu.cmu.cs.cs214.rec05.beverage;

public class HouseBlendCoffee extends Coffee {
	
	private int PRICE_CENTS = 80;
	private CoffeeSizeFactor coffeeSize;
	
	public HouseBlendCoffee(String size) {
		coffeeSize = new CoffeeSizeFactor(size);
	}
	
	@Override
	public int getCost() {
		return PRICE_CENTS + coffeeSize.getCost();
	}

}
