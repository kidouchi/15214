package edu.cmu.cs.cs214.rec05.beverage;

public class TeaSizeFactor implements SizeFactor {
	
	private String sizeStr;
	private int PRICE_SMALL = 20;
	private int PRICE_MEDIUM = 50;
	private int PRICE_LARGE = 70;
	
	public TeaSizeFactor(String size) {
		sizeStr = size;
	}
	
	@Override
	public int getCost() {
		if(sizeStr.equals("small")) {
			return PRICE_SMALL;
		}
		else if (sizeStr.equals("medium")){
			return PRICE_MEDIUM;
		}
		else if (sizeStr.equals("large")) {
			return PRICE_LARGE;
		}
		
		return 0;
	}

}
