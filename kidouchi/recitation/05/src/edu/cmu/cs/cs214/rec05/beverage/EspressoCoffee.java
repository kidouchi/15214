package edu.cmu.cs.cs214.rec05.beverage;

public class EspressoCoffee extends Coffee{

	private int PRICE_CENTS = 100;
	private CoffeeSizeFactor coffeeSize;
	
	public EspressoCoffee(String size) {
		coffeeSize = new CoffeeSizeFactor(size);
	}
	
	@Override
	public int getCost() {
		return PRICE_CENTS + coffeeSize.getCost();
	}


}
