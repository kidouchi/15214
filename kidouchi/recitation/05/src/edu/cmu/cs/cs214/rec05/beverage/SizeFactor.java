package edu.cmu.cs.cs214.rec05.beverage;

public interface SizeFactor {
	
	/**
	 * Get the cost based on the beverage's size
	 * @return
	 */
	public int getCost();
	
	
}
