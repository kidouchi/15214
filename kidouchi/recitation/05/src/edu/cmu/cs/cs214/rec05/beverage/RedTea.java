package edu.cmu.cs.cs214.rec05.beverage;

public class RedTea extends Tea {
	
	private int PRICE_CENTS = 80;
	private TeaSizeFactor teaSize;
	
	
	public RedTea(String size) {
		teaSize = new TeaSizeFactor(size);
	}
	
	@Override
	public int getCost() {
		return PRICE_CENTS + teaSize.getCost();
	}


}
