package edu.cmu.cs.cs214.rec05.ingredients;

import edu.cmu.cs.cs214.rec05.beverage.Beverage;

public class WhippedCream extends FlavoredBeverage {

	private int PRICE_CENTS = 30;
	private int bvgCost;
	
	public WhippedCream(Beverage beverage) {
		bvgCost = beverage.getCost();
	}
	
	@Override
	public int getCost() {
		return PRICE_CENTS + bvgCost;
	}
}
