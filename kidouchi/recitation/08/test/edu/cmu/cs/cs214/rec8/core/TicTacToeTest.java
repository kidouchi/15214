package edu.cmu.cs.cs214.rec8.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;

public class TicTacToeTest {

    private TicTacToe game;
    private Player playerX;

    @Before
    public void setUp() {
        game = new TicTacToeImpl();
        playerX = new Player("Cross", "x");
    }

    @Test
    public void testSingleMove() {
        game.startNewGame();
        assertNull(game.getSquare(0, 0));
        game.playMove(0, 0);
        assertEquals(playerX, game.getSquare(0, 0));
    }

	// TODO: write more tests
}
