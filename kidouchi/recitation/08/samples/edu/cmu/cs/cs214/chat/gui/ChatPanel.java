package edu.cmu.cs.cs214.chat.gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import edu.cmu.cs.cs214.chat.core.ChatServer;
import edu.cmu.cs.cs214.chat.core.ChatSubscriber;

public class ChatPanel extends JPanel implements ChatSubscriber {
    private static final long serialVersionUID = 4819162036004676580L;

    private static final int FIELD_WIDTH = 60;
    private static final int AREA_WIDTH = FIELD_WIDTH + 10;
    private static final int AREA_HEIGHT = 20;

    private final String mName;
    private final ChatServer mServer;
    private final JTextArea mChatArea;

    public ChatPanel(ChatServer server, String name) {
        if (name == null || server == null) {
            throw new NullPointerException("Server and name must not be null.");
        }
        mName = name;
        mServer = server;

        // Sets up a scrollable text area that line-wraps its messages, for the
        // chat messages.
        mChatArea = new JTextArea(AREA_HEIGHT, AREA_WIDTH);
        mChatArea.setEditable(false);
        mChatArea.setLineWrap(true);
        mChatArea.setWrapStyleWord(true);

        // Fun fact: the JScrollPane is a good example of the decorator pattern.
        JScrollPane scrollPane = new JScrollPane(mChatArea);

        // Sets up a text field for typing chat messages, and a button to send
        // them.
        final JTextField chatField = new JTextField(FIELD_WIDTH);
        JButton sendButton = new JButton("Send");
        JPanel messagePanel = new JPanel();
        messagePanel.setLayout(new BorderLayout());
        messagePanel.add(chatField, BorderLayout.WEST);
        messagePanel.add(sendButton, BorderLayout.EAST);

        // Observer to send a message when the user presses the send button or
        // hits enter in the message field.
        ActionListener sendChatListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String message = chatField.getText();
                if (!message.isEmpty()) {
                    mServer.publish(mName, message);
                }
                chatField.setText("");
                chatField.requestFocus();
            }
        };

        sendButton.addActionListener(sendChatListener);
        chatField.addActionListener(sendChatListener);

        setLayout(new BorderLayout());
        add(scrollPane, BorderLayout.NORTH);
        add(messagePanel, BorderLayout.SOUTH);
        setVisible(true);

        // Subscribes to chat messages.
        mServer.subscribe(this);
    }

    @Override
    public void handleMessage(String name, String message) {
        String newText = String.format(" %s: %s\n", name, message);
        mChatArea.append(newText);
    }
}
