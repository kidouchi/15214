package edu.cmu.cs.cs214.chat.core;

public interface ChatSubscriber {
    public void handleMessage(String name, String message);
}
