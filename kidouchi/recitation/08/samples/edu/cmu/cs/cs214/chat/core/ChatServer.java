package edu.cmu.cs.cs214.chat.core;

import java.util.ArrayList;
import java.util.List;

public class ChatServer {
    private final List<ChatSubscriber> subscribers;

    public ChatServer() {
        subscribers = new ArrayList<ChatSubscriber>();
    }

    public void subscribe(ChatSubscriber subscriber) {
        subscribers.add(subscriber);
    }

    public void unsubscribe(ChatSubscriber subscriber) {
        subscribers.remove(subscriber);
    }

    public void publish(String name, String message) {
        for (ChatSubscriber s : subscribers) {
            s.handleMessage(name, message);
        }
    }
}
