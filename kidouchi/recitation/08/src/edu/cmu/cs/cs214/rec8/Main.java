package edu.cmu.cs.cs214.rec8;

import javax.swing.SwingUtilities;

public class Main {

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                createAndShowGameBoard();
            }
        });
    }

    private static void createAndShowGameBoard() {
        // TODO: Start implementing your GUI here
    }

}
