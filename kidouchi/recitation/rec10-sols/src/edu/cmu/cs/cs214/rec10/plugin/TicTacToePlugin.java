package edu.cmu.cs.cs214.rec10.plugin;

import edu.cmu.cs.cs214.rec10.framework.core.GameFramework;
import edu.cmu.cs.cs214.rec10.framework.core.Player;
import edu.cmu.cs.cs214.rec10.framework.core.GamePlugin;

/**
 * An example Tic-Tac-Toe game plug-in.
 */
public class TicTacToePlugin implements GamePlugin {

    private static final String GAME_NAME = "Tic-Tac-Toe";

    private static final int GRID_WIDTH = 3;
    private static final int GRID_HEIGHT = 3;

    private static final String PLAYER_WON_MSG = "%s has won the game!";
    private static final String GAME_TIED_MSG = "The game has ended in a tie.";

    private GameFramework framework;
    private boolean stalemate;

    @Override
    public String getGameName() {
        return GAME_NAME;
    }

    @Override
    public int getGridWidth() {
        return GRID_WIDTH;
    }

    @Override
    public int getGridHeight() {
        return GRID_HEIGHT;
    }

    @Override
    public void onRegistered(GameFramework f) {
        framework = f;
    }

    @Override
    public void onNewGame() {
        stalemate = true;
    }

    @Override
    public boolean isMoveValid(int x, int y) {
        // In TicTacToe a move is valid as long as the selected
        // square is not already marked.
        return framework.getSquare(x, y) == null;
    }

    @Override
    public void onNewMove() {
        // Nothing to do here.
    }

    @Override
    public boolean isGameOver() {
        if (hasWon(framework.getCurrentPlayer())) {
            // Then a player has won the game.
            stalemate = false;
            return true;
        }
        if (isFull()) {
            // Then the game ended in a tie.
            return true;
        }
        return false;
    }

    @Override
    public String getGameOverMessage() {
        if (!stalemate) {
            String name = framework.getCurrentPlayer().getName();
            return String.format(PLAYER_WON_MSG, name);
        }
        return GAME_TIED_MSG;
    }

    private boolean isFull() {
        for (int y = 0; y < GRID_HEIGHT; y++) {
            for (int x = 0; x < GRID_WIDTH; x++) {
                if (framework.getSquare(x, y) == null) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean hasWon(Player currentPlayer) {
        final String symbol = currentPlayer.getSymbol();

        // Check for a horizontal win
        for (int y = 0; y < GRID_HEIGHT; y++) {
            for (int x = 0; x < GRID_WIDTH; x++) {
                Object square = framework.getSquare(x, y);
                if (square == null || !square.equals(symbol)) {
                    break;
                }
                if (x == GRID_WIDTH - 1) {
                    return true;
                }
            }
        }

        // Check for a vertical win
        for (int x = 0; x < GRID_WIDTH; x++) {
            for (int y = 0; y < GRID_HEIGHT; y++) {
                Object square = framework.getSquare(x, y);
                if (square == null || !square.equals(symbol)) {
                    break;
                }
                if (y == GRID_HEIGHT - 1) {
                    return true;
                }
            }
        }

        // Check for a diagonal win
        for (int x = 0, y = 0; x < GRID_WIDTH && y < GRID_HEIGHT; x++) {
            Object square = framework.getSquare(x, y);
            if (square == null || !square.equals(symbol)) {
                break;
            }
            if (x == GRID_WIDTH - 1 && y == GRID_HEIGHT - 1) {
                return true;
            }
            y++;
        }

        // Check for anti-diagonal win
        for (int x = GRID_WIDTH - 1, y = 0; x >= 0 && y < GRID_HEIGHT; x--) {
            Object square = framework.getSquare(x, y);
            if (square == null || !square.equals(symbol)) {
                break;
            }
            if (x == 0 && y + 1 == GRID_HEIGHT) {
                return true;
            }
            y++;
        }

        return false;
    }

    @Override
    public boolean isMoveOver() {
        // A move is always over after every valid action.
        return true;
    }

    @Override
    public void onMovePlayed(int x, int y) {
        framework.setSquare(x, y, framework.getCurrentPlayer().getSymbol());
    }
}
