package edu.cmu.cs.cs214.rec02.inheritance;

import java.util.Set;

public abstract class Farm implements Place {

	Set<Animal> animals;

	public int countAnimals() {
		return animals.size();
	}

	public void addAnimal(Animal a) {
		animals.add(a);
		if (a instanceof Sheep) {
			System.out.println("Added a Sheep");
		} else if (a instanceof Cow) {
			System.out.println("Added a Dog");
		}
	}

}
