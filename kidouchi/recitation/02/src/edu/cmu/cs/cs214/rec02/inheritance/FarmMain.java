package edu.cmu.cs.cs214.rec02.inheritance;

public class FarmMain {

	public static void main(String[] args){
		
		/*
		 * 4. Why wouldn't the following line compile?
		 */
		
		//Animal animal = new Animal();
	
		Animal animalSheep = new Sheep();
		Animal animalCow = new Cow();
		Sheep sheep = new Sheep();
		Cow cow = new Cow();
		
		/*
		 * 5. What will the following print?
		 */
		
		animalSheep.makeNoise();
		animalCow.makeNoise();

		/* 
		 * 6. Which of the following assignments will fail?
		 */
		
		// Sheep newSheep0 = (Sheep) animalSheep;
		// Sheep newSheep1 = (Sheep) sheep;
		// Cow newCow0 = (Cow) animalSheep;
		
		/*
		 * 7. How many legs does each animal have? (What is the output of the
		 * following?)
		 */
		
		cow.countLegs();
		sheep.countLegs();
		
		/*
		 * 8. What will print out when the following is run?
		 */
		
		animalSheep.action();
		animalCow.action();
		
		Farm momAndPops = new MyFarm("Kansas");
		
		/*
		 * 9. Though MyFarm doesn't contain the method addAnimal(), there is
		 * no error in the following lines. Why?
		 */
		
		momAndPops.addAnimal(animalSheep);
		momAndPops.addAnimal(sheep);
	}
	
}
