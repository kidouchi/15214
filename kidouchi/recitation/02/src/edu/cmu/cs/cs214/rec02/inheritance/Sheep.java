package edu.cmu.cs.cs214.rec02.inheritance;

public class Sheep extends Animal {

	public Sheep() {
		super();
		numLegs = 2;
	}

	public void makeNoise() {
		//super.makeNoise();
		System.out.println("'Baahh.'");
	}

	public void action() {
		System.out.println("Sheep Action");
	}

}
