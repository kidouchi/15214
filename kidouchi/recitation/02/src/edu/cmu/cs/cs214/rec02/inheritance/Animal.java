package edu.cmu.cs.cs214.rec02.inheritance;

public abstract class Animal {

	public int numLegs;

	public Animal() {
		numLegs = 4;
	}

	public void countLegs() {
		System.out.println(numLegs);
	}

	public void makeNoise() {
		System.out.println("Animal noise");
	}

	public void action() {
		System.out.println("Animal action");
		makeNoise();
	}

}
