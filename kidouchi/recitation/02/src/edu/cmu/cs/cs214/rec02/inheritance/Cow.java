package edu.cmu.cs.cs214.rec02.inheritance;

public class Cow extends Animal {

	public void makeNoise() {

		System.out.println("'Moo!'");
	}

	public void action() {
		super.action();
		System.out.println("Cow Action");
	}
}