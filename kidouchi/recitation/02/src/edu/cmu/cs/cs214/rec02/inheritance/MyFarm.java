package edu.cmu.cs.cs214.rec02.inheritance;

import java.util.HashSet;

public class MyFarm extends Farm {

	String location;

	public MyFarm(String location) {
		animals = new HashSet<Animal>();
		this.location = location;

		for (int i = 0; i < 10; i++) {
			if (i % 5 == 4) {
				animals.add(new Cow());
			} else {
				animals.add(new Sheep());
			}
		}
	}

	public String getLocation() {
		return location;
	}

}
