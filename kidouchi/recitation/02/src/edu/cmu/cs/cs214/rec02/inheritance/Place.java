package edu.cmu.cs.cs214.rec02.inheritance;

public interface Place {

	public String getLocation();

}
