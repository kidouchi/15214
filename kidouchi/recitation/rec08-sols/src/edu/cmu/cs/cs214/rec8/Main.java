package edu.cmu.cs.cs214.rec8;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import edu.cmu.cs.cs214.rec8.core.Player;
import edu.cmu.cs.cs214.rec8.core.TicTacToe;
import edu.cmu.cs.cs214.rec8.core.TicTacToeImpl;
import edu.cmu.cs.cs214.rec8.gui.GameBoardPanel;

public class Main {

    private static final String RECITATION_NAME = "Recitation 8";

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                createAndShowGameBoard();
            }
        });
    }

    private static void createAndShowGameBoard() {
        // Create and set-up the window.
        JFrame frame = new JFrame(RECITATION_NAME);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        TicTacToe game = new TicTacToeImpl();
        game.addPlayer(new Player("Cross", "x"));
        game.addPlayer(new Player("Knot", "o"));

        // Create and set up the content pane.
        GameBoardPanel gamePanel = new GameBoardPanel(game);
        gamePanel.setOpaque(true);
        frame.setContentPane(gamePanel);

        game.startNewGame();

        // Display the window.
        frame.pack();
        frame.setVisible(true);
    }

}
