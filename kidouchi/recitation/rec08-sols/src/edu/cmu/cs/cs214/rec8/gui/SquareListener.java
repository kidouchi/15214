package edu.cmu.cs.cs214.rec8.gui;

import java.awt.event.ActionEvent;

import java.awt.event.ActionListener;

import edu.cmu.cs.cs214.rec8.core.TicTacToe;

public class SquareListener implements ActionListener {

    private final int x;
    private final int y;
    private final TicTacToe game;

    /**
     * Creates a new square listener to get click events at a specific game grid
     * coordinate.
     */
    public SquareListener(int x, int y, TicTacToe game) {
        this.x = x;
        this.y = y;
        this.game = game;
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        game.playMove(x, y);
    }

}
