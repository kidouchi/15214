package edu.cmu.cs.cs214.rec8.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import edu.cmu.cs.cs214.rec8.core.GameChangeListener;
import edu.cmu.cs.cs214.rec8.core.Player;
import edu.cmu.cs.cs214.rec8.core.TicTacToe;

public class GameBoardPanel extends JPanel implements GameChangeListener {
    private static final long serialVersionUID = 2677360302729633608L;

    private final TicTacToe game;
    private final JButton[][] squares;
    private final JLabel currentPlayerLabel;

    private final int gridWidth;
    private final int gridHeight;

    public GameBoardPanel(TicTacToe g) {
        game = g;
        game.addGameChangeListener(this);
        currentPlayerLabel = new JLabel();
        gridWidth = game.getGridWidth();
        gridHeight = game.getGridHeight();
        squares = new JButton[gridWidth][gridHeight];
        initGui();
    }

    private void initGui() {
        setLayout(new BorderLayout());
        add(currentPlayerLabel, BorderLayout.NORTH);
        add(createBoardPanel(), BorderLayout.CENTER);
    }

    private JPanel createBoardPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(gridHeight, gridWidth));

        // Create all of the squares and display them.
        for (int y = 0; y < gridHeight; y++) {
            for (int x = 0; x < gridWidth; x++) {
                squares[y][x] = new JButton();
                squares[y][x].setText(x + "," + y);
                squares[y][x].addActionListener(new SquareListener(x, y, game));
                panel.add(squares[y][x]);
            }
        }
        return panel;
    }

    @Override
    public void squareChanged(int x, int y) {
        JButton button = squares[y][x];
        Player player = game.getSquare(x, y);
        if (player != null) {
            button.setText(player.getSymbol());
        } else {
            button.setText("   ");
        }
    }

    @Override
    public void currentPlayerChanged(Player player) {
        String name = player.getName();
        String symbol = player.getSymbol();
        currentPlayerLabel.setText("Current player: " + name + " (" + symbol + ")");
    }

    @Override
    public void gameEnded(Player winner) {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);

        if (winner != null) {
            showDialog(frame, "Winner!", winner.getName() + " just won the game!");
        } else {
            showDialog(frame, "Stalemate", "The game has ended in a stalemate.");
        }

        // Append the 'start new game' command to the end of the
        // EventQueue. This is necessary because we need to wait
        // for all of the buttons to finish dispatching before
        // we reset the game's state. (If you are confused about
        // this, try calling 'game.startNewGame()' without the
        // 'invokeLater' and see what happens).
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                game.startNewGame();
            }
        });
    }

    private static void showDialog(Component component, String title, String message) {
        JOptionPane.showMessageDialog(component, message, title, JOptionPane.INFORMATION_MESSAGE);
    }
}
