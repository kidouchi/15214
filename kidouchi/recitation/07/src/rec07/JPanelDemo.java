package rec07;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class JPanelDemo extends JPanel {

	
	private JTextField mTextField;
	private JLabel mLabel;
	private JButton mButton;
	
	public JPanelDemo() {
		//adding Hello World!
		mLabel = new JLabel("Hello, world!");
		add(mLabel);
	
		mTextField = new JTextField();
		mTextField.setColumns(24);
		add(mTextField);
		
		//adding button
		mButton = new JButton("Press");
		add(mButton);
		
		//make button do something
		mButton.addActionListener(
				new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Yo");
			}
		});
		
	}
	
	
	
	
	
}
