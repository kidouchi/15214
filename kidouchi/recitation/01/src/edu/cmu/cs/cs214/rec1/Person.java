package edu.cmu.cs.cs214.rec1;

public class Person {
    private String name;
    private Animal[] pets;
    private int numPets;

    public Person(String name) {
        this.name = name;
        this.pets = new Animal[42];
    }

    public void addPet(Animal newFriend) {
        /*
         * TODO add newFriend to 'pets' array
         */
    	pets[numPets] = newFriend;
    	numPets++;
    }

    public void speak() {
        if (numPets > 0) {
            System.out.println("I am " + name + " and my pets say:");
            for (int i = 0; i < numPets; i++) {
                pets[i].speak();
            }
        } else {
            System.out.println("My name is " + name + ".");
        }
    }
}