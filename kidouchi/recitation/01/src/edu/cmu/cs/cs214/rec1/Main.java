package edu.cmu.cs.cs214.rec1;

public class Main {
    public static void main(String[] args) {
        Animal wolf = new Animal("Ghost", "Bark bark.");
        Person jon = new Person("Jon Snow");
        Animal bat = new Animal("Bob", "rawr");
        
        jon.addPet(wolf);
        jon.addPet(bat);
        jon.speak();
    }
}