package edu.cmu.cs.cs214.rec1;

public class Animal {
    private String name;
    private String sound;

    public Animal(String name, String sound) {
        this.name = name;
        this.sound = sound;
    }

    public void speak() {
        System.out.println(name + ": " + sound);
    }
}