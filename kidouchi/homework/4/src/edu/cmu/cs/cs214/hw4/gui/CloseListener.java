package edu.cmu.cs.cs214.hw4.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

public class CloseListener implements ActionListener {

	private JFrame f;
	
	public CloseListener(JFrame f) {
		this.f = f;
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		f.dispose();
	}
	
	

}
