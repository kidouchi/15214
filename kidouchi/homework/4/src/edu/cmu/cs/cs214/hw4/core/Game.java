package edu.cmu.cs.cs214.hw4.core;

import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

public class Game {

	private static final int MAX_TILES = 7;
	private static final int MAX_SPECIAL_TILES = 3;
	private static final int BOARD_HEIGHT = 15;
	private static final int BOARD_WIDTH = 15;

	private final int NEGATE_TILE = 0;
	private final int LOSE_TILE = 1;
	private final int ZERO_TILE = 2;

	private ArrayList<Player> players;
	private int currentPlayerIndex;

	private ArrayList<Tuple> toPlacePos;
	private ArrayList<LetterTile> toPlaceLetter;
	private ArrayList<LetterTile> toExchange;
	private SpecialTile toPlaceSpecial;

	private Board b;
	private Board temp;
	private Referee r;
	private ArrayList<LetterTile> tilebank;
	private int pass = 0;
	private final ArrayList<GameChangeListener> gameChangeListeners;
	private final ArrayList<HandChangeListener> handChangeListeners;

	// Booleans to keep track of what the player wants to
	// do with their turn
	private boolean wantsToPlaySpecialTile = false;
	private boolean wantsToPlayReg = false;
	private boolean wantsToPass = false;
	private boolean wantsToExchange = false;
	private boolean isBegin = false;
	private boolean wantsToBuy = false;
	private boolean wantsToAddTiles = false;

	public Game() {
		players = new ArrayList<Player>(); // Start Players
		tilebank = new ArrayList<LetterTile>(); // Start Tile Bank
		b = new Board(BOARD_WIDTH, BOARD_HEIGHT); // Start Board
		temp = new Board(BOARD_WIDTH, BOARD_HEIGHT);
		r = new Referee(); // Instantiate Referee
		this.gameChangeListeners = new ArrayList<GameChangeListener>();
		this.handChangeListeners = new ArrayList<HandChangeListener>();
		this.toPlacePos = new ArrayList<Tuple>();
		this.toPlaceLetter = new ArrayList<LetterTile>();
		this.toExchange = new ArrayList<LetterTile>();
		this.toPlaceSpecial = null;
	}

	/**
	 * Setup the game
	 */
	public void setupGame() {
		this.isBegin = true;
		startBoard();
		startTileBank();
	}
	
	/**
	 * Instantiate a new game
	 */
	public void startGame() {
		currentPlayerIndex = 0;
		notifyPlayerChanged(getCurrentPlayer());
	}

	/**
	 * Adds listeners 
	 * 
	 * @param listener relevant to game
	 */
	public void addGameChangeListener(GameChangeListener listener) {
		gameChangeListeners.add(listener);
	}

	/**
	 * Adds listeners
	 * 
	 * @param listener relevant to game
	 */
	public void addHandChangeListener(HandChangeListener listener) {
		handChangeListeners.add(listener);
	}

	/**
	 * Keeps track of what players click as their position
	 * 
	 * @param pos a valid position on board
	 */
	public void addToPlacePos(Tuple pos) {
		toPlacePos.add(pos);
	}
	
	/**
	 * Returns current positions that player clicked
	 * 
	 * @return arraylist of positions
	 */
	public ArrayList<Tuple> getToPlacePos() {
		return this.toPlacePos;
	}

	/**
	 * Keeps track of what letters the player chose
	 * 
	 * @param t a valid letter
	 */
	public void addToPlaceLetter(LetterTile t) {
		toPlaceLetter.add(t);
	}
	
	/**
	 * Returns the list of letters the player chose
	 *  
	 * @return arraylist of letter tiles
	 */
	public ArrayList<LetterTile> getToPlaceLetter() {
		return this.toPlaceLetter;
	}
	
	/**
	 * Keeps track of what special tile player wants to use
	 * 
	 * @return special tile
	 */
	public SpecialTile getToPlaceSpecial() {
		return this.toPlaceSpecial;
	}

	/**
	 * Updates what special tile will be placed
	 * 
	 * @param s is a valid special tile
	 */
	public void setToPlaceSpecial(SpecialTile s) {
		this.toPlaceSpecial = s;
	}

	/** 
	 * Keeps track of the tiles the player wants to exchange
	 * 
	 * @param t is a letter tile
	 */
	public void addToExchange(LetterTile t) {
		toExchange.add(t);
	}
		
	/**
	 * Returns the list of 'about to exchange' tiles
	 * 
	 * @return an arraylist of letter tiles
	 */
	public ArrayList<LetterTile> getToExchange() {
		return this.toExchange;
	}

	
	/** 
	 * Resets everything that the player chose
	 * which means clear the lists that keep track of that
	 */
	public void resetEverything() {
		this.toPlaceSpecial = null;
		this.toPlaceLetter.clear();
		this.toExchange.clear();
		this.toPlacePos.clear();
	}

	/**
	 * Returns the current player
	 * 
	 * @return a valid player in game
	 */
	public Player getCurrentPlayer() {
		return players.get(currentPlayerIndex);
	}

	/**
	 * Returns the list of players
	 * 
	 * @return arraylist of Player
	 */
	public ArrayList<Player> getPlayers() {
		return this.players;
	}

	/**
	 * Switches to the next player and updates 
	 * the game
	 */
	private void switchPlayers() {
		if (players.size() < 2) {
			throw new IllegalStateException("Need at least 2 players");
		}
		currentPlayerIndex = (currentPlayerIndex + 1) % players.size();
		notifyPlayerChanged(getCurrentPlayer());
	}

	// ************************ Notify Functions *****************
	/**
	 * Notifies ScrabblePanel when a move is made
	 *  
	 * @param pos is a tuple list of valid positions in board
	 * @param ts is a list of valid letter tiles
	 * @param s is a valid special tile
	 */
	private void notifyMoveMade(Tuple[] pos, LetterTile[] ts, SpecialTile s) {
		for (GameChangeListener listener : gameChangeListeners) {
			if (pos != null && ts != null) {
				for (int i = 0; i < ts.length; i++) {
					int x = pos[i].getX();
					int y = pos[i].getY();
					listener.squareChanged(x, y, ts[i], null, getCurrentPlayer());
				}
			} else if (pos.length == 1 && s != null) {
				int x = pos[0].getX();
				int y = pos[0].getY();
				listener.squareChanged(x, y, null, s, getCurrentPlayer());
			}
		}
	}

	/**
	 * Notifies PlayerPanel when a player's hand changes
	 * 
	 * @param tarr is a valid letter tile list
	 * @param toAdd is a boolean
	 * @param p is a valid player in game
	 */
	private void notifyHandChanged(LetterTile[] tarr, boolean toAdd, Player p) {
		int index = 0;
		for (HandChangeListener listener : handChangeListeners) {
			for (int i = 0; i < tarr.length; i++) {
				LetterTile t = tarr[i];
				if (!toAdd) {
					index = p.getIndexOfLetterTile(t);
					listener.handChanged(index, t, toAdd);
				} 
				else {
					index = p.getOpenIndex();
					listener.handChanged(index, t, toAdd);
				}
			}
		}
	}

	/**
	 * Notifies PlayerPanel when a player's special hand is changed
	 * 
	 * @param st is valid special tile
	 * @param toAdd is a boolean
	 * @param p is a valid player in game
	 */
	private void notifySpecialHandChanged(SpecialTile st, boolean toAdd,
			Player p) {
		for (HandChangeListener listener : handChangeListeners) {
			int index = p.getIndexOfSpecialTile(st);
			listener.specialHandChanged(index, st, toAdd);
		}
	}
	
	/**
	 * Notifies the PlayerPanel when a player's turn is changed
	 * 
	 * @param p is a valid player in game
	 */
	private void notifyPlayerChanged(Player p) {
		for (HandChangeListener hlistener : handChangeListeners) {
			hlistener.currentPlayerChanged(p);
		}
	}

	/**
	 * Notifies the ScrabblePanel that the game is over
	 * 
	 * @param winner is a valid player in game
	 */
	private void notifyGameEnd(Player winner) {
		for (GameChangeListener listener : gameChangeListeners) {
			listener.gameEnded(winner);
		}
	}

	/**
	 * Returns the winner of the game when the game ends
	 * 
	 * @return the winner 
	 */
	private Player findWinner() {
		int max = 0;
		Player temp = null;
		for (Player p : players) {
			if (p.getPoints() > max) {
				max = p.getPoints();
				temp = p;
			}
		}
		return temp;
	}

	/**
	 * Checks if the game is over
	 */
	private void checkGameEnd() {
		if (pass == 2 * players.size()) {
			notifyGameEnd(findWinner());
		}
	}

	/** 
	 * Returns the board in game
	 * 
	 * @return board
	 */
	public Board getBoard() {
		return this.b;
	}

	/**
	 * Returns the temporary board in game
	 * 
	 * @return board
	 */
	public Board getTempBoard() {
		return this.temp;
	}

	/**
	 * Returns the max amount of letters a player can have
	 * 
	 * @return int
	 */
	public int getMaxHandNum() {
		return MAX_TILES;
	}

	/**
	 * Returns the max amount of special tiles a player can have
	 *  
	 * @return int
	 */
	public int getMaxSpecialTileNum() {
		return MAX_SPECIAL_TILES;
	}

	/**
	 * Returns the referee of the game
	 * (more for testing)
	 * 
	 * @return Referee
	 */
	public Referee getReferee() {
		return this.r;
	}

	/**
	 * Returns the tileholder at a given position
	 * 
	 * @param 0 <= x <= board width
	 * @param 0 <= y <= board height
	 * @return
	 */
	public TileHolder getSquare(int x, int y) {
		return b.getBoard()[x][y];
	}

	/**
	 * @return number of total passes made consecutively during the game
	 */
	public int getNumPass() {
		return this.pass;
	}

	/**
	 * @return number of players in the game
	 */
	public int getNumPlayers() {
		return this.players.size();
	}

	/**
	 * Just turns the following boolean true (So when a button of what move the
	 * player wants to make is pressed)
	 */
	public void wantsToPlaySpecialTile() {
		this.wantsToPlaySpecialTile = true;
	}

	/**
	 * Just turns the following boolean true (So when a button of what move the
	 * player wants to make is pressed)
	 */
	public void wantsToPlayReg() {
		this.wantsToPlayReg = true;
	}

	/**
	 * Just turns the following boolean true (So when a button of what move the
	 * player wants to make is pressed)
	 */
	public void wantsToPass() {
		this.wantsToPass = true;
	}

	/**
	 * Just turns the following boolean true (So when a button of what move the
	 * player wants to make is pressed)
	 */
	public void wantsToExchange() {
		this.wantsToExchange = true;
	}

	/**
	 * Just turns the following boolean true (So when a button of what move the
	 * player wants to make is pressed)
	 */
	public void wantsToBuy() {
		this.wantsToBuy = true;
	}

	/**
	 * Is set to false are the "beginning" of the game is over
	 */
	public void notBegin() {
		this.isBegin = false;
	}

	public void wantsToAdd() {
		this.wantsToAddTiles = true;
	}

	/**
	 * Checks that the player is in the game
	 * 
	 * @param p
	 *            is a valid player
	 * @return true if the player is in the game, false o/w
	 */
	private boolean isPlayerInGame(Player p) {
		return players.contains(p);
	}
	
	private void removeLetterTilesFromPlayer(Player p, LetterTile[] t) {
		for(int i = 0; i < t.length; i++) {
			if(t[i] != null) {
				p.removeTile(t[i]);
			}
			else {
				throw new IllegalArgumentException("Something wrong in 329");
			}
		}
	}

	// ****************************** MOVES ******************************

	/**
	 * Starts the first move in the center of the board
	 * 
	 * @param p
	 *            is a valid player and is the first player to go
	 * @param lt
	 *            is a valid LetterTile array that creates a valid word
	 */
	public void playFirstMove(LetterTile[] lt, Tuple[] pos) {
		if (pos[0].getX() != 7 && pos[0].getY() != 7) {
			throw new IllegalArgumentException("Not the start position");
		}

		Player p = getCurrentPlayer();
		TileHolder[][] tboa = this.temp.getBoard();

		for (int i = 0; i < lt.length; i++) {
			int x = pos[i].getX();
			int y = pos[i].getY();
			tboa[x][y].setLetterTile(lt[i]);
		}
		
		if (r.validMove(lt, pos, tboa, b)) {
			this.b.setBoard(tboa);
			this.temp.setBoard(tboa);
			for (int a = 0; a < lt.length; a++) {
				p.addPoints(lt[a].getPoint());
			}
			notifyMoveMade(pos, lt, null);
			notifyHandChanged(lt, false, p);
			removeLetterTilesFromPlayer(p, lt);
			switchPlayers();
			checkGameEnd();
		} else {
			throw new IllegalStateException("Invalid move. Try Again!");
		}
	}

	/**
	 * 
	 * @param pos
	 * @param tiles
	 * @param p
	 * @param p2
	 * @param tempBoard
	 */
	// Case when player wants to place tiles on the board
	public void regularMove(Tuple[] pos, LetterTile[] tiles) {
		if(pos.length < tiles.length) {
			throw new IllegalArgumentException("Not work");
		}
		
		if (this.isBegin) {
			this.pass = 0;
			// already notifies
			playFirstMove(tiles, pos);
			this.isBegin = false;
		}
		else if (this.wantsToPlayReg) {
			TileHolder[][] tboa = this.temp.getBoard();
			int track = 0;
			this.pass = 0;
			Player p = getCurrentPlayer();
			// Place the tiles on the temporary board
			for (int i = 0; i < pos.length; i++) {
				int x = pos[i].getX();
				int y = pos[i].getY();
				if(tboa[x][y].getLetterTile() == null) {
					if(track < tiles.length) {
						tboa[x][y].setLetterTile(tiles[track]);
						track++;
					}
				}
				SpecialTile t = tboa[x][y].getSpecialTile();
				if (t != null) {
					t.specialAction(p);
				}
			}
			// Check player made valid move
			if (r.validMove(tiles, pos, tboa, b)) {
				this.b.setBoard(tboa);
				this.temp.setBoard(tboa);
				for (int i = 0; i < tiles.length; i++) {
					p.addPoints(tiles[i].getPoint());
				}
				notifyMoveMade(pos, tiles, null);
				notifyHandChanged(tiles, false, p);
				removeLetterTilesFromPlayer(p, tiles);
				checkGameEnd();
				switchPlayers();
			} 
			else {
				throw new IllegalStateException("Not a valid move. Try again.");
			}
			this.wantsToPlayReg = false;
		}
	}

	/**
	 * 
	 * @param pos
	 * @param tempBoard
	 * @param s
	 */
	public void specialTileMove(Tuple pos, SpecialTile s) {
		if (this.wantsToPlaySpecialTile) {
			Tuple[] newpos = new Tuple[1];
			if (s == null) {
				throw new IllegalArgumentException("Not given Special Tile");
			}
			this.pass = 0;
			Player p = getCurrentPlayer();
			TileHolder[][] tboa = this.temp.getBoard();
			if (r.isInBoard(pos, b) && p.containsSpecialTile(s)) {
				tboa[pos.getX()][pos.getY()].setSpecialTile(s);
				this.b.setBoard(tboa);
				this.temp.setBoard(tboa);
				newpos[0] = pos;
				notifyMoveMade(newpos, null, s);
				notifySpecialHandChanged(s, false, p);
				checkGameEnd();
				switchPlayers();
			}
			this.wantsToPlaySpecialTile = false;
		}
	}

	/**
	 * 
	 */
	public void passMove() {
		if (this.wantsToPass) {
			this.pass++;
			this.wantsToPass = false;
			checkGameEnd();
			switchPlayers();
		}
	}

	public void exchangeTileMove(LetterTile t, Player p) {
		if (this.wantsToExchange) {
			LetterTile[] orig = new LetterTile[1];
			LetterTile[] newt = new LetterTile[1];
			this.pass = 0;
			// Get tile from player and put back in bank
			orig[0] = t;
			notifyHandChanged(orig, false, p);
			p.removeTile(t);
			tilebank.add(t);

			// Draw random tile and give to player
			LetterTile random = drawTile();
			newt[0] = random;
			notifyHandChanged(newt, true, p);
			p.addTile(random);
			tilebank.remove(random);
			checkGameEnd();
			switchPlayers();
		}
	}

	/**
	 * 
	 * @param st
	 * @param p
	 */
	public void buySpecialTileMove(SpecialTile st) {
		if (this.wantsToBuy) {
			this.pass = 0;
			Player p = getCurrentPlayer();
			// Purchase the Tile and give to Player
			// buyMove already notifies
			buyMove(p, st);
			checkGameEnd();
			switchPlayers();
			this.wantsToBuy = false;
		}
	}

	/**
	 * Executes the move where the player purchases a SpecialTile
	 * 
	 * @param p
	 *            is a valid player
	 * @param t
	 *            is a SpecialTile the player wants to buy
	 */
	// Was giving me null Error :(
	// I couldn't fix in time and I didn't want to take
	// another late day :(
	private void buyMove(Player p, SpecialTile s) {
		int cost = s.getCost();
		if (p.getPoints() >= cost) {
			if (p.hasSpecialOpening()) {
				p.addSpecialTile(s);
				p.addPoints(-cost);
				notifySpecialHandChanged(s, true, p);
			} else {
				throw new IllegalStateException(
						"Can't Hold Anymore Special Tiles");
			}
		} else {
			throw new IllegalStateException("Can't Afford");
		}
	}

	/**
	 * Adds a new player to the game
	 * 
	 * @param p
	 *            is a valid player
	 */
	public void addPlayer(Player p) {
		if (p == null) {
			throw new IllegalArgumentException("Give a valid player");
		}

		if(players.size() == 0) {
			notifyPlayerChanged(p);
		}

		if (players.size() < 4) {
			players.add(p);
			giveStartingTiles(p);
		}

		else {
			throw new IllegalStateException(
					"Error: Can't have more than 4 players");
		}
	}

	/**
	 * Draws a random tile from the Tile Bank
	 * 
	 * @return random LetterTile from the Tile Bank
	 */
	public LetterTile drawTile() {
		int ranIndex = (int) (tilebank.size() * Math.random());
		LetterTile temp = tilebank.get(ranIndex);
		// Once the tile is drawn, it should be removed from bank
		return temp;
	}

	/** 
	 * Allows player to add a tile to hand
	 * 
	 * @param t valid letter tile
	 */
	public void addTileMove(LetterTile t) {
		Player p = getCurrentPlayer();
		this.pass = 0;
		if(!p.hasLetterOpening()) {
			checkGameEnd();
			switchPlayers();
			throw new IllegalStateException("Can't hold anymore");
		}
		else {
			LetterTile[] temp = new LetterTile[1];
			temp[0] = t;
			notifyHandChanged(temp, true, p);
			p.addTile(t);
		}
	}

	/**
	 * Returns the name of the special tile
	 * 
	 * @param s is a valid special tile
	 * @return string
	 */
	public String getSpecialName(SpecialTile s) {
		if (s != null) {
			int id = s.getId();
			if (id == NEGATE_TILE) {
				return "NEGATE";
			} else if (id == LOSE_TILE) {
				return "LOSE";
			} else if (id == ZERO_TILE) {
				return "ZERO";
			}
		}
		return "";
	}

	/**
	 * Give the player a random hand of 7 tiles at the start of the game
	 * 
	 * @param p
	 *            is a valid player in the game
	 */
	private void giveStartingTiles(Player p) {
		LetterTile[] temp = new LetterTile[MAX_TILES];
		if (!isPlayerInGame(p)) {
			throw new IllegalArgumentException("Lies!You're not in this game");
		}
		for (int i = 0; i < MAX_TILES; i++) {
			LetterTile hold = drawTile();
			temp[i] = hold;
			p.addTile(hold);
			tilebank.remove(hold);
		}
		notifyHandChanged(temp, true, p);
	}

	/**
	 * Creates the initial Tile Bank
	 */
	private void startTileBank() {
		for (int a = 0; a < 9; a++) {
			tilebank.add(new LetterTile('A', 1));
		}
		for (int b = 0; b < 2; b++) {
			tilebank.add(new LetterTile('B', 3));
		}
		for (int c = 0; c < 2; c++) {
			tilebank.add(new LetterTile('C', 3));
		}
		for (int d = 0; d < 4; d++) {
			tilebank.add(new LetterTile('D', 2));
		}
		for (int e = 0; e < 12; e++) {
			tilebank.add(new LetterTile('E', 1));
		}
		for (int f = 0; f < 2; f++) {
			tilebank.add(new LetterTile('F', 4));
		}
		for (int g = 0; g < 3; g++) {
			tilebank.add(new LetterTile('G', 2));
		}
		for (int h = 0; h < 2; h++) {
			tilebank.add(new LetterTile('H', 4));
		}
		for (int i = 0; i < 9; i++) {
			tilebank.add(new LetterTile('I', 1));
		}
		tilebank.add(new LetterTile('J', 8));
		tilebank.add(new LetterTile('K', 5));
		for (int l = 0; l < 4; l++) {
			tilebank.add(new LetterTile('L', 1));
		}
		for (int m = 0; m < 2; m++) {
			tilebank.add(new LetterTile('M', 3));
		}
		for (int n = 0; n < 6; n++) {
			tilebank.add(new LetterTile('N', 1));
		}
		for (int o = 0; o < 8; o++) {
			tilebank.add(new LetterTile('O', 1));
		}
		for (int p = 0; p < 2; p++) {
			tilebank.add(new LetterTile('P', 3));
		}
		tilebank.add(new LetterTile('Q', 10));
		for (int r = 0; r < 6; r++) {
			tilebank.add(new LetterTile('R', 1));
		}
		for (int s = 0; s < 4; s++) {
			tilebank.add(new LetterTile('S', 1));
		}
		for (int t = 0; t < 6; t++) {
			tilebank.add(new LetterTile('T', 1));
		}
		for (int u = 0; u < 4; u++) {
			tilebank.add(new LetterTile('U', 1));
		}
		for (int v = 0; v < 2; v++) {
			tilebank.add(new LetterTile('V', 4));
		}
		for (int w = 0; w < 2; w++) {
			tilebank.add(new LetterTile('W', 4));
		}
		tilebank.add(new LetterTile('X', 8));
		for (int y = 0; y < 2; y++) {
			tilebank.add(new LetterTile('Y', 4));
		}
		tilebank.add(new LetterTile('Z', 10));
	}

	/**
	 * Create fresh new Scrabble Board and temporary board
	 */
	public void startBoard() {
		for (int i = 0; i < BOARD_WIDTH; i++) {
			for (int j = 0; j < BOARD_HEIGHT; j++) {
				b.setTileHolder(new Tuple(i, j), new TileHolder(null, null,
						null));
				temp.setTileHolder(new Tuple(i, j), new TileHolder(null, null,
						null));
			}
		}
	}
}
