package edu.cmu.cs.cs214.hw4;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import edu.cmu.cs.cs214.hw4.core.Game;
import edu.cmu.cs.cs214.hw4.core.Player;
import edu.cmu.cs.cs214.hw4.gui.PlayerPanel;
import edu.cmu.cs.cs214.hw4.gui.ScrabblePanel;

public class Main {
	
	private static final String SCRABBLE = "Scrabble";

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				createAndShowGameBoard();
			}
		});
	}
	
	private static void showDialog(Component component, String title, String message) {
		JOptionPane.showMessageDialog(component, message, title, JOptionPane.INFORMATION_MESSAGE);
	}
	
	private static void createAndShowGameBoard() {
		JFrame frame = new JFrame(SCRABBLE); 
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Game g = new Game();
		g.setupGame();
		showDialog(frame, "Rules of Scrabble", "Welcome to Scrabble! \n\n Please start at the *");
		showDialog(frame, "Directions", "First click on the tiles you wish to place your letters in. Then select the letters in your hand. Finally click Play Move.");
		Player p1 = new Player("Bob", 100, g);
		Player p2 = new Player("Phil", 100, g);
		g.addPlayer(p1);
		g.addPlayer(p2);
		g.startGame();

		
		// Create Scrabble Board Panel
		JPanel outerPanel = new JPanel(new BorderLayout());
		ScrabblePanel gamePanel = new ScrabblePanel(g);
		PlayerPanel pPanel = new PlayerPanel(g);
		outerPanel.add(gamePanel, BorderLayout.CENTER);
		outerPanel.add(pPanel, BorderLayout.SOUTH);
		frame.setContentPane(outerPanel);
		
		// Create Window
		frame.setPreferredSize(new Dimension(600, 1000)); // Take out later
		frame.pack();
		frame.setVisible(true);
	}
	
}
