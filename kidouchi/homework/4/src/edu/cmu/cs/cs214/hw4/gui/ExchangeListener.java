package edu.cmu.cs.cs214.hw4.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

import edu.cmu.cs.cs214.hw4.core.Game;
import edu.cmu.cs.cs214.hw4.core.LetterTile;
import edu.cmu.cs.cs214.hw4.core.Player;

public class ExchangeListener implements ActionListener {
	
	private LetterTile lt;
	private final Game g;
	private Player p;
	private JButton b;
	
	public ExchangeListener(LetterTile lt, Player p, Game g, JButton b) {
		this.lt = lt;
		this.g = g;
		this.b = b;
		this.p = p;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		//g.addToExchange(lt);
		g.exchangeTileMove(lt, p);
		b.setSelected(true);
		b.setEnabled(false);
	}

}
