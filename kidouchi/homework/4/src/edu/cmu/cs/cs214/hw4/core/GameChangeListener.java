package edu.cmu.cs.cs214.hw4.core;

public interface GameChangeListener {
	
	/**
	 * Receives notifications any time a tile on the board changes.
	 * Could be adding LetterTile or SpecialTile.
	 * 
	 * @param 0 <= x <= board width 
	 * @param 0 <= y <= board height
	 * @param t is a valid Tile in Scrabble
	 */
	public void squareChanged(int x, int y, LetterTile lt,
								SpecialTile st, Player p);
	
	
	/**
	 * Called when the game ends, announcing the winner
	 * 
	 * @param winner is the player that won the game
	 */
	public void gameEnded(Player winner);
}
