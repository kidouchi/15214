package edu.cmu.cs.cs214.hw4.core;

//Will force a player to start at 0 points
public class StartAtZeroTile extends SpecialTile {

	private Player p;
	private final int cost = 50;
	
	public StartAtZeroTile(Player p) {
		super(p);
	}
	
	@Override
	public int getCost() {
		return cost;
	}
	
	@Override
	public void specialAction(Player p) {
		if(p == null) {
			throw new IllegalArgumentException("Not a valid player");
		}
		int ppoints = p.getPoints();
		p.addPoints(-ppoints);
	}

	@Override
	public Player whoCanSee() {
		return p;
	}

	@Override
	public int getId() {
		return 2;
	}


}
