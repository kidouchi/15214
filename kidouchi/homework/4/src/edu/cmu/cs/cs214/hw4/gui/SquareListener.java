package edu.cmu.cs.cs214.hw4.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import edu.cmu.cs.cs214.hw4.core.Game;
import edu.cmu.cs.cs214.hw4.core.Tuple;

public class SquareListener implements ActionListener {
	
	private final int x;
	private final int y;
	private final Game g;
	
	public SquareListener(int x, int y, Game g) {
		this.x = x;
		this.y = y;
		this.g = g;
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		g.addToPlacePos(new Tuple(x, y));
	}
}
