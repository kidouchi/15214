package edu.cmu.cs.cs214.hw4.core;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Hashtable;

public class Referee {

	private Hashtable<String, Integer> scrabbleWords = new Hashtable<String, Integer>();

	public Referee() {
		// Instantiate Scrabble Dictionary
		try {
			scrabbleDict();
		} catch (Exception e) {
			System.out.println("Scrabble Hashtable Error");
		}
	}

	/**
	 * Creates the Scrabble Dictionary It contains the list of all possibly
	 * words playabe in Scrabble
	 * @throws Exception
	 */
	private void scrabbleDict() throws Exception {
		String strLine;
		FileInputStream in = new FileInputStream("assets/words.txt");
		DataInputStream dataIn = new DataInputStream(in);
		BufferedReader br = new BufferedReader(new InputStreamReader(dataIn));

		while ((strLine = br.readLine()) != null) {
			strLine = strLine.trim();
			if ((strLine.length() > 0)) {
				scrabbleWords.put(strLine.toUpperCase(), 0);
			}
		}
	}

	/**
	 * Checks the position is in the game board
	 * @param pos is a valid Tuple
	 * @param b is a valid board
	 * @return true if it's in the board, false o/w
	 */
	public boolean isInBoard(Tuple pos, Board b) {
		if(b == null) {
			throw new IllegalArgumentException("Not a valid board");
		}
		return (0 <= pos.getX() && pos.getX() < b.getWidth() && 
				0 <= pos.getY() && pos.getY() < b.getHeight());
	}

	/**
	 * Checks that the word is a valid word in 
	 * Scrabble
	 * @param word is a String
	 * @return true if it's a valid word in Scrabble, false o/w
	 */
	private boolean isWord(String word) {
		if(word == null) {
			throw new IllegalArgumentException("Not a valid string");
		}
		return scrabbleWords.containsKey(word);
	}

	/**
	 * Checks that the letters are placed in readable, consecutive order
	 * 
	 * @param pos is a valid position in board
	 * @return boolean
	 */
	private boolean isReadableOrder(Tuple[] pos) {
		int xprev;
		int xnow;
		int yprev;
		int ynow;
		for(int j = 0; j < pos.length; j++) {
			int x = pos[j].getX();
			int y = pos[j].getY();
		}
		for(int i = 0; i < pos.length-1; i++) {
			if(isVertical(pos)) {
				yprev = pos[i].getY();
				ynow = pos[i+1].getY();
				if((yprev+1) != ynow) {
					return false;
				}
			}
			else if(isHorizontal(pos)) {
				xprev = pos[i].getX();
				xnow = pos[i+1].getX();
				if((xprev+1) != xnow) {
					return false;
				}
			}
		}
		return true;
	}
	
	
	/**
	 * @param temp must be a board where the player's 
	 * "move" has already been placed, but note that this
	 * isn't the final board
	 * @param pos is an array of valid positions on the board
	 * @return the word created from the player's tile in combination
	 * with the tiles already placed on the board
	 */
	private String getFullWord(TileHolder[][] temp, Tuple[] pos,
								LetterTile[] letters) {
		String hold = "";
		int track = 0;
		for(int i = 0; i < pos.length; i++) {
			int x = pos[i].getX();
			int y = pos[i].getY();
			LetterTile test = temp[x][y].getLetterTile();
			hold += test.getLetter();
		}
		return hold;
	}
	
	/** 
	 * Checks the positions are all horizontally aligned
	 * @param pos is a valid array of Tuples
	 * @return true if horizontal, false o/w
	 */
	private boolean isHorizontal(Tuple[] pos) {
		if(pos.length <= 1) {
			return true;
		}
		int y = pos[0].getY();
		for (int i = 1; i < pos.length; i++) {
			if (y != pos[i].getY()) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Checks the positions are all horizontally aligned
	 * @param pos is valid array of Tuples
	 * @return true if vertical, false o/w
	 */
	private boolean isVertical(Tuple[] pos) {
		if(pos.length <= 1) {
			return true;
		}
		int x = pos[0].getX();
		for(int i = 1; i < pos.length; i++) {
			if(x != pos[i].getX()) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Checks that the player made a valid move
	 * @param word is an array of valid LetterTiles
	 * @param pos is an array of valid Tuples
	 * @param temp is a board where the player's move is already placed
	 * @param b is a valid board
	 * @return true if valid move, false o/w
	 */
	// For is word to consider overlap case
	public boolean validMove(LetterTile[] word, Tuple[] pos, 
									TileHolder[][] temp, Board b) {
		if(b == null) {
			throw new IllegalArgumentException("Not a valid board");
		}
		for(int i = 0; i < pos.length; i++) {
			if(!isInBoard(pos[i], b)) {
				return false;
			}
		}
		
		return isWord(getFullWord(temp, pos, word)) && 
				(isReadableOrder(pos));
	}

	/**
	 * Conditions of game over: All players pass twice in a row
	 * @param g is a valid game
	 * @param b is a valid board
	 * @return whether game is over or not
	 */
	public boolean isGameOver(Game g) {
		return g.getNumPass() == 2*g.getNumPlayers();
	}

	/**
	 * Finds the winner, the player with the most points
	 * @param ps is a valid array list of players
	 * @return the player with the most points
	 */
	public Player Winner(ArrayList<Player> ps) {
		int max = 0;
		Player winner = null;

		for (int i = 0; i < ps.size(); i++) {
			if (ps.get(i).getPoints() > max) {
				max = ps.get(i).getPoints();
				winner = ps.get(i);
			}
		}
		return winner;
	}

}
