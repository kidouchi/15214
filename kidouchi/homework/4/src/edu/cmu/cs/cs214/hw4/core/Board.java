package edu.cmu.cs.cs214.hw4.core;

public class Board {
	
	private int bheight;
	private int bwidth; 
	private TileHolder[][] board;
	
	public Board (int bwidth, int bheight) {
		this.bheight = bheight;
		this.bwidth = bwidth;
		this.board = new TileHolder[bwidth][bheight];
	}
	
	/**
	 * Returns height of board
	 * 
	 * @return int
	 */
	public int getHeight() {
		return bheight;
	}
	
	/**
	 * Returns the width of board
	 * 
	 * @return int
	 */
	public int getWidth() {
		return bwidth;
	}
	
	/**
	 * Returns the board
	 * 
	 * @return TileHolder[][]
	 */
	public TileHolder[][] getBoard() {
		return this.board;
	}
	
	/**
	 * Sets the board to a new board
	 * 
	 * @param board is a valid board
	 */
	public void setBoard(TileHolder[][] board) {
		this.board = board;
	}
	
	/**
	 * Sets a tileholder in the board
	 * @param pos is a valid position in board
	 * @param th is a tileholder
	 */
	public void setTileHolder(Tuple pos, TileHolder th) {
		this.board[pos.getX()][pos.getY()] = th; 
	}

}
