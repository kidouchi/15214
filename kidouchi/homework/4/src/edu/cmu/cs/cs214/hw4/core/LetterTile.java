package edu.cmu.cs.cs214.hw4.core;

public class LetterTile extends Tile{
	
	private char letter;
	private int point;
	
	public LetterTile(char letter, int point) {
		this.letter = letter;
		this.point = point;
	}
	
	public char getLetter() {
		return this.letter;
	}
	
	public int getPoint() {
		return this.point;
	}
	

}
