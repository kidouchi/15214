package edu.cmu.cs.cs214.hw4.gui;

import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import edu.cmu.cs.cs214.hw4.core.Game;
import edu.cmu.cs.cs214.hw4.core.Player;

public class AddTileListener implements ActionListener {

	private final Game g;
	private Player p;
	
	
	public AddTileListener(Game g, Player p) {
		this.g = g;
		this.p = p;
	}
	
	private static void showDialog(Component component, String title,
			String message) {
		JOptionPane.showMessageDialog(component, message, title,
				JOptionPane.INFORMATION_MESSAGE);
	}

	
	@Override
	public void actionPerformed(ActionEvent evt) {
		JFrame msgFrame = new JFrame();
		try {
			showDialog(msgFrame, "Instruction", "Keep clicking on 'Add Tile' until you get a full hand" );
			g.addTileMove(g.drawTile());
		}
		catch(IllegalArgumentException e) {
			showDialog(msgFrame, "Done", "You have a full hand now! The next player will now go.");
		}
		catch(IllegalStateException e) {
			showDialog(msgFrame, "Done", "You have a full hand now! The next player will now go.");
		}
 	}

}
