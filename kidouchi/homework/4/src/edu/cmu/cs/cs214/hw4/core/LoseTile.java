package edu.cmu.cs.cs214.hw4.core;

import java.util.ArrayList;

// Exchange a random tile with another player
public class LoseTile extends SpecialTile{
	
	private Player p;
	private final int cost = 10;
	
	public LoseTile(Player p) {
		super(p);
	}

	@Override
	public int getCost() {
		return cost;
	}
	
	@Override
	public void specialAction(Player p) {
		LetterTile[] hand = p.getHand();
		int ran_index = (int) (hand.length* Math.random());
		LetterTile rm = hand[ran_index];
		p.removeTile(rm);;
	}

	@Override
	public Player whoCanSee() {
		return p;
	}

	@Override
	public int getId() {
		return 1;
	}
	
	
}
