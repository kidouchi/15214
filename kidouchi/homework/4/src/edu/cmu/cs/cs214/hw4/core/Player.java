package edu.cmu.cs.cs214.hw4.core;

import java.util.ArrayList;

public class Player {

	private String name;
	private int myPoints;
	private LetterTile[] myTiles;
	private SpecialTile[] sTiles;
	
	public Player(String name, int points, Game g) {
		this.myTiles = new LetterTile[g.getMaxHandNum()];
		this.sTiles = new SpecialTile[g.getMaxSpecialTileNum()];
		this.myPoints = points;
		this.name = name;
	}

	public String getName() {
		return this.name;
	}
	
	public void addPoints(int points) {
		myPoints += points;
	}
	
	public int getPoints() {
		return myPoints;
	}
	
	public void addTile(LetterTile t) {
		for(int i = 0; i < myTiles.length; i++) {
			if(myTiles[i] == null) {
				myTiles[i] = t;
				return;
			}
		}
		throw new IllegalStateException("Error: Player can hold only 7 tiles at a time");
	}
	
	public int getIndexOfLetterTile(LetterTile t) {
		for(int i = 0; i < myTiles.length; i++) {
			if(myTiles[i] != null) {
				if(myTiles[i].getLetter() == t.getLetter()) {
					return i;
				}
			}
		}
		return -1;
	}
	
	public int getOpenIndex() {
		for(int i = 0; i < myTiles.length; i++) {
			if(myTiles[i] == null) {
				System.out.println("WJAT!");
				return i;
			}
		}
		return -1;
	}
	
	public void removeTile(LetterTile t) {
		for(int i = 0; i < myTiles.length; i++) {
			if(myTiles[i] != null) {
				if(myTiles[i].getLetter() == t.getLetter()) {
					myTiles[i] = null;
					return;
				}
			}
		}
		throw new IllegalStateException("Error: Player has no tiles to remove");
	}
	
	public boolean containsLetterTile(LetterTile t) {
		for(int i = 0; i < myTiles.length; i++) {
			if(myTiles[i].getLetter() == t.getLetter()) {
				return true;
			}
		}
		return false;
	}
	
	public boolean hasLetterOpening() {
		for(int i = 0; i < myTiles.length; i++) {
			if(myTiles[i] == null) {
				return true;
			}
		}
		return false;
	}
	
	public boolean hasSpecialOpening() {
		for(int i = 0; i < sTiles.length; i++) {
			if(sTiles[i] == null) {
				return true;
			}
		}
		return false;
	}
	
	public boolean containsSpecialTile(SpecialTile s) {
		for(int i = 0; i < sTiles.length; i++) {
			if(sTiles[i].getId() == s.getId()) {
				return true;
			}
		}
		return false;
	}

	public void addSpecialTile(SpecialTile s) {
		for(int i = 0; i < sTiles.length; i++) {
			if(sTiles[i] == null) {
				sTiles[i] = s;
				return;
			}
		}
		throw new IllegalStateException("Error: Can't add another Special Tile");
	}
	
	public void removeSpecialTile(SpecialTile t) {
		for(int i = 0; i < sTiles.length; i++) {
			if(sTiles[i].getId() == t.getId()) {
				sTiles[i] = null;
				return;
			}
		}
		throw new IllegalStateException("Error: No Special Tiles to remove");
	}
	
	public int getIndexOfSpecialTile(SpecialTile s) {
		for(int i = 0; i < sTiles.length; i++) {
			if(sTiles[i] != null && 
					sTiles[i].getId() == s.getId()) {
				return i;
			}
		}
		return -1;
	}
	
	public LetterTile[] getHand() {
		return myTiles;
	}
	
	public SpecialTile[] getSpecialHand() {
		return sTiles;
	}

}
