package edu.cmu.cs.cs214.hw4.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import edu.cmu.cs.cs214.hw4.core.Game;
import edu.cmu.cs.cs214.hw4.core.GameChangeListener;
import edu.cmu.cs.cs214.hw4.core.LetterTile;
import edu.cmu.cs.cs214.hw4.core.Player;
import edu.cmu.cs.cs214.hw4.core.SpecialTile;

public class ScrabblePanel extends JPanel implements GameChangeListener {
	
	private static final long serialVersionUID = -8831938153490239804L;

	private final Game g;
	private final JButton[][] squares;
	
	private final int gridWidth;
	private final int gridHeight;
	
	public ScrabblePanel(Game g) {
		this.g = g;
		g.addGameChangeListener(this);
		this.gridWidth = g.getBoard().getWidth();
		this.gridHeight = g.getBoard().getHeight();
		this.squares = new JButton[this.gridWidth][this.gridHeight];
		initGui();
	}
	
	private void initGui() {
		setLayout(new BorderLayout());
		add(createBoardPanel(), BorderLayout.CENTER);
	}
	
	private JPanel createBoardPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(this.gridWidth, this.gridHeight));
		panel.setSize(new Dimension(300, 300));
		
		for(int x = 0; x < this.gridWidth; x++) {
			for(int y = 0; y < this.gridHeight; y++) {
				squares[x][y] = new JButton();
				squares[x][y].setPreferredSize(new Dimension(50, 50));
				squares[x][y].setSelected(false);
				squares[x][y].addActionListener(new SquareListener(x, y, g));
				squares[x][y].setText("  ");
				if(x == 7 && y == 7) {
					squares[x][y].setText("  *  ");
				}
				panel.add(squares[x][y]);
			}
 		}
		return panel;
	}


	@Override
	public void squareChanged(int x, int y, LetterTile lt,
				SpecialTile st, Player p) {
		JButton button = squares[x][y];
		if(st != null) {
			char[] charword = g.getSpecialName(st).toCharArray();
			button.setText(charword[0] + "* ");
			button.setSelected(true);
		}
		if (lt != null) {
			button.setText("" + lt.getLetter());
			button.setSelected(true);
		}
	}

	@Override
	public void gameEnded(Player winner) {
		JFrame frame = (JFrame) SwingUtilities.getRoot(this);
		
		if(winner != null) {
			showDialog(frame, "Winner is ...", winner.getName() + "! Congrats!");	
		}
		else {
			showDialog(frame, "Winnder is ...", "Awww it's a draw :(");
		}
		
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				g.startGame();
			}
		});
	}
	
	private static void showDialog(Component component, String title, String message) {
		JOptionPane.showMessageDialog(component, message, title, JOptionPane.INFORMATION_MESSAGE);
	}
	

}
