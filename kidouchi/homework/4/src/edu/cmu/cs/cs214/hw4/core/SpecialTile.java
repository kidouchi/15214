package edu.cmu.cs.cs214.hw4.core;

public abstract class SpecialTile extends Tile {
	
	private Player p;
	
	public SpecialTile(Player p) {
		this.p = p;
	}
	
	/**
	 * Returns cost of  tile
	 * 
	 * @return int
	 */
	abstract public int getCost();
	
	/** 
	 * Returns id of tile
	 * 
	 * @return int
	 */
	abstract public int getId();
	
	/** 
	 * Performs special tile's action
	 * 
	 * @param p1 a valid player in game
	 */
	abstract public void specialAction(Player p1);
	
	/**
	 * Returns a player who can see the special tile on the board
	 * 
	 * @return Player 
	 */
	abstract public Player whoCanSee();
}
