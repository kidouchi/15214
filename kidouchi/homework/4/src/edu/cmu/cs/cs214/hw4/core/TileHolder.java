package edu.cmu.cs.cs214.hw4.core;

public class TileHolder {
	
	private LetterTile lt;
	private SpecialTile st;
	private Player p; // Which player can see this tile if there is 
					  // a special tile placed

	public TileHolder(LetterTile ltile, SpecialTile stile, Player p) {
		this.lt = ltile;
		this.st = stile;
		if(stile != null && p != null) {
			this.p = p;
		}
	}
	
	public LetterTile getLetterTile() {
		return this.lt;
	}
	
	public SpecialTile getSpecialTile() {
		return this.st;
	}
	
	public void setLetterTile(LetterTile lt) {
		this.lt = lt;
	}
	
	public void setSpecialTile(SpecialTile st) {
		this.st = st;
	}
	
	public Player whoCanSee() {
		return this.p;
	}
	
}
