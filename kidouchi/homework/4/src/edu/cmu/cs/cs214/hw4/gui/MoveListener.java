package edu.cmu.cs.cs214.hw4.gui;

import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import edu.cmu.cs.cs214.hw4.core.Game;
import edu.cmu.cs.cs214.hw4.core.LetterTile;
import edu.cmu.cs.cs214.hw4.core.LoseTile;
import edu.cmu.cs.cs214.hw4.core.NegateTile;
import edu.cmu.cs.cs214.hw4.core.Player;
import edu.cmu.cs.cs214.hw4.core.SpecialTile;
import edu.cmu.cs.cs214.hw4.core.StartAtZeroTile;
import edu.cmu.cs.cs214.hw4.core.Tuple;

public class MoveListener implements ActionListener {

	private final Game g;
	private final int whatMove;
	private JButton[] b;

	private final int regMove = 0;
	private final int specialTileMove = 1;
	private final int passMove = 2;
	private final int buyMove = 3;
	private final int exchangeMove = 4;

	public MoveListener(Game g, int whatMove, JButton[] b) {
		this.g = g;
		this.whatMove = whatMove;
		this.b = b;
	}
	
	public void restart() {
		for(int i = 0; i < b.length; i++) {
			b[i].setSelected(false);
		}
	}

	private static void showDialog(Component component, String title,
			String message) {
		JOptionPane.showMessageDialog(component, message, title,
				JOptionPane.INFORMATION_MESSAGE);
	}

	private JPanel createShopPanel(JFrame f) {
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(1, 3));
		Player currP = g.getCurrentPlayer();

		JButton negTile = new JButton();
		negTile.setText("Negate Tile");
		NegateTile newNeg = new NegateTile(currP);
		negTile.addActionListener(new PurchaseListener(newNeg, currP, g, f));

		JButton losTile = new JButton();
		losTile.setText("Lose Tile");
		LoseTile newLos = new LoseTile(currP);
		losTile.addActionListener(new PurchaseListener(newLos, currP, g, f));

		JButton zeroTile = new JButton();
		zeroTile.setText("Zero Tile");
		StartAtZeroTile newZero = new StartAtZeroTile(currP);
		zeroTile.addActionListener(new PurchaseListener(newZero, currP, g, f));

		panel.add(negTile);
		panel.add(losTile);
		panel.add(zeroTile);

		return panel;
	}

	private JPanel createExchangePanel(JFrame f, LetterTile[] lets) {
		JPanel panel = new JPanel();
		JButton[] buttons = new JButton[lets.length];
		panel.setLayout(new GridLayout(2, lets.length));
		Player p = g.getCurrentPlayer();

		for (int i = 0; i < lets.length; i++) {
			LetterTile t = lets[i];
			buttons[i] = new JButton();
			if (t != null) {
				buttons[i].setText("" + t.getLetter());
			} else {
				buttons[i].setText("   ");
			}
			buttons[i].addActionListener(new ExchangeListener(t, p, g,
					buttons[i]));
			panel.add(buttons[i]);
		}

		JButton done = new JButton();
		done.setText("Done");
		done.addActionListener(new CloseListener(f));
		panel.add(done);
		return panel;
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		JFrame errFrame = new JFrame("ERROR!");
		JFrame shopFrame = new JFrame("Special Tile Shop");
		JFrame exchangeFrame = new JFrame("Exchange");

		Player curr = g.getCurrentPlayer();

		ArrayList<LetterTile> pLetters = g.getToPlaceLetter();
		LetterTile[] letters = pLetters
				.toArray(new LetterTile[pLetters.size()]);

		ArrayList<Tuple> pTuples = g.getToPlacePos();
		Tuple[] positions = pTuples.toArray(new Tuple[pTuples.size()]);

		SpecialTile special = g.getToPlaceSpecial();

		if (whatMove == regMove) {
			try{
				g.wantsToPlayReg();
				g.regularMove(positions, letters);
			}
			catch (IllegalArgumentException e) {
				showDialog(errFrame, "Error", "This is an invalid move. Try again");
				restart();
			}
			catch (IllegalStateException e) {
				showDialog(errFrame, "Error", "This is an invalid move. Try again");
				restart();
			}
			
		} else if (whatMove == specialTileMove) {
			try {
				g.wantsToPlaySpecialTile();
				g.specialTileMove(positions[0], special);
			}
			catch(IllegalArgumentException e) {
				showDialog(errFrame, "Error", "This is an invalid move. Try again");
			}
		} else if (whatMove == passMove) {
			g.wantsToPass();
			g.passMove();
		} else if (whatMove == buyMove) {
			g.wantsToBuy();
			showDialog(
					shopFrame,
					"Welcome to the Tile Shop!",
					"Welcome! Look around :D! Do you see what you like?\n\n"
							+ "Negate Tile: Reverses a players point in negative form! Cost: 100 \n\n"
							+ "Lose Tile: This sweet motherload will get rid of a random tile from a \n "
							+ "player and it'll never return! Cost: 10\n\n"
							+ "Start At Zero Tile: This will return a player to 0 points Cost: 50\n"
							+ "Click 'Okay' to choose your purchase. Enjoy!");
			shopFrame.add(createShopPanel(shopFrame));
			shopFrame.pack();
			shopFrame.setVisible(true);
		} else if (whatMove == exchangeMove) {
			g.wantsToExchange();
			LetterTile[] handarr = curr.getHand();
			showDialog(exchangeFrame, "Exchange Time!",
					"Choose all the tiles you wish to exchange and \n"
							+ "and we'll give you new tiles from the bank!");
			exchangeFrame.add(createExchangePanel(exchangeFrame, handarr));
			exchangeFrame.pack();
			exchangeFrame.setVisible(true);
		} 
		g.resetEverything();
	}
}
