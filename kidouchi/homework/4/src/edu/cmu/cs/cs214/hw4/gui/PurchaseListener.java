package edu.cmu.cs.cs214.hw4.gui;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import edu.cmu.cs.cs214.hw4.core.Game;
import edu.cmu.cs.cs214.hw4.core.NegateTile;
import edu.cmu.cs.cs214.hw4.core.Player;
import edu.cmu.cs.cs214.hw4.core.SpecialTile;

public class PurchaseListener implements ActionListener {
	
	private Player p;
	private SpecialTile s;
	private final Game g;
	private JFrame f;
	
	public PurchaseListener(SpecialTile s, Player p, Game g, JFrame f) {
		this.p = p;
		this.s = s;
		this.g = g;
		this.f = f;
	}
	
	private static void showDialog(Component component, String title,
			String message) {
		JOptionPane.showMessageDialog(component, message, title,
				JOptionPane.INFORMATION_MESSAGE);
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		try{
			g.buySpecialTileMove(s);
		}
		catch (IllegalStateException e) {
			JFrame errFrame = new JFrame();
			showDialog(errFrame, "Error", "You either can't afford it or you have no room. \n"
					+ "Please try using a Special Tile or gaining some points.");
		}
		f.dispose();
	}
	

}
