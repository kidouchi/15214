package edu.cmu.cs.cs214.hw4.core;

public interface HandChangeListener {
	
	/**
	 * Receives notification any time a player's hand changes.
	 *  
	 * @param lt is a valid array of LetterTiles the player has
	 */
	public void handChanged(int index, LetterTile t, boolean toAdd);
			
	/**
	 * Receives notification any time a player's hand of 
	 * special tiles changes
	 * 
	 * @param st is a valid SpecialTile
	 */
	public void specialHandChanged(int index, SpecialTile st, boolean toAdd);

	
	public void currentPlayerChanged(Player p);
}
