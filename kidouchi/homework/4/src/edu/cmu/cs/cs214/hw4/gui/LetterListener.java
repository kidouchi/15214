package edu.cmu.cs.cs214.hw4.gui;

import java.awt.Button;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import edu.cmu.cs.cs214.hw4.core.Game;
import edu.cmu.cs.cs214.hw4.core.LetterTile;

public class LetterListener implements ActionListener {
	
	private LetterTile t;
	private final Game g;
	private JButton b;
	
	public LetterListener(LetterTile t, Game g, JButton b) {
		this.g = g;
		this.t = t;
		this.b = b;
	}
	
	public void setLetter(LetterTile t) {
		this.t = t;
	}
	
	public LetterTile getLetterTile () {
		return this.t;
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		g.addToPlaceLetter(t);
		b.setSelected(true);
	}
	
	
}
