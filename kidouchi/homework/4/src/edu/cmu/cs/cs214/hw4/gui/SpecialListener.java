package edu.cmu.cs.cs214.hw4.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import edu.cmu.cs.cs214.hw4.core.Game;
import edu.cmu.cs.cs214.hw4.core.SpecialTile;

public class SpecialListener implements ActionListener {
	
	private SpecialTile s;
	private final Game g;
	
	public SpecialListener(SpecialTile s, Game g) {
		this.s = s;
		this.g = g;
	}

	public void setSpecialTile(SpecialTile s) {
		this.s = s;
	}
	
	public SpecialListener getListener() {
		return this;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		g.setToPlaceSpecial(s);
	}

}
