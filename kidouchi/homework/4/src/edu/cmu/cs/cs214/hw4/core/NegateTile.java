package edu.cmu.cs.cs214.hw4.core;


// The score that a player has is negated
// ie. current score: 10 --> new score: -10
public class NegateTile extends SpecialTile {
	
	private Player p;
	private final int cost = 100;
	
	public NegateTile(Player p) {
		super(p);
	}
	
	@Override
	public int getCost() {
		return cost;
	}
	
	@Override
	public void specialAction(Player p) {
		if(p == null) {
			throw new IllegalArgumentException("Not a valid player");
		}
		int npoints = p.getPoints() * 2; 
		p.addPoints(-npoints);
	}

	@Override
	public Player whoCanSee() {
		return p;
	}

	@Override
	public int getId() {
		return 0;
	}
	
}
