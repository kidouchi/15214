package edu.cmu.cs.cs214.hw4.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import edu.cmu.cs.cs214.hw4.core.Game;
import edu.cmu.cs.cs214.hw4.core.HandChangeListener;
import edu.cmu.cs.cs214.hw4.core.LetterTile;
import edu.cmu.cs.cs214.hw4.core.Player;
import edu.cmu.cs.cs214.hw4.core.SpecialTile;
 
public class PlayerPanel extends JPanel implements HandChangeListener {

	private final Game g;
	private final int maxHandNum;
	private final int maxSpecialHandNum;
	private final JLabel currentHandLabel;
	private final JButton[] tiles;
	private final JButton[] stiles;
	private final JButton[] whatMove;
	
	private final int maxMoves = 6;
	private final int regMove = 0;
	private final int specialTileMove = 1;
	private final int passMove = 2;
	private final int buyMove = 3;
	private final int exchangeMove = 4;
	private final int addMove = 5;
	
	
	private final int PanelHeight = 3;
	private final int PanelWidth = 7;

	public PlayerPanel(Game g) {
		this.g = g;
		g.addHandChangeListener(this);
		this.maxHandNum = g.getMaxHandNum();
		this.maxSpecialHandNum = g.getMaxSpecialTileNum();
		this.tiles = new JButton[maxHandNum];
		this.stiles = new JButton[maxSpecialHandNum];
		this.whatMove = new JButton[maxMoves];
		this.currentHandLabel = new JLabel();
		initGui();
	}
	
	private void initGui() {
		setLayout(new BorderLayout());
		add(currentHandLabel, BorderLayout.NORTH);
		add(createHandPanel(), BorderLayout.SOUTH);
	}
	
	private JPanel createHandPanel() {
		JPanel playPanel = new JPanel();
		playPanel.setLayout(new GridLayout(PanelHeight, PanelWidth));
		JPanel letterPanel = new JPanel();
		letterPanel.setLayout(new GridLayout(1, maxHandNum));
		
		JPanel specialPanel = new JPanel();
		specialPanel.setLayout(new GridLayout(1, maxSpecialHandNum));
		
		JPanel movesPanel = new JPanel();
		movesPanel.setLayout(new GridLayout(1, maxMoves));
		
		
		
		Player p = g.getCurrentPlayer();
		
		LetterTile[] tarr = p.getHand();
		for(int i = 0; i < this.maxHandNum; i++) {
			LetterTile t = tarr[i];
			tiles[i] = new JButton();
			tiles[i].setText(""+t.getLetter());
			tiles[i].addActionListener(new LetterListener(t, g, tiles[i]));
			letterPanel.add(tiles[i]);
		}
		
		SpecialTile[] sarr = p.getSpecialHand();
		for(int j = 0; j < maxSpecialHandNum; j++) {
			SpecialTile s = sarr[j];
			stiles[j] = new JButton();
			if(sarr[j] != null) {
				stiles[j].setText(g.getSpecialName(sarr[j]));
			} 
			else {
				stiles[j].setText("   ");
			}
			stiles[j].addActionListener(new SpecialListener(null, g));
			specialPanel.add(stiles[j]);
		}
		
		for(int k = 0; k < maxMoves; k++) {
			whatMove[k] = new JButton();
			if(k == 5) {
				whatMove[k].addActionListener(new AddTileListener(g, p));
			}
			else {
				whatMove[k].addActionListener(new MoveListener(g, k, tiles));
			}
			movesPanel.add(whatMove[k]);
		}
		
		whatMove[regMove].setText("Play Move");
		whatMove[specialTileMove].setText("Play Special Tile");
		whatMove[passMove].setText("Pass");
		whatMove[buyMove].setText("Buy Special Tile");
		whatMove[exchangeMove].setText("Exchange Tiles");
		whatMove[addMove].setText("Add Tile");
		
		playPanel.add(letterPanel);
		playPanel.add(specialPanel);
		playPanel.add(movesPanel);
		return playPanel;
	}
	
	@Override
	public void handChanged (int index, LetterTile t, boolean toAdd) {
		JButton button = tiles[index];
		LetterListener l = (LetterListener)button.getActionListeners()[0];
		if(!toAdd) {
			button.setText("   ");
			l.setLetter(null);
		}
		else {
			button.setText("" + t.getLetter());
			l.setLetter(t);
		}	
	}
	
	@Override
	public void specialHandChanged(int index, SpecialTile st, boolean toAdd) {
		JButton button = stiles[index];
		SpecialListener s = (SpecialListener)button.getActionListeners()[0];
		if(!toAdd) {
			button.setText("   ");
			s.setSpecialTile(null);
		}
		else {
			Player p = g.getCurrentPlayer();
			button.setText(g.getSpecialName(st));
			s.setSpecialTile(st);
		}
	}

	@Override
	public void currentPlayerChanged(Player p) {
		LetterTile[] larr = p.getHand();
		SpecialTile[] sarr = p.getSpecialHand();
		String name = p.getName();
		int score = p.getPoints();
		currentHandLabel.setText("Current player: " + name + "             Score: " + score);
		
		for(int i = 0; i < larr.length; i++) {
			LetterListener l = (LetterListener)tiles[i].getActionListeners()[0];
			LetterTile t = larr[i];
			tiles[i].setSelected(false);
			if(t == null) {
				tiles[i].setText("   ");
			}
			else {
				tiles[i].setText(""+t.getLetter());
			}
			l.setLetter(t);
		}
		
		for(int j = 0; j < sarr.length; j++) {
			SpecialListener s = (SpecialListener)stiles[j].getActionListeners()[0];
			SpecialTile st = sarr[j];
			stiles[j].setText(g.getSpecialName(st));
			s.setSpecialTile(st);
		}
	}
}
