{\rtf1\ansi\ansicpg1252\cocoartf1187\cocoasubrtf400
{\fonttbl\f0\fswiss\fcharset0 Helvetica;}
{\colortbl;\red255\green255\blue255;}
\margl1440\margr1440\vieww10800\viewh8400\viewkind0
\pard\tx720\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\pardirnatural

\f0\fs24 \cf0 Main.java \
Location : edu.cmu.cs.cs214.hw4\
\
How To Play Scrabble:\
\
There are buttons in which you can decide what move you want to make.\
	(I know it's not intuitive, but I didn't have much time. Sorry!)\
	- Play Move: Click the squares on the board, because this will determine where you will place your letters (Order matters! Click in the order in which you want to place your tiles). Then choose the letters you want to play on the board. (Note: You must also click on the squares that have a letter already, otherwise the game won't know which positions you want to make your word) Then click on 'Play Move'.\
	- Play Special Tile: If you have a Special Tile in your inventory, then you can use this move. Click a spot on the board (One only!) and then click on the Special Tile you want to place. Then click on 'Play Special Tile'\
	- Pass: Just click this button if you want to pass \
	- Buy Special Tile: If you have enough points you can make a Special Tile purchase! Just click on the button and the Special Tile Store will appear. Click 'OK' after reading the description and a new window will appear. Just click on the tile you want to buy.\
	- Exchange Tiles: If you don't like the tiles that you have then you can exchange tiles. Click on 'Exchange Tiles' and a new window will appear. Select all the tiles you want to exchange. You'll be given new random letters.\
	-Add Tile: If you have less than 7 letters, then you can add more tiles to your hand\
\
Game ends when everyone passes twice.}