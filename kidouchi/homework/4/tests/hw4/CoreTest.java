package hw4;


public class CoreTest {
//	private Game g;
//	private Player p;
//	
//	@Before
//	public void setUp() throws Exception {
//		g = new Game();
//		p = new Player("Bob", 0);
//		g.addPlayer(p);
//	}
//
//	@After
//	public void tearDown() throws Exception {
//		// Don't do anything
//	}
//
//	/**
//	 * Test player gets 7 tiles
//	 */
//	@Test
//	public void testPlayerHand() {
//		// System.out.println("START");
//		// for(int i = 0; i < p1.getHandSize(); i++) {
//		// System.out.println(p1.getHand().get(i).getLetter());
//		// }
//		assertEquals(7, p.getHandSize());
//	}
//
//	/**
//	 * Test when adding a tile to player's hand when player already has 7 tiles
//	 */
//	@Test(expected = IllegalStateException.class)
//	public void testPlayerAddTileErr() {
//		p.addTile(new LetterTile('B', 0));
//		assertEquals(7, p.getHandSize());
//	}
//
//	@Test
//	public void testPlayerAddTile() {
//		p.removeTile(p.getHand().get(2));
//		p.addTile(new LetterTile('Z', 10));
//		// System.out.println("HERE");
//		// for(int i = 0; i < p1.getHandSize(); i++) {
//		// System.out.println(p1.getHand().get(i).getLetter());
//		// }
//	}
//
//	/**
//	 * Test when add more than 4 players
//	 */
//	@Test(expected = IllegalStateException.class)
//	public void testAddManyPlayers() {
//		g.addPlayer(p);
//		g.addPlayer(p);
//		g.addPlayer(p);
//		g.addPlayer(p);
//		g.addPlayer(p);
//	}
//
//	@Test
//	public void testStartBoard() {
//		Board b = g.getBoard();
//		TileHolder[][] board = b.getBoard();
//		assertEquals(null, board[0][14].getLetterTile());
//		assertEquals(null, board[0][14].getSpecialTile());
//	}
//
//	@Test
//	public void testValidMove() {
//		Referee r = g.getReferee();
//		LetterTile[] word = { 	new LetterTile('B', 0), 
//								new LetterTile('Y', 0),
//								new LetterTile('E', 0) };
//		Tuple[] pos = { new Tuple(0, 2), 
//						new Tuple(0, 1), 
//						new Tuple(0, 0) };
//		TileHolder[][] board = g.getBoard().getBoard();
//		board[0][2].setLetterTile(word[0]);
//		board[0][1].setLetterTile(word[1]);
//		board[0][0].setLetterTile(word[2]);
//		Board b = g.getBoard();		
//		assertEquals(true, r.validMove(word, pos, board, b));
//	
//	}
//
//	@Test(expected = IllegalArgumentException.class)
//	public void testValidMoveBad() {
//		Board b = g.getBoard();
//		Referee r = g.getReferee();
//		LetterTile[] word = {	new LetterTile('B', 0),
//			 	new LetterTile('Y', 0),
//			 	new LetterTile('E', 0) };
//		Tuple[] pos = {	new Tuple(15,0), 
//						new Tuple(0,1),
//						new Tuple(0,2) };
//		assertEquals(false, r.validMove(word, pos, b.getBoard(), null));
//	}
//	
//	@Test
//	public void testValidMoveBad2() {
//		Board b = g.getBoard();
//		Referee r = g.getReferee();
//		LetterTile[] word = {	new LetterTile('B', 0),
//			 	new LetterTile('Y', 0),
//			 	new LetterTile('E', 0) };
//		Tuple[] pos = {	new Tuple(15,0), 
//						new Tuple(0,1),
//						new Tuple(0,2) };
//		assertEquals(false, r.validMove(word, pos, b.getBoard(), b));
//	}
//	
//	@Test
//	public void testValidMoveBad3() {
//		Board b = g.getBoard();
//		Referee r = g.getReferee();
//		LetterTile[] word = {	new LetterTile('B', 0),
//			 	new LetterTile('Y', 0),
//			 	new LetterTile('Y', 0) };
//		Tuple[] pos = {	new Tuple(15,0), 
//						new Tuple(0,1),
//						new Tuple(0,2) };
//		assertEquals(false, r.validMove(word, pos, b.getBoard(), b));
//	}
//	
//	@Test
//	public void testValidMoveBad4() {
//		Board b = g.getBoard();
//		Referee r = g.getReferee();
//		LetterTile[] word = {	
//				new LetterTile('B', 0),
//			 	new LetterTile('Y', 0),
//			 	new LetterTile('E', 0) };
//		Tuple[] pos = {	new Tuple(15,0), 
//						new Tuple(0,1),
//						new Tuple(0,2) };
//		assertEquals(false, r.validMove(word, pos, b.getBoard(), b));
//	}
//	
//	@Test 
//	public void testWinner() {
//		Referee r = g.getReferee();
//		Player p1 = new Player("Bob", 0);
//		Player p2 = new Player("Suzy", 1);
//		Player p3 = new Player("Tim", 2);
//		Player p4 = new Player("Kim", 3);
//		ArrayList<Player> ps = new ArrayList<Player>();
//		ps.add(p1);
//		ps.add(p2);
//		ps.add(p3);
//		ps.add(p4);
//		assertEquals(p4, r.Winner(ps));
//	}
//	
//	@Test
//	public void coverIsHorizontal() {
//		Tuple[] pos = {	new Tuple(1,1), 
//						new Tuple(2,1), 
//						new Tuple(3,1) };
//		Board b = g.getBoard();
//		Referee r = g.getReferee();
//		LetterTile[] word = {	
//				new LetterTile('B', 0),
//			 	new LetterTile('Y', 0),
//			 	new LetterTile('E', 0) };
//		TileHolder[][] temp = b.getBoard();
//		temp[1][1] = new TileHolder(word[0], null, null);
//		temp[2][1] = new TileHolder(word[1], null, null);
//		temp[3][1] = new TileHolder(word[2], null, null);
//		assertEquals(true, r.validMove(word, pos, temp, b));
//	}
//	
//	@Test
//	public void tileHolderCover() {
//		TileHolder t = new TileHolder(null, null, null);
//		NegateTile ts = new NegateTile();
//		t.setSpecialTile(ts);
//	}
//	
//	@Test(expected = IllegalArgumentException.class)
//	public void makeMoveTestErr() {
//		Tuple[] pos = {	new Tuple(1,1), 
//						new Tuple(2,1) };
//		LetterTile[] word = {	
//				new LetterTile('B', 0),
//			 	new LetterTile('Y', 0),
//			 	new LetterTile('E', 0) };
//		g.makeMove(pos, word, null, p, null);
//	}
//
//	@Test(expected = IllegalArgumentException.class)
//	public void makeMoveTestErr1() {
//		Tuple[] pos = {	new Tuple(1,1), 
//						new Tuple(2,1),
//						new Tuple(3,1)};	
//		LetterTile[] word = {	
//				new LetterTile('B', 0),
//			 	new LetterTile('Y', 0),
//			 	new LetterTile('E', 0) };
//		g.makeMove(pos, word, null, null, null);
//	}
//	
//	@Test
//	public void makeMoveTest1() {
//		g.wantsToPlayReg();
//		g.notBegin();
//		Tuple[] pos = {	new Tuple(1,1), 
//						new Tuple(2,1),
//						new Tuple(3,1)};	
//		LetterTile[] word = {	
//		new LetterTile('B', 0),
//	 	new LetterTile('Y', 0),
//	 	new LetterTile('E', 0) };
//		Player p = new Player("Bob", 10);
//		g.addPlayer(p);
//		Board b = g.getBoard();
//		TileHolder[][] t = b.getBoard();
//		t[1][1].setSpecialTile(new NegateTile());
//		g.makeMove(pos, word, null, p, null);
//		assertEquals(-10, p.getPoints());
//		assertEquals('B', 
//			b.getBoard()[1][1].getLetterTile().getLetter());
//	}
//	
//	@Test
//	public void makeMoveTest2() {
//		g.wantsToPlayReg();
//		g.notBegin();
//		Tuple[] pos = {	new Tuple(1,1), 
//						new Tuple(2,1),
//						new Tuple(3,1)};	
//		LetterTile[] word = {	
//						new LetterTile('B', 0),
//						new LetterTile('Y', 0),
//						new LetterTile('E', 0) };
//		Player p = new Player("Bob", 10);
//		g.addPlayer(p);
//		Board b = g.getBoard();
//		TileHolder[][] t = b.getBoard();
//		t[1][1].setSpecialTile(new StartAtZeroTile());
//		g.makeMove(pos, word, null, p, null);
//		assertEquals(0, p.getPoints());
//	}
//	
//	@Test 
//	public void makeMoveTest3() {
//		g.wantsToPlayReg();
//		g.notBegin();
//		Tuple[] pos = {	new Tuple(1,1), 
//						new Tuple(2,1),
//						new Tuple(3,1)};	
//		LetterTile[] word = {	
//		new LetterTile('B', 0),
//	 	new LetterTile('Y', 0),
//	 	new LetterTile('E', 0) };
//		Player p = new Player("Bob", 10);
//		Player p1 = new Player("Tim", 0);
//		Board b = g.getBoard();
//		TileHolder[][] t = b.getBoard();
//		g.addPlayer(p);
//		g.addPlayer(p1);
//		t[1][1].setSpecialTile(new ExchangeLetterTile());
//		g.makeMove(pos, word, null, p, p1);	
//	}
//	
//	@Test
//	public void makeMoveTest4() {
//		g.wantsToPlayReg();
//		g.notBegin();
//		Tuple[] pos = {	new Tuple(1,1), 
//				new Tuple(2,1),
//				new Tuple(3,1)};	
//		LetterTile[] word = {	
//				new LetterTile('B', 0),
//				new LetterTile('Y', 0),
//				new LetterTile('E', 0) };
//		g.makeMove(pos, word, null, p, null);
//		TileHolder[][] test = g.getBoard().getBoard();
//		assertEquals('B', test[1][1].getLetterTile().getLetter());
//		assertEquals('Y', test[2][1].getLetterTile().getLetter());
//		assertEquals('E', test[3][1].getLetterTile().getLetter());
//	}
//	
//	
//	@Test(expected = IllegalArgumentException.class)
//	public void testBeginGameErr() {
//		Tuple[] pos = {	new Tuple(1,1), 
//						new Tuple(2,1),
//						new Tuple(3,1)};	
//		LetterTile[] word = {	
//				new LetterTile('B', 0),
//				new LetterTile('Y', 0),
//				new LetterTile('E', 0) };
//		
//		g.makeMove(pos, word, null, p, null);
//	}
//	
//	@Test
//	public void testBeginGame() {
//		Tuple[] pos = {	new Tuple(7,7), 
//				new Tuple(8,7),
//				new Tuple(9,7)};	
//		LetterTile[] word = {	
//				new LetterTile('B', 0),
//				new LetterTile('Y', 0),
//				new LetterTile('E', 0) };
//		g.makeMove(pos, word, null, p, null);
//		assertEquals('Y', 
//				g.getBoard().getBoard()[8][7].getLetterTile().getLetter());
//	}
//	
//	@Test
//	public void testPlaySpecial() {
//		g.notBegin();
//		g.wantsToPlaySpecialTile();
//		Tuple[] pos = {new Tuple (0,0)};
//		NegateTile t = new NegateTile(); 
//		g.makeMove(pos, null, t, p, null);
//		assertEquals(true, (g.getBoard().getBoard()[0][0].getSpecialTile()
//								instanceof SpecialTile));
//	}
//	
//	@Test
//	public void testPassNum() {
//		g.wantsToPass();
//		g.notBegin();
//		assertEquals(0, g.getNumPass());
//		Tuple[] pos = {	new Tuple(7,7), 
//				new Tuple(8,7),
//				new Tuple(9,7)};	
//		LetterTile[] word = {	
//				new LetterTile('B', 0),
//				new LetterTile('Y', 0),
//				new LetterTile('E', 0) };
//		g.makeMove(pos, word, null, p, null);
//		assertEquals(1,g.getNumPass());
//	}
//	
//	@Test
//	public void testExchange() {
//		g.wantsToExchange();
//		g.notBegin();
//		Tuple[] pos = {};	
//		LetterTile[] word = {p.getHand().get(1)};
//		g.makeMove(pos, word, null, p, null);
//	}
//	
//	@Test
//	public void testBuy() {
//		g.wantsToBuy();
//		g.notBegin();
//		Tuple[] pos = {};
//		NegateTile nt = new NegateTile();
//	}
//	
	
}
