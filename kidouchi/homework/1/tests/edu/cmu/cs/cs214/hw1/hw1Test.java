package edu.cmu.cs.cs214.hw1;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.cmu.cs.cs214.hw1.graph.AdjacencyListGraph;
import edu.cmu.cs.cs214.hw1.graph.AdjacencyMatrixGraph;
import edu.cmu.cs.cs214.hw1.graph.Algorithms;
import edu.cmu.cs.cs214.hw1.staff.Graph;
import edu.cmu.cs.cs214.hw1.staff.Vertex;

public class hw1Test {

	// For AdjacencyListGraph:
	// The reason there won't be full coverage for the for loop
	// in getNode is because it doesn't necessarily need to complete
	// loop to find the Node given the Vertex
	// Another reason that returning null won't be covered
	// is because there isn't the case where a vertex doesn't have
	// a node (based on how I wrote everything) and null is to
	// satisfy the condition that

	// For AdjacencyMatrixGraph:
	// the getNode and getVertex function are not getting full
	// coverage for similar reasons as getNode from AdjacencyListGraph

	// For Algorithms:
	// Can't get full coverage for getTuple for similar reasons
	// to getNode
	// For commonFriends, the if statement with inArr I d

	private static final int MAX_VERTICES = 20;
	private Graph sampleGraphM;
	private Graph sampleGraphL;
	private Vertex anne, ben, claire, don, paul, mary, philip, tom, jessie,
			steven, emily, kate, robert, lisa;

	/**
	 * Builds the sample graph illustrated in hw1.pdf Appendix A.
	 * 
	 * @param source
	 *            an initially empty {@link Graph}. This method requires that
	 *            source must has MAX_VERTICES set to at least 14.
	 */
	private void BuildSampleGraph(Graph source) {
		source.addVertex(paul);
		source.addVertex(mary);
		source.addVertex(philip);
		source.addVertex(tom);
		source.addVertex(jessie);
		source.addVertex(steven);
		source.addVertex(robert);
		source.addVertex(emily);
		source.addVertex(kate);
		source.addVertex(lisa);
		source.addEdge(paul, mary);
		source.addEdge(mary, philip);
		source.addEdge(philip, tom);
		source.addEdge(philip, jessie);
		source.addEdge(tom, jessie);
		source.addEdge(jessie, steven);
		source.addEdge(jessie, emily);
		source.addEdge(emily, mary);
		source.addEdge(emily, lisa);
		source.addEdge(paul, robert);
		source.addEdge(paul, kate);
		source.addEdge(robert, kate);
		source.addVertex(anne);
		source.addVertex(ben);
		source.addVertex(claire);
		source.addVertex(don);
		source.addEdge(anne, ben);
		source.addEdge(ben, claire);
	}

	@Before
	public void setUp() throws Exception {
		paul = new Vertex("Paul");
		mary = new Vertex("Mary");
		philip = new Vertex("Philip");
		tom = new Vertex("Tom");
		jessie = new Vertex("Jessie");
		steven = new Vertex("Steven");
		emily = new Vertex("Emily");
		kate = new Vertex("Kate");
		robert = new Vertex("Robert");
		lisa = new Vertex("Lisa");
		anne = new Vertex("Anne");
		ben = new Vertex("Ben");
		claire = new Vertex("Claire");
		don = new Vertex("Don");

		sampleGraphM = new AdjacencyMatrixGraph(MAX_VERTICES);
		sampleGraphL = new AdjacencyListGraph(MAX_VERTICES);

		BuildSampleGraph(sampleGraphM);
		BuildSampleGraph(sampleGraphL);
	}

	@After
	public void tearDown() throws Exception {
		// Don't put anything here
	}

	/**
	 * Helper testing method which returns false iff one or more specified
	 * 'checkVertices' do not exist in the Vertices array 'vertices'.
	 * 
	 * @param vertices
	 *            an array of vertices
	 * @param checkVertices
	 *            the vertices to check (google search "java varargs" if you
	 *            don't know what the "..." means)
	 */
	private static boolean contains(Vertex[] vertices, Vertex... checkVertices) {
		for (Vertex check : checkVertices) {
			boolean found = false;
			for (Vertex v : vertices) {
				if (v.equals(check)) {
					found = true;
					break;
				}
			}
			if (!found) {
				return false;
			}
		}
		return true;
	}

	/** Test basic functions of inArr for Adj Matrix*/
	@Test
	public void testInArrMatrix() {
		Vertex[] neighborsM = sampleGraphM.getNeighbors(jessie);
		assertEquals(true, Algorithms.inArr(neighborsM, tom, 4));
		assertEquals(true, Algorithms.inArr(neighborsM, steven, 4));
	}

	/** Test basic functions of inArr for Adj List Graph */
	@Test
	public void testInArrLG() {
		Vertex[] neighborsL = sampleGraphL.getNeighbors(jessie);
		assertEquals(true, Algorithms.inArr(neighborsL, tom, 4));
		assertEquals(true, Algorithms.inArr(neighborsL, steven, 4));
	}

	/** Test basic functions of getNeighbors for Adj Matrix*/
	@Test
	public void testGetNeighborsMatrix() {
		Vertex[] neighbors;
		neighbors = sampleGraphM.getNeighbors(jessie);
		assertEquals(4, neighbors.length);
		assertEquals(true, contains(neighbors, philip, tom, steven, emily));
		neighbors = sampleGraphM.getNeighbors(don);
		assertEquals(0, neighbors.length);
		assertEquals(true, contains(neighbors));
		neighbors = sampleGraphM.getNeighbors(steven);
		assertEquals(1, neighbors.length);
		assertEquals(true, contains(neighbors, jessie));
	}
		
	/** Test basic functions of getNeighbors for List Graph*/
	@Test
	public void testGetNeighborsLG() {
		Vertex[] neighbors;
 		neighbors = sampleGraphL.getNeighbors(jessie);
		assertEquals(4, neighbors.length);
		assertEquals(true, contains(neighbors, philip, tom, steven, emily));
		neighbors = sampleGraphL.getNeighbors(don);
		assertEquals(0, neighbors.length);
		assertEquals(true, contains(neighbors));
		neighbors = sampleGraphL.getNeighbors(steven);
		assertEquals(1, neighbors.length);
		assertEquals(true, contains(neighbors, jessie));
	}

	/** Test basic functions of commonFriends for Matrix*/
	@Test
	public void testCommonFriendsMatrix() {
		Vertex[] friends;
		friends = Algorithms.commonFriends(sampleGraphM, tom, paul);
		assertEquals(true, contains(friends));
		assertEquals(0, friends.length);
		friends = Algorithms.commonFriends(sampleGraphM, philip, emily);
		assertEquals(true, contains(friends, jessie, mary));
		friends = Algorithms.commonFriends(sampleGraphM, jessie, steven);
		assertEquals(true, contains(friends));
	}
	
	/** Test basic functions of commonFriends for List Graph*/
	@Test
	public void testCommonFriendsLG() {
		Vertex[] friends;
		friends = Algorithms.commonFriends(sampleGraphL, tom, paul);
		assertEquals(0, friends.length);
		friends = Algorithms.commonFriends(sampleGraphL, philip, emily);
		assertEquals(true, contains(friends, jessie, mary));
		friends = Algorithms.commonFriends(sampleGraphL, jessie, steven);
		assertEquals(true, contains(friends));
	}

	/** Test basic function of suggestFriend for Matrix */
	@Test
	public void testSuggestFriendMatrix() {
		Vertex test = Algorithms.suggestFriend(sampleGraphM, philip);
		assertEquals(emily, test);
		test = Algorithms.suggestFriend(sampleGraphM, jessie);
		assertEquals(mary, test);
		test = Algorithms.suggestFriend(sampleGraphM, don);
		assertEquals(null, test);
		test = Algorithms.suggestFriend(sampleGraphM, steven);
		assertEquals(emily, test);
	}
	
	/** Test basic function of suggestFriend for List Graph */
	@Test
	public void testSuggestFriendLG() {
		Vertex test1 = Algorithms.suggestFriend(sampleGraphL, philip);
		assertEquals(emily, test1);
		test1 = Algorithms.suggestFriend(sampleGraphL, jessie);
		assertEquals(mary, test1);
		test1 = Algorithms.suggestFriend(sampleGraphL, don);
		assertEquals(null, test1);
		test1 = Algorithms.suggestFriend(sampleGraphL, steven);
		assertEquals(emily, test1);
	}

	/** Test basic function of shortestDistance for Matrix*/
	@Test
	public void testShortestDistanceMatrix() {
		int test = Algorithms.shortestDistance(sampleGraphM, philip, tom);
		assertEquals(1, test);
		test = Algorithms.shortestDistance(sampleGraphM, philip, lisa);
		assertEquals(3, test);
		test = Algorithms.shortestDistance(sampleGraphM, philip, don);
		assertEquals(-1, test);
		test = Algorithms.shortestDistance(sampleGraphM, philip, philip);
		assertEquals(0, test);
	}
	
	/** Test basic function of shortestDistance for List Graph*/
	@Test
	public void testShortestDistanceLG() {
		int test1 = Algorithms.shortestDistance(sampleGraphL, philip, tom);
		assertEquals(1, test1);
		test1 = Algorithms.shortestDistance(sampleGraphL, philip, lisa);
		assertEquals(3, test1);
		test1 = Algorithms.shortestDistance(sampleGraphL, philip, don);
		assertEquals(-1, test1);
		test1 = Algorithms.shortestDistance(sampleGraphL, philip, philip);
		assertEquals(0, test1);
	}

	/** Test basic function of isAdjacent for Matrix */
	@Test
	public void testIsAdjacentMatrix() {
		assertEquals(true, sampleGraphM.isAdjacent(mary, paul));
		assertEquals(false, sampleGraphM.isAdjacent(steven, don));
	}
	
	/** Test basic function of isAdjacent for List Graph*/
	@Test
	public void testIsAdjacentLG() {
		assertEquals(true, sampleGraphL.isAdjacent(mary, paul));
		assertEquals(false, sampleGraphL.isAdjacent(steven, don));
		
	}

	/** Test basic function of Vertex */
	@Test
	public void testVertex() {
		ben.setLabel("Ben");
		assertEquals(true, ben.equals(ben));
		assertEquals(false, ben.equals(anne));
		assertEquals(66667, ben.hashCode());
		Object nothing = new Object();
		assertEquals(false, ben.equals(nothing));
		assertEquals("Ben", ben.toString());
	}

}
