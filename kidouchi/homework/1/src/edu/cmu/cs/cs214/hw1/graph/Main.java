package edu.cmu.cs.cs214.hw1.graph;

import edu.cmu.cs.cs214.hw1.staff.Graph;
import edu.cmu.cs.cs214.hw1.staff.Vertex;

/**
 * This class should contain some test to help ensure the correctness of your
 * implementations.
 * 
 * Run this class to perform tests. A couple of sample tests have been provided.
 * You may are encouraged to add additional tests to ensure the correctness of
 * your graph/algorithm implementations.
 */
public class Main {

	private static final int MAX_VERTICES = 20;
	private Graph sampleGraph;
	private Vertex anne, ben, claire, don, paul, mary, philip, tom, jessie,
			steven, emily, kate, robert, lisa;

	public static void main(String[] args) {
		new Main().runTests();
	}

	public Main() {
		paul = new Vertex("Paul");
		mary = new Vertex("Mary");
		philip = new Vertex("Philip");
		tom = new Vertex("Tom");
		jessie = new Vertex("Jessie");
		steven = new Vertex("Steven");
		emily = new Vertex("Emily");
		kate = new Vertex("Kate");
		robert = new Vertex("Robert");
		lisa = new Vertex("Lisa");
		anne = new Vertex("Anne");
		ben = new Vertex("Ben");
		claire = new Vertex("Claire");
		don = new Vertex("Don");

		// TODO: uncomment one of these two lines
		//sampleGraph = new AdjacencyMatrixGraph(MAX_VERTICES);
		 sampleGraph = new AdjacencyListGraph(MAX_VERTICES);

		 BuildSampleGraph(sampleGraph);
	}

	/**
	 * Builds the sample graph illustrated in hw1.pdf Appendix A.
	 * 
	 * @param source
	 *            an initially empty {@link Graph}. This method requires that
	 *            source must has MAX_VERTICES set to at least 14.
	 */
	private void BuildSampleGraph(Graph source) {
		source.addVertex(paul);
		source.addVertex(mary);
		source.addVertex(philip);
		source.addVertex(tom);
		source.addVertex(jessie);
		source.addVertex(steven);
		source.addVertex(robert);
		source.addVertex(emily);
		source.addVertex(kate);
		source.addVertex(lisa);
		source.addEdge(paul, mary);
		source.addEdge(mary, philip);
		source.addEdge(philip, tom);
		source.addEdge(philip, jessie);
		source.addEdge(tom, jessie);
		source.addEdge(jessie, steven);
		source.addEdge(jessie, emily);
		source.addEdge(emily, mary);
		source.addEdge(emily, lisa);
		source.addEdge(paul, robert);
		source.addEdge(paul, kate);
		source.addEdge(robert, kate);
		source.addVertex(anne);
		source.addVertex(ben);
		source.addVertex(claire);
		source.addVertex(don);
		source.addEdge(anne, ben);
		source.addEdge(ben, claire);
		System.out.println("b".compareTo("a"));
		System.out.println("a".compareTo("b"));
	}

	/**
	 * Performs tests on the {@code sampleGraph} (see hw0.pdf) and prints the
	 * results to the screen.
	 * 
	 * TODO: We won't grade how thoroughly you test your code, but we strongly
	 * recommend that you write some tests of your own to ensure the correctness
	 * of your graph and algorithm implementations!
	 */
	private void runTests() {
		testGetNeighbors();
		testCommonFriends();
		testSuggestFriend();
		testShortestDistance();
		// TODO: add more tests for the rest of your implementations here!
	}

	/**
	 * Perform tests for {@link Graph#getNeighbors(Vertex)}.
	 */
	private void testGetNeighbors() {
		System.out.println("Testing 'getNeighbors'...");
		Vertex[] neighbors;
		neighbors = sampleGraph.getNeighbors(jessie);
		printResult(neighbors.length == 4);
		printResult(contains(neighbors, philip, tom, steven, emily));
		neighbors = sampleGraph.getNeighbors(don);
		printResult(neighbors.length == 0);
		printResult(contains(neighbors));
		neighbors = sampleGraph.getNeighbors(steven);
		printResult(neighbors.length == 1);
		printResult(contains(neighbors, jessie));
	}

	/**
	 * Perform tests for {@link Main#commonFriends(Graph, Vertex, Vertex)}.
	 */
	private void testCommonFriends() {
		System.out.println("Testing 'commonFriends'...");
		Vertex[] friends;
		friends = Algorithms.commonFriends(sampleGraph, tom, paul);
		printResult(friends.length == 0);

		friends = Algorithms.commonFriends(sampleGraph, philip, emily);
		printResult(contains(friends, jessie, mary));

		friends = Algorithms.commonFriends(sampleGraph, jessie, steven);
		printResult(contains(friends));
	}

	private void testSuggestFriend() {
		System.out.println("Testing 'suggestFriend'");
		Vertex test = Algorithms.suggestFriend(sampleGraph, philip);
		printResult(test.equals(emily));
		test = Algorithms.suggestFriend(sampleGraph, jessie);
		printResult(test.equals(mary));
		test = Algorithms.suggestFriend(sampleGraph, don);
		printResult(test == null);
		test = Algorithms.suggestFriend(sampleGraph, steven);
		printResult(test.equals(emily));
	}

	private void testShortestDistance() {
		System.out.println("Testing 'shortestDistance'");
		int test = Algorithms.shortestDistance(sampleGraph, philip, tom);
		printResult(test == 1);
		test = Algorithms.shortestDistance(sampleGraph, philip, lisa);
		printResult(test == 3);
		test = Algorithms.shortestDistance(sampleGraph, philip, don);
		printResult(test == -1);
		test = Algorithms.shortestDistance(sampleGraph, philip, philip);
		printResult(test == 0);
	}

	/**
	 * Helper testing method which returns false iff one or more specified
	 * 'checkVertices' do not exist in the Vertices array 'vertices'.
	 * 
	 * @param vertices
	 *            an array of vertices
	 * @param checkVertices
	 *            the vertices to check (google search "java varargs" if you
	 *            don't know what the "..." means)
	 */
	private static boolean contains(Vertex[] vertices, Vertex... checkVertices) {
		for (Vertex check : checkVertices) {
			boolean found = false;
			for (Vertex v : vertices) {
				if (v.equals(check)) {
					found = true;
					break;
				}
			}
			if (!found) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Helper testing method which prints the test's result to the screen.
	 */
	private static void printResult(boolean result) {
		if (result) {
			System.out.println("PASS");
		} else {
			System.out.println("FAIL");
		}
	}
}
