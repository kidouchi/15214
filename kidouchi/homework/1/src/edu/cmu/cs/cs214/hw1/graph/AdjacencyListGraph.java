package edu.cmu.cs.cs214.hw1.graph;

import edu.cmu.cs.cs214.hw1.staff.Graph;
import edu.cmu.cs.cs214.hw1.staff.Vertex;

public class AdjacencyListGraph implements Graph {
	// begin

	private int max_vertices;
	private Node[] adjList;
	private int adj_num = 0; // Index of adjList

	/**
	 * Class constructor
	 * 
	 * @param max_vertices
	 */
	public AdjacencyListGraph(int max_vertices) {
		this.max_vertices = max_vertices;
		adjList = new Node[max_vertices];
	}

	/**
	 * Adds a vertex to the graph.
	 * 
	 * Precondition: v is not already a vertex in the graph
	 */
	public void addVertex(Vertex v) {
		Node add = new Node(v);
		add.connections = new Vertex[max_vertices];
		add.num_connect = 0;
		adjList[adj_num] = add; // add to adjacency list
		adj_num++; // update list
	}

	/**
	 * Converts the given vertex into a Node
	 * 
	 * Precondition: v is a valid vertex in the graph
	 * 
	 * @param v
	 *            is a vertex in the graph
	 * @return Node version of Vertex v
	 */
	public Node getNode(Vertex v) {
		for (int i = 0; i < adj_num; i++) {
			if (adjList[i].v.getLabel() == v.getLabel()) {
				return adjList[i];
			}
		}
		return null;
	}

	/**
	 * Adds an edge to the graph.
	 * 
	 * Precondition: v1 and v2 are vertices in the graph
	 */
	public void addEdge(Vertex v1, Vertex v2) {
		Node vertex1 = getNode(v1);
		Node vertex2 = getNode(v2);

		vertex1.connections[vertex1.num_connect] = v2;
		vertex1.num_connect++;
		vertex2.connections[vertex2.num_connect] = v1;
		vertex2.num_connect++;
	}

	/**
	 * Check if there is an edge between v1 and v2.
	 * 
	 * Precondition: v1 and v2 are vertices in the graph Postcondition: return
	 * true iff an edge connects v1 and v2
	 */
	public boolean isAdjacent(Vertex v1, Vertex v2) {
		// Just check that vertex v1 has v2 in its list
		Node source = getNode(v1);
		for (int i = 0; i < source.num_connect; i++) {
			if (source.connections[i].getLabel() == v2.getLabel()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Get an array containing all vertices adjacent to v.
	 * 
	 * Precondition: v is a vertex in the graph Postcondition: returns an array
	 * containing all vertices adjacent to v. This method should return an empty
	 * array if v has no neighbors.
	 */
	public Vertex[] getNeighbors(Vertex v) {
		Vertex[] empty = new Vertex[0];
		Node person = getNode(v);
		Vertex[] neighbor = new Vertex[person.num_connect];
		int n_num = 0;
		for (int i = 0; i < person.num_connect; i++) {
			neighbor[n_num] = person.connections[i];
			n_num++;
		}
		if (n_num == 0) {
			return empty;
		}
		else {
			return neighbor;
		}
	}

	/**
	 * Get all vertices in the graph.
	 * 
	 * Postcondition: returns an array containing all vertices in the graph.
	 * This method should return an empty array if the graph has no vertices.
	 */
	public Vertex[] getVertices() {
		Vertex[] vertList = new Vertex[20];
		int vert_num = 0;
		for (int i = 0; i < adj_num; i++) {
			vertList[vert_num] = adjList[i].v;
			vert_num++;
		}
		Vertex[] temp = new Vertex[vert_num];
		for (int j = 0; j < vert_num; j++) {
			temp[j] = vertList[j];
		}
		return temp;
	}
	// end
}
