package edu.cmu.cs.cs214.hw1.graph;

import edu.cmu.cs.cs214.hw1.staff.Graph;
import edu.cmu.cs.cs214.hw1.staff.Vertex;

public class Algorithms {

	// *********************** Algorithms ****************************

	private static Queue q = new Queue(); // A queue

	/**
	 * Will check that vertex already exists in a given array
	 * 
	 * @param arr
	 *            is valid Vertex array
	 * @param v
	 *            is a vertex in the graph
	 * @param arr_num
	 *            is an index for arr
	 * @return true if vertex is in array and false otherwise
	 */
	public static boolean inArr(Vertex[] arr, Vertex v, int arr_num) {
		for (int i = 0; i < arr_num; i++) {
			if (v.getLabel() == arr[i].getLabel()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Will find the tuple associated with the vertex Otherwise it will return
	 * null
	 * 
	 * @param v
	 *            is a vertex in the graph
	 * @param tlist
	 *            is a valid array of Tuples
	 * @return tuple for given vertex v
	 */
	public static Tuple getTuple(Vertex v, Tuple[] tlist) {
		for (int i = 0; i < tlist.length; i++) {
			if (tlist[i].v.getLabel().equals(v.getLabel())) {
				return tlist[i];
			}
		}
		return null;
	}

	/**
	 * Will return a tuple list containing all tuples associated with a vertex
	 * 
	 * @param graph
	 *            contains all the vertices
	 * @return Tuple array containing all Tuple versions of all the vertices
	 */
	public static Tuple[] allTuples(Graph graph) {
		Vertex[] vlist = graph.getVertices(); // List of all vertices in graph
		Tuple[] tlist = new Tuple[20]; // New list to hold tuples
		int t_num = 0; // Index for tlist array
		for (int i = 0; i < vlist.length; i++) {
			Tuple tuple = new Tuple(vlist[i]);
			tuple.dist = 0;
			tlist[t_num] = tuple;
			t_num++;
		}
		return tlist;
	}

	/**
	 * Resets all the values in the tuple, because I'm paranoid and last time I
	 * didn't properly reset my nodes
	 * 
	 * @param tlist
	 *            is an array with tuples
	 */
	public static void resetAllTuples(Tuple[] tlist) {
		for (int i = 0; i < tlist.length; i++) {
			tlist[i] = null;
		}
	}

	/**
	 * This method finds the shortest distance between two vertices. It returns
	 * shortest distance or -1 if the two nodes are not connected.
	 * 
	 * @param graph
	 *            the friends graph
	 * @param a
	 *            the first Giggle+ user
	 * @param b
	 *            the second Giggle+ user
	 */
	public static int shortestDistance(Graph graph, Vertex a, Vertex b) {
		Tuple[] tlist = allTuples(graph); // Tuple list from all the vertices in
											// the graph
		Tuple[] queue = new Tuple[20]; // Queue list
		int q_num = 0; // Keep track of index in queue
		Vertex[] visited = new Vertex[20]; // List of all vertex visited
		int visit_num = 0; // Index in visited array

		// Add source as visited vertex
		visited[visit_num] = a;
		visit_num++;

		// Convert source to a tuple and add to queue
		Tuple tuple = getTuple(a, tlist);
		q.enqueue(tuple, queue, q_num);
		q_num++;

		Tuple hold; // Temporary tuple variable
		int dist = 0;
		Vertex[] neighbors;

		while (!q.isEmptyQueue(q_num)) {
			hold = queue[0];
			queue = q.dequeue(queue, q_num);
			q_num--;
			// If we find distance to target person, then we are done
			if (hold.v.getLabel() == b.getLabel()) {
				resetAllTuples(tlist);
				return hold.dist;
			}
			neighbors = graph.getNeighbors(hold.v);
			for (int i = 0; i < neighbors.length; i++) {
				dist = hold.dist + 1;
				// Only add people we haven't visited into the queue
				if (!inArr(visited, neighbors[i], visit_num)) {
					visited[visit_num] = neighbors[i];
					visit_num++;
					// Set values into the tuple
					Tuple add = getTuple(neighbors[i], tlist);
					add.dist = dist; // update distance
					queue = q.enqueue(add, queue, q_num); // add to queue
					q_num++; // update queue index
				}
			}
		}
		resetAllTuples(tlist);
		return -1;
	}

	/**
	 * Sorts the array alphabetically
	 * 
	 * @param arr
	 *            is a valid Vertex array
	 * @return sorted (alphabetically) array
	 */
	public static Vertex[] sortArr(Vertex[] arr) {
		Vertex[] sort = new Vertex[arr.length];
		int sort_num = 0;
		Vertex hold = null;
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr.length; j++) {
				// arr[i] is later in alphabet
				if (arr[i].getLabel().compareTo(arr[j].getLabel()) > 0) {
					hold = arr[i];
				}
				// earlier in alphabet
				else {
					hold = arr[j];
				}
			}
			sort[sort_num] = hold;
			sort_num++;
		}
		return sort;
	}

	/**
	 * This method is used to find common friends between v1 and v2. This method
	 * should return the vertices in alphabetical order. The result should not
	 * contain any duplicates.
	 * 
	 * @param graph
	 *            the friends graph
	 * @param a
	 *            the first Giggle+ user
	 * @param b
	 *            the second Giggle+ user
	 */
	public static Vertex[] commonFriends(Graph graph, Vertex a, Vertex b) {
		Vertex[] empty = new Vertex[0]; // An empty vertex array
		Vertex[] neighbors_a = graph.getNeighbors(a); // Get all neighbors for
														// vertex a
		Vertex[] neighbors_b = graph.getNeighbors(b); // Get all neighbors for
														// vertex b
		Vertex[] common = new Vertex[20]; // List containing all common
											// neighbors between a and b
		int comm_num = 0; // Index for common array

		// Go through neighbors and compare between vertex a and b
		for (int i = 0; i < neighbors_a.length; i++) {
			for (int j = 0; j < neighbors_b.length; j++) {
				if (neighbors_a[i].getLabel() == neighbors_b[j].getLabel()) {
					if (!inArr(common, neighbors_a[i], comm_num)) {
						common[comm_num] = neighbors_a[i]; // since share common
															// neighbors, then
															// add to common
															// list
						comm_num++;
					}
				}
			}
		}
		// If a and b share no common members so return empty array
		if (comm_num == 0) {
			return empty;
		} else {
			Vertex[] temp = new Vertex[comm_num];
			for (int j = 0; j < comm_num; j++) {
				temp[j] = common[j];
			}
			return sortArr(temp); // Remember to return sorted array
		}
	}

	/**
	 * This method is used to find the person who has the most common friends
	 * with a particular user. Use alphabetical order to break a tie.
	 * 
	 * @param graph
	 *            the friends graph
	 * @param source
	 *            the Giggle+ user in question
	 */
	public static Vertex suggestFriend(Graph graph, Vertex source) {
		Vertex[] people = graph.getVertices();
		int max = 0;
		int arr_len = 0;
		Vertex most_popular = null;

		for (int i = 0; i < people.length; i++) {
			// Don't include yourself
			if (people[i].getLabel() != source.getLabel()) {
				arr_len = commonFriends(graph, people[i], source).length;
				// Look for person with most common friends
				if (arr_len > max) {
					max = arr_len;
					most_popular = people[i];
				} else if (arr_len == max) {
					if (most_popular != null) {
						if (people[i].getLabel().compareTo(
								most_popular.getLabel()) < 0) {
							most_popular = people[i];
						}
					}
				}
			}
		}
		return most_popular;
	}

}
