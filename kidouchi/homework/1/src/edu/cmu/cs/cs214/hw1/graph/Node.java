package edu.cmu.cs.cs214.hw1.graph;

import edu.cmu.cs.cs214.hw1.staff.Vertex;

public class Node {
	Vertex v;
	Vertex[] connections;
	int num_connect;
	int tag;
	
	public Node(Vertex v) {
		this.v = v;
	}

}
