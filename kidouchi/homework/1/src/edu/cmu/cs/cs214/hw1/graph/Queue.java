package edu.cmu.cs.cs214.hw1.graph;

public class Queue {
// begin
	
	//Vertex[] queue = new Vertex[20];
	//int q_num = 0; 
	
	// Checks queue is empty or not
	public boolean isEmptyQueue (int q_num) {
		if(q_num == 0) {
			return true;
		}
		return false;
	}
			
	// Add person to queue
	public Tuple[] enqueue (Tuple add, Tuple[] queue, int q_num) {
		queue[q_num] = add;
		return queue;
	}
	
	// Delete person from queue 
	public Tuple[] dequeue (Tuple[] queue, int q_num) {
		Tuple[] new_queue = new Tuple[20];
		if(!isEmptyQueue(q_num)) {
			for (int i = 1; i < q_num; i++) {
				new_queue[i-1] = queue[i];
			}
			queue = new_queue;
		}
		return queue;
	}

// end	
}
