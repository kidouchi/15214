package edu.cmu.cs.cs214.hw1.graph;

import edu.cmu.cs.cs214.hw1.staff.Graph;
import edu.cmu.cs.cs214.hw1.staff.Vertex;

public class AdjacencyMatrixGraph implements Graph {
	// begin
	
	private int max_vertices; //Max number of vertex in the graph
	private int[][] adjMatrix; //Matrix keeping track of connected vertices
	private Node[] tracker; //List keeping track of all added vertices

	public AdjacencyMatrixGraph(int max_vertices) {
		this.max_vertices = max_vertices;
		adjMatrix = new int[max_vertices][max_vertices]; // 2d adjacency matrix
		tracker = new Node[max_vertices]; // Keep track of all vertices added
	}

	private int tag_track = 1; // Will provide tag number for every added vertex
	private int track_num = 0; // Index for tracker
 
	/**
	 * Adds a vertex to the graph.
	 * 
	 * Precondition: v is not already a vertex in the graph
	 */
	public void addVertex(Vertex v) {
		Node vertex = new Node(v);
		vertex.tag = tag_track;
		tag_track++;
		tracker[track_num] = vertex;
		track_num++;
	}

	/**
	 * Converts vertex into a Node Otherwise it will return null
	 * 
	 * @param v
	 *            is a vertex in the graph
	 * @return Node version of Vertex v
	 */
	public Node getNode(Vertex v) {
		for (int i = 0; i < track_num; i++) {
			if (tracker[i].v.getLabel() == v.getLabel()) {
				return tracker[i];
			}
		}
		return null;
	}

	/**
	 * Adds an edge to the graph.
	 * 
	 * Precondition: v1 and v2 are vertices in the graph
	 */
	public void addEdge(Vertex v1, Vertex v2) {
		Node vertex1 = getNode(v1);
		Node vertex2 = getNode(v2);

		adjMatrix[vertex1.tag][vertex2.tag] = 1;
		adjMatrix[vertex2.tag][vertex1.tag] = 1;
	}

	/**
	 * Check if there is an edge between v1 and v2.
	 * 
	 * Precondition: v1 and v2 are vertices in the graph Postcondition: return
	 * true iff an edge connects v1 and v2
	 */
	public boolean isAdjacent(Vertex v1, Vertex v2) {
		Node vertex1 = getNode(v1);
		Node vertex2 = getNode(v2);

		if (adjMatrix[vertex1.tag][vertex2.tag] == 1) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * It will retrieve the vertex given its tag number Otherwise it will return
	 * null
	 * 
	 * @param tag
	 *            is an integer representing its vertex
	 * @return Vertex represented by the tag
	 */
	public Vertex getVertex(int tag) {
		for (int i = 0; i < track_num; i++) {
			if (tracker[i].tag == tag) {
				return tracker[i].v;
			}
		}
		return null;
	}

	/**
	 * Get an array containing all vertices adjacent to v.
	 * 
	 * Precondition: v is a vertex in the graph Postcondition: returns an array
	 * containing all vertices adjacent to v. This method should return an empty
	 * array if v has no neighbors.
	 */
	public Vertex[] getNeighbors(Vertex v) {
		Node vertex = getNode(v);
		Vertex[] vertex_list = new Vertex[max_vertices];
		int temp_num = 0;
		for (int i = 0; i < max_vertices; i++) {
			if (adjMatrix[vertex.tag][i] == 1) {
				vertex_list[temp_num] = getVertex(i);
				temp_num++;
			}
		}
		Vertex[] temp = new Vertex[temp_num];
		for (int j = 0; j < temp_num; j++) {
			temp[j] = vertex_list[j];
		}
		return temp;
	}

	/**
	 * Get all vertices in the graph.
	 * 
	 * Postcondition: returns an array containing all vertices in the graph.
	 * This method should return an empty array if the graph has no vertices.
	 */
	public Vertex[] getVertices() {
		Vertex[] vlist = new Vertex[track_num];
		for (int i = 0; i < track_num; i++) {
			vlist[i] = tracker[i].v;
		}
		return vlist;
	}
	// end
}
