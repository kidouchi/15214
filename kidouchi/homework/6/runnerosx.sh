#!/bin/bash

# This script will start 4 workers, a master server, and a client, each 
# as its own separate process. This script will likely come in handy as
# you test your code.

# You must run this script from your hw6 project's root directory.

echo "Starting workers server processes...\n"
(java -Dfile.encoding=UTF-8 -classpath bin edu.cmu.cs.cs214.hw6.WorkerServer 15214)&
(java -Dfile.encoding=UTF-8 -classpath bin edu.cmu.cs.cs214.hw6.WorkerServer 15215)&
(java -Dfile.encoding=UTF-8 -classpath bin edu.cmu.cs.cs214.hw6.WorkerServer 15216)&
(java -Dfile.encoding=UTF-8 -classpath bin edu.cmu.cs.cs214.hw6.WorkerServer 15217)&

# Sleep for 5 seconds to let workers start.
sleep 5
echo "Starting master server...\n"
(java -Dfile.encoding=UTF-8 -classpath bin edu.cmu.cs.cs214.hw6.MasterServer 15218 worker1 127.0.0.1 15214 1,4,5,7,9 worker2 127.0.0.1 15215 1,3,6,8,9 worker3 127.0.0.1 15216 2,3,6,8,10 worker4 127.0.0.1 15217 2,4,5,7,10)&

# Sleep for 2 seconds for master to start.
sleep 2
echo "Starting client...\n"
# java -Dfile.encoding=UTF-8 -classpath bin edu.cmu.cs.cs214.hw6.plugin.wordcount.WordCountClient 127.0.0.1 15218
java -Dfile.encoding=UTF-8 -classpath bin edu.cmu.cs.cs214.hw6.plugin.wordprefix.WordPrefixClient 127.0.0.1 15218

# Kill workers and master when done.
kill -9 $(lsof -P | grep ':15214' | awk '{print $2}')
kill -9 $(lsof -P | grep ':15215' | awk '{print $2}')
kill -9 $(lsof -P | grep ':15216' | awk '{print $2}')
kill -9 $(lsof -P | grep ':15217' | awk '{print $2}')
kill -9 $(lsof -P | grep ':15218' | awk '{print $2}')
