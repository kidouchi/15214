package edu.cmu.cs.cs214.hw6;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import edu.cmu.cs.cs214.hw6.plugin.wordcount.WordCountClient;
import edu.cmu.cs.cs214.hw6.plugin.wordprefix.WordPrefixClient;
import edu.cmu.cs.cs214.hw6.util.Log;

/**
 * An abstract client class used primarily for code reuse between the
 * {@link WordCountClient} and {@link WordPrefixClient}.
 */
public abstract class AbstractClient {
    private final String mMasterHost;
    private final int mMasterPort;

    private static final String TAG = "AbstractClient";
    /**
     * The {@link AbstractClient} constructor.
     *
     * @param masterHost The host name of the {@link MasterServer}.
     * @param masterPort The port that the {@link MasterServer} is listening on.
     */
    public AbstractClient(String masterHost, int masterPort) {
        mMasterHost = masterHost;
        mMasterPort = masterPort;
    }

    protected abstract MapTask getMapTask();

    protected abstract ReduceTask getReduceTask();

    public void execute() {
        final MapTask mapTask = getMapTask();
        final ReduceTask reduceTask = getReduceTask();
        Socket socket = null;
        
        try {
        	socket = new Socket(mMasterHost, mMasterPort);
        	// Send the mapTask and reduceTask to MasterServer
        	ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
        	out.writeObject(mapTask);
        	out.writeObject(reduceTask);
        	
        	// The Master Server is done
        	ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
        	in.readObject();
        	
        }
        catch (ClassNotFoundException e) {
        	Log.e(TAG, "Class not found error", e);
        }
        catch (IOException e) {
        	Log.e(TAG, "Could not open server socket on port " + mMasterPort + ".", e);
        }
        finally {
        	try {
        		if (socket != null) {
        			socket.close();
        		}
        	}
        	catch (IOException e) {
        		Log.e(TAG, "I/O error", e);
        	}
        }
    }
}
