package edu.cmu.cs.cs214.hw6.plugin.wordprefix;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import edu.cmu.cs.cs214.hw6.Emitter;
import edu.cmu.cs.cs214.hw6.ReduceTask;

/**
 * The reduce task for a word-prefix map/reduce computation.
 */
public class WordPrefixReduceTask implements ReduceTask {
    private static final long serialVersionUID = 6763871961687287020L;
    private Map<String, Integer> tracker;
    
    @Override
    public void execute(String key, Iterator<String> values, Emitter emitter) throws IOException {
    	tracker = new HashMap<String, Integer>();
    	int max = 0;
    	int count = 0;
    	String maxWord = "";
    	// Keep track of how many times a word appears
    	while(values.hasNext()) {
    		String temp = values.next();
    		if(tracker.containsKey(temp)) {
    			int num = tracker.get(temp);
    			tracker.put(temp, num+1);
    		}
    		else {
    			tracker.put(temp, 1);
    		}
    	}
    	// Find the word that appears most often
    	for(String word : tracker.keySet()) {
    		count = tracker.get(word);
    		if(count >= max) {
    			max = count;
    			maxWord = word;
    		}
    	}
    	emitter.emit(key, maxWord);
    }
    
}
