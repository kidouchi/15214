package hw0;

public class Person {
	String name; // Name of Person
	Person[] connections; // Array of Person's friends
	int num_connect; // Index in connections array
	boolean visited; // Will say if person was visited in graph (For getDistance)
	int dist; // Distance between source from target (For getDistance)
	
	// Class constructor
	public Person(String name) {
		this.name = name;
	}
}

