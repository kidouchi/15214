package hw0;

public class Main {

	public static void main(String[] args) {
		FriendGraph graph = new FriendGraph();
		
		
		/*Person rachel = new Person("Rachel");
		Person ross = new Person("Ross");
		Person ben = new Person("Ben");
		Person kramer = new Person("Kramer");
		Person bob = new Person("Bob");
		Person phil = new Person("Phil");
		Person john = new Person("John");
	*/

		//graph.addPerson(rachel);
		//graph.addPerson(ross);
		//graph.addPerson(ben);
		//graph.addPerson(kramer);
		//graph.addPerson(bob);
		//graph.addPerson(phil);
		//graph.addPerson(john);
		//graph.addFriendship("Rachel", "Ross");
		//graph.addFriendship("Ross", "Ben");
		//graph.addFriendship("Ross", "Phil");
		//graph.addFriendship("Rachel", "Bob");
		//graph.addFriendship("Phil", "John");
		// graph.test();
		/*
		System.out.println("--------------------------");
		System.out.println("--------------------------");
		System.out.println("-----------Graph Distance Testing-----------");
		System.out.println(graph.getDistance("Rachel", "Ross")); // should print
																	// 1
		System.out.println("--------------------------");
		System.out.println(graph.getDistance("Rachel", "Ben")); // should print
																// 2
		System.out.println("--------------------------");
		System.out.println(graph.getDistance("Rachel", "Rachel")); // should
																	// print 0
		System.out.println("--------------------------");
		System.out.println(graph.getDistance("Rachel", "Kramer")); // should
																	// print -1
		System.out.println("--------------------------");
		System.out.println(graph.getDistance("Rachel", "Bob")); // should print
																// 1
		System.out.println("--------------------------");
		System.out.println(graph.getDistance("Rachel", "Phil")); // should print
																	// 2
		System.out.println("--------------------------");
		System.out.println(graph.getDistance("Rachel", "John")); // should print
																	// 3
	*/
		System.out.println("TESTING THAT FAILED!!");
		Person alex = new Person("Alex");
		Person pablo = new Person("Pablo");
		Person charlie = new Person("Charlie");
		Person shannon = new Person("Shannon");
		Person dan = new Person("Dan");

		graph.addPerson(alex);
		graph.addPerson(pablo);
		graph.addPerson(charlie);
		graph.addPerson(shannon);
		graph.addPerson(dan);

		// For test 1
		graph.addFriendship("Alex", "Dan");
		graph.addFriendship("Alex", "Pablo");
		graph.addFriendship("Alex", "Charlie");
		graph.addFriendship("Shannon", "Dan");
		graph.addFriendship("Shannon", "Pablo");
		graph.addFriendship("Shannon", "Charlie");
		/*
		graph.addFriendship("Pablo", "Alex");
		graph.addFriendship("Alex", "Pablo");

		graph.addFriendship("Pablo", "Shannon");
		graph.addFriendship("Shannon", "Pablo");

		graph.addFriendship("Charlie", "Alex");
		graph.addFriendship("Alex", "Charlie");

		graph.addFriendship("Charlie", "Shannon");
		graph.addFriendship("Shannon", "Charlie");

		graph.addFriendship("Dan", "Alex");
		graph.addFriendship("Alex", "Dan");

		graph.addFriendship("Dan", "Shannon");
		graph.addFriendship("Shannon", "Dan");
		*/

		// For test 2
		// graph.addFriendship("Alex", "Shannon");
		// graph.addFriendship("Alex", "Dan");
		// graph.addFriendship("Dan", "Alex");
		// graph.addFriendship("Dan", "Shannon");
		// graph.addFriendship("Dan", "Pablo");
		// graph.addFriendship("Pablo", "Shannon");
		// graph.addFriendship("Pablo", "Dan");

		System.out.println(graph.getDistance("Shannon", "Pablo"));

	}
}
