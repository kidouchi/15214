package hw0;

// Kelly Idouchi (kidouchi@andrew.cmu.edu)
// HW0

public class FriendGraph {
	//begin
	
	private Person[] tracker = new Person[50]; // Keeps track of all people that are added
	private int track_num = 0; // Index in tracker array
	
	// This gets the Person node in the graph given a string
	// If the node doesn't exist, then will return null
	public Person getPerson(String person) {
		for (int i = 0; i < 50; i++) {
			if(tracker[i].name == person) {
				return tracker[i];
			}
		}
		return null;
	}

	// This adds a Person node in graph
	public void addPerson(Person person) {
		tracker[track_num] = person;
		track_num++;
		person.num_connect = 0;
		person.visited = false;
		person.dist = 0;
		person.connections = new Person[50];
	}
	
	// This connects two people in the graph
	public void addFriendship(String friend1, String friend2) {
		// Checks that friend1 and friend2 are in the graph
		if(getPerson(friend1) != null && getPerson(friend2) != null) {
			Person connect1 = getPerson(friend1); 
			Person connect2 = getPerson(friend2);
			// Add friend2 as friend1's friend
			connect1.connections[connect1.num_connect] = connect2;
			connect1.num_connect++;
			// Add friend1 as friend2's friend
			connect2.connections[connect2.num_connect] = connect1;
			connect2.num_connect++;
		}
	}
	
	//-------------------------- Queue Functions ------------------------------
	private Person[] queue = new Person[50];
	int q_num = 0; 
	
	// Checks queue is empty or not
	public boolean isEmptyQueue () {
		if(q_num == 0) {
			return true;
		}
		return false;
	}
			
	// Add person to queue
	public void enqueue (Person add) {
		queue[q_num] = add;
		q_num++;
	}
	
	// Delete person from queue 
	public void dequeue () {
		Person[] new_queue = new Person[50];
		if(!isEmptyQueue()) {
			for (int i = 1; i < 50; i++) {
				new_queue[i-1] = queue[i];
			}
			queue = new_queue;
			q_num--;
		}
	}
	
	// This will reset all the people as having not been visited
	// in the graph
	public void setUnvisited () {
		for(int i = 0; i < track_num; i++) {
			tracker[i].visited = false;
		}
	}
	
	// Finds the shortest distance between two people in the graph
	public int getDistance(String friend1, String friend2) {
		Person connect = getPerson(friend1);
		// Check off starting person as visited
		connect.visited = true;
		enqueue(connect);
		Person hold; // A temporary value to hold a person
		int dist;
		while(!isEmptyQueue()) {
			hold = queue[0];
			dequeue();
			// If we find distance to target person, then we are done
			if (friend2.equals(hold.name)) {
				setUnvisited();
				return hold.dist;
			}
			for(int i = 0; i < hold.num_connect; i++) {
				dist = hold.dist + 1;
				// Only add people we haven't visited into the queue
				if(!hold.connections[i].visited) {
					hold.connections[i].visited = true;
					hold.connections[i].dist = dist;
					enqueue(hold.connections[i]);
				}
			}
		}
		setUnvisited();
		return -1;
	}
		
 //end
}
