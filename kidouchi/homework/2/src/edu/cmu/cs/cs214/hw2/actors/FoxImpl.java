package edu.cmu.cs.cs214.hw2.actors;

import edu.cmu.cs.cs214.hw2.ai.FoxAI;
import edu.cmu.cs.cs214.hw2.staff.interfaces.AbstractAI;
import edu.cmu.cs.cs214.hw2.staff.interfaces.Animal;
import edu.cmu.cs.cs214.hw2.staff.interfaces.Command;
import edu.cmu.cs.cs214.hw2.staff.interfaces.Fox;
import edu.cmu.cs.cs214.hw2.staff.interfaces.World;
import edu.cmu.cs.cs214.hw2.staff.util.Direction;
import edu.cmu.cs.cs214.hw2.staff.util.Location;

public class FoxImpl extends AbstractAI implements Animal, Fox {
	private static final int FOX_MAX_ENERGY = 160;
	private static final int FOX_VIEW_RANGE = 5;
	private static final int FOX_BREED_LIMIT = FOX_MAX_ENERGY * 3 / 4;
	private static final int FOX_COOL_DOWN = 2;
	private static final int FOX_INITIAL_ENERGY = FOX_MAX_ENERGY * 1 / 2;

	private FoxAI FAI;
	private int remainingEnergy;

	private void init(int energy) {
		this.remainingEnergy = energy;
		this.FAI = new FoxAI();
	}

	public FoxImpl(int energy) {
		init(energy);
	}

	public FoxImpl() {
		init(FOX_INITIAL_ENERGY);
	}

	public void act(World world) {
		Command c = FAI.act(world, this);
		if (c != null) {
			c.execute(world, this);
		}
	}

	@Override
	public int getViewRange() {
		return FOX_VIEW_RANGE;
	}

	@Override
	public int getCoolDown() {
		return FOX_COOL_DOWN;
	}

	@Override
	public int getEnergy() {
		return remainingEnergy;
	}

	@Override
	public int getMaxEnergy() {
		return FOX_MAX_ENERGY;
	}

	@Override
	public int getBreedLimit() {
		return FOX_BREED_LIMIT;
	}

	@Override
	public void eat(World world, Direction dir) {
		if (world == null) {
			throw new NullPointerException("World must not be null");
		} else if (dir == null) {
			throw new NullPointerException("Direction must not be null");
		}

		remainingEnergy--;

		Location currLoc = world.getLocation(this);
		Location nextLoc = new Location(currLoc, dir);

		Object food = world.getThing(nextLoc);
		if (world.isValidLocation(nextLoc) && food != null) {
			world.remove(food);
			remainingEnergy += ((RabbitImpl) food).getEnergyValue();
		}

	}

	@Override
	public void move(World world, Direction dir) {
		if (world == null) {
			throw new NullPointerException("World must not be null");
		} else if (dir == null) {
			throw new NullPointerException("Direction must not be null");
		}

		remainingEnergy--;

		Location currLoc = world.getLocation(this);
		Location nextLoc = new Location(currLoc, dir);

		if (world.isValidLocation(nextLoc) && (world.getThing(nextLoc) == null)) {
			world.remove(this);
			world.add(this, nextLoc);
		}
		if (shouldDie(remainingEnergy)) {
			world.remove(this);
		}
	}

	@Override
	public void breed(World world, Direction dir) {
		if (world == null) {
			throw new NullPointerException("World must not be null");
		} else if (dir == null) {
			throw new NullPointerException("Direction must not be null");
		}

		remainingEnergy--; // Each act takes energy

		Location currLoc = world.getLocation(this);
		Location nextLoc = new Location(currLoc, dir);

		if (world.isValidLocation(nextLoc) && world.getThing(nextLoc) == null) {
			FoxImpl baby = new FoxImpl(remainingEnergy / 2); // Give baby have
																// of the
																// mother's
																// energy
			remainingEnergy = remainingEnergy / 2; // The mother's energy is cut
													// in half because of baby
			world.add(baby, nextLoc);
		}

		if (shouldDie(remainingEnergy)) {
			world.remove(this);
		}
	}

	// end
}