package edu.cmu.cs.cs214.hw2.staff.interfaces;


/**
 * An Alien is a non-{@link Edible} {@link Animal}
 *
 */
public interface Alien extends Animal {
}
