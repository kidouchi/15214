package edu.cmu.cs.cs214.hw2.actors;

import edu.cmu.cs.cs214.hw2.ai.RabbitAI;
import edu.cmu.cs.cs214.hw2.staff.interfaces.AbstractAI;
import edu.cmu.cs.cs214.hw2.staff.interfaces.Animal;
import edu.cmu.cs.cs214.hw2.staff.interfaces.Command;
import edu.cmu.cs.cs214.hw2.staff.interfaces.Edible;
import edu.cmu.cs.cs214.hw2.staff.interfaces.Rabbit;
import edu.cmu.cs.cs214.hw2.staff.interfaces.World;
import edu.cmu.cs.cs214.hw2.staff.util.Direction;
import edu.cmu.cs.cs214.hw2.staff.util.Location;

public class RabbitImpl extends AbstractAI implements Animal, Edible, Rabbit {
	private static final int RABBIT_MAX_ENERGY = 20;
	private static final int RABBIT_VIEW_RANGE = 3;
	private static final int RABBIT_BREED_LIMIT = RABBIT_MAX_ENERGY * 3 / 4;
	private static final int RABBIT_ENERGY_VALUE = 20;
	private static final int RABBIT_COOL_DOWN = 4;
	private static final int RABBIT_INITIAL_ENERGY = RABBIT_MAX_ENERGY * 1 / 2;

	private RabbitAI RAI;
	private int remainingEnergy;

	private void init(int energy) {
		this.remainingEnergy = energy;
		this.RAI = new RabbitAI();
	}

	public RabbitImpl(int energy) {
		init(energy);
	}

	public RabbitImpl() {
		init(RABBIT_INITIAL_ENERGY);
	}

	/**
	 * Returns the amount of energy that the {@link Animal} has remaining.
	 * 
	 * @return An integer greater than 0 if alive; an integer less than or equal
	 *         to 0 otherwise if dead.
	 */
	public int getEnergy() {
		return remainingEnergy;
	}

	/**
	 * Returns the Edible object's energy value. Energy values must be greater
	 * than 0.
	 * 
	 * @return An integer representing the object's energy value.
	 */
	public int getEnergyValue() {
		return RABBIT_ENERGY_VALUE;
	}

	/**
	 * Returns the max amount of energy this {@link Animal} can have.
	 * 
	 * @return An integer representing the max energy an {@link Animal} can
	 *         have.
	 */
	public int getMaxEnergy() {
		return RABBIT_MAX_ENERGY;
	}

	/**
	 * Returns the threshold of energy required to be able to breed.
	 * 
	 * @return An integer representing the amount of energy required to breed.
	 */
	public int getBreedLimit() {
		return RABBIT_BREED_LIMIT;
	}

	/**
	 * Allows the actor to choose and execute an action affecting the world.
	 * 
	 * @param world
	 *            The world that the actor is currently in.
	 * 
	 * @throws NullPointerException
	 *             If world is null.
	 */
	public void act(World world) {
		Command c = RAI.act(world, this);
		if (c != null) {
			c.execute(world, this);
		}
	}

	/**
	 * How far can this actor see?
	 * 
	 * @return An integer which represents the number of cells the actor can see
	 *         around themselves.
	 */
	public int getViewRange() {
		return RABBIT_VIEW_RANGE;
	}

	/**
	 * How frequently can this actor act?
	 * 
	 * @return An integer representing the number of steps this actor must wait
	 *         before acting again.
	 */
	public int getCoolDown() {
		return RABBIT_COOL_DOWN;
	}

	/**
	 * Consumes an Actor located at the space adjacent to this Animal in the
	 * specified direction.
	 * 
	 * @param world
	 *            The world containing this actor.
	 * @param dir
	 *            The direction of the Actor to eat.
	 * 
	 * @throws NullPointerException
	 *             If world or dir are null.
	 */
	public void eat(World world, Direction dir) {
		if (world == null) {
			throw new NullPointerException("World must not be null.");
		} else if (dir == null) {
			throw new NullPointerException("Direction must not be null.");
		}

		remainingEnergy--;

		Location currLoc = world.getLocation(this);
		Location nextLoc = new Location(currLoc, dir);

		Object grass = world.getThing(nextLoc);
		if (world.isValidLocation(nextLoc) && grass != null) {
			world.remove(grass);
			remainingEnergy += ((Grass) grass).getEnergyValue();
		}
		if (shouldDie(remainingEnergy)) {
			world.remove(this);
		}

	}

	/**
	 * Moves the Animal one space in the world in the specified direction.
	 * 
	 * @param world
	 *            The world containing this actor.
	 * @param dir
	 *            The direction to move in.
	 * 
	 * @throws NullPointerException
	 *             If world or dir are null.
	 */
	public void move(World world, Direction dir) {
		if (world == null) {
			throw new NullPointerException("World must not be null");
		} else if (dir == null) {
			throw new NullPointerException("Direction must not be null");
		}

		remainingEnergy--;

		Location currLoc = world.getLocation(this);
		Location nextLoc = new Location(currLoc, dir);

		if (world.isValidLocation(nextLoc) && (world.getThing(nextLoc) == null)) {
			world.remove(this);
			world.add(this, nextLoc);
		}
		if (shouldDie(remainingEnergy)) {
			world.remove(this);
		}
	}

	/**
	 * Breeds a new animal of the exact type as the current Animal. The new
	 * Animal will spawn in the square adjacent to this Animal in the specified
	 * direction. When an animal breeds, it transfers 50% of its energy into its
	 * offspring's energy.
	 * 
	 * @param world
	 *            The world containing this actor.
	 * @param dir
	 *            The direction in which the new Animal will spawn.
	 * 
	 * @throws NullPointerException
	 *             If world or dir are null.
	 */
	public void breed(World world, Direction dir) {
		if (world == null) {
			throw new NullPointerException("World must not be null.");
		} else if (dir == null) {
			throw new NullPointerException("Direction must not be null.");
		}

		remainingEnergy--;

		Location currLoc = world.getLocation(this);
		Location nextLoc = new Location(currLoc, dir);

		if (world.isValidLocation(nextLoc) && world.getThing(nextLoc) == null) {
			RabbitImpl baby = new RabbitImpl(remainingEnergy / 2);
			remainingEnergy = remainingEnergy / 2;
			world.add(baby, nextLoc);
		}
		if (shouldDie(remainingEnergy)) {
			world.remove(this);
		}

	}

	// end
}
