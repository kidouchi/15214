package edu.cmu.cs.cs214.hw2.actors;

import edu.cmu.cs.cs214.hw2.ai.AlienAI;
import edu.cmu.cs.cs214.hw2.staff.interfaces.Alien;
import edu.cmu.cs.cs214.hw2.staff.interfaces.Animal;
import edu.cmu.cs.cs214.hw2.staff.interfaces.Command;
import edu.cmu.cs.cs214.hw2.staff.interfaces.Fox;
import edu.cmu.cs.cs214.hw2.staff.interfaces.World;
import edu.cmu.cs.cs214.hw2.staff.util.Direction;
import edu.cmu.cs.cs214.hw2.staff.util.Location;
import edu.cmu.cs.cs214.hw2.staff.util.Util;

public class AlienImpl implements Animal, Alien {

	private static final int ALIEN_VIEW_RANGE = 1;
	private static final int ALIEN_COOL_DOWN = 100;

	private AlienAI AAI;

	public AlienImpl() {
		this.AAI = new AlienAI();
	}

	@Override
	public void act(World world) {
		Command c = AAI.act(world, this);
		if (c != null) {
			c.execute(world, this);
		}

	}

	@Override
	public int getViewRange() {
		return ALIEN_VIEW_RANGE;
	}

	@Override
	public int getCoolDown() {
		return ALIEN_COOL_DOWN;
	}

	@Override
	public int getEnergy() {
		return 0;
	}

	@Override
	public int getMaxEnergy() {
		return 0;
	}

	@Override
	public int getBreedLimit() {
		return 0;
	}

	@Override
	public void eat(World world, Direction dir) {
		// Shouldn't need to eat
	}

	@Override
	// The alien never moves
	public void move(World world, Direction dir) {
		if (world == null) {
			throw new NullPointerException("World must not be null.");
		} else if (dir == null) {
			throw new NullPointerException("Direction must not be null.");
		}

		Location nextLoc = Util.randomEmptyLoc(world);
		Object fox = world.getThing(nextLoc);
		if (world.isValidLocation(nextLoc) && fox instanceof Fox) {
			world.remove(fox);
			// move fox to random location
			world.add(fox, nextLoc);
		}
	}

	@Override
	public void breed(World world, Direction dir) {
		// shouldn't breed
	}

}
