package edu.cmu.cs.cs214.hw2.ai;

import edu.cmu.cs.cs214.hw2.actors.Grass;
import edu.cmu.cs.cs214.hw2.actors.RabbitImpl;
import edu.cmu.cs.cs214.hw2.commands.BreedCommand;
import edu.cmu.cs.cs214.hw2.commands.EatCommand;
import edu.cmu.cs.cs214.hw2.commands.MoveCommand;
import edu.cmu.cs.cs214.hw2.staff.interfaces.AI;
import edu.cmu.cs.cs214.hw2.staff.interfaces.AbstractAI;
import edu.cmu.cs.cs214.hw2.staff.interfaces.Actor;
import edu.cmu.cs.cs214.hw2.staff.interfaces.Command;
import edu.cmu.cs.cs214.hw2.staff.interfaces.Fox;
import edu.cmu.cs.cs214.hw2.staff.interfaces.World;
import edu.cmu.cs.cs214.hw2.staff.util.Direction;
import edu.cmu.cs.cs214.hw2.staff.util.Location;

/**
 * The Rabbit AI. It will move towards grass, if it sees a grass beside it then
 * eat it, breed when it has enough energy, or move around.
 * 
 */
public class RabbitAI extends AbstractAI implements AI {

	public RabbitAI() {
	}

	/**
	 * Return opposite direction from enemy but run towards grass
	 * 
	 * @param boundary
	 *            is array with valid locations in the world
	 * @param world
	 *            is a valid world
	 * @return returns direction opposite of enemy
	 */
	public Direction runAway(Location[] boundary, World world, Location current) {
		if (world == null) {
			throw new NullPointerException("World must not be null");
		}
		if (!world.isValidLocation(current)) {
			throw new IllegalArgumentException("Invalid location");
		}

		int close = 3;
		for (int i = 0; i < boundary.length; i++) {

			if (world.getThing(boundary[i]) instanceof Fox
					&& current.distanceTo(boundary[i]) < close) {
				return oppositeDir(current.dirTo(boundary[i]));
			}
		}
		return null;
	}

	/**
	 * Provides the brain of the Rabbit. The details are explained above the
	 * class. It returns a command for the Rabbit to execute
	 * 
	 * @param world
	 *            is a valid world
	 * @param actor
	 *            is valid and can exist in the world
	 */
	@Override
	public Command act(World world, Actor actor) {
		if (world == null) {
			throw new NullPointerException("World can't be null");
		}
		if (actor == null) {
			throw new IllegalArgumentException("Actor is invalid");
		}
		Direction[] allDirections = Direction.values(); // All possible
														// directions
		Location current = world.getLocation(actor); // current position of
														// actor
		int in_range = actor.getViewRange(); // the view range of the actor
		Location[] boundary = viewBounds(current, in_range, world); // All the
																	// locations
																	// that the
																	// actor can
																	// see
		Direction go_this_way = runAway(boundary, world, current); // A
																	// direction
																	// away
																	// from
																	// enemy
																	// or
																	// towards
																	// grass
		RabbitImpl rabbit_actor = (RabbitImpl) actor;
		int energy = rabbit_actor.getEnergy(); // Gets the current energy of the
												// rabbit
		int breed_limit = rabbit_actor.getBreedLimit(); // Gets the rabbit's
														// breed limit
		int max_energy = rabbit_actor.getMaxEnergy(); // Gets the rabbit's max
														// energy
		Location adjPos; // An adjacent position to the current position
		Object beside;

		for (int i = 0; i < allDirections.length; i++) {
			adjPos = new Location(current, allDirections[i]);
			if (world.isValidLocation(adjPos)) {
				beside = world.getThing(adjPos);
				// Check for grass
				if (beside instanceof Grass && energy < max_energy) {
					return new EatCommand(allDirections[i]);
				}
				// Either run away from enemy or run closer to grass
				else if (go_this_way != null) {
					return new MoveCommand(go_this_way);
				}
				// If energy is enough to breed then breed
				else if (energy >= breed_limit && beside == null) {
					return new BreedCommand(allDirections[i]);
				}
			}
		}
		// Worst case just move in random direction
		int dir = (int) (Math.random() * allDirections.length);
		return new MoveCommand(allDirections[dir]);

	}

	// end
}
