package edu.cmu.cs.cs214.hw2.ai;

import edu.cmu.cs.cs214.hw2.commands.MoveCommand;
import edu.cmu.cs.cs214.hw2.staff.interfaces.AI;
import edu.cmu.cs.cs214.hw2.staff.interfaces.AbstractAI;
import edu.cmu.cs.cs214.hw2.staff.interfaces.Actor;
import edu.cmu.cs.cs214.hw2.staff.interfaces.Command;
import edu.cmu.cs.cs214.hw2.staff.interfaces.Fox;
import edu.cmu.cs.cs214.hw2.staff.interfaces.World;
import edu.cmu.cs.cs214.hw2.staff.util.Direction;
import edu.cmu.cs.cs214.hw2.staff.util.Location;

/**
 * The Alien AI. This strange creature doesn't eat move or breed. All it does
 * is... Make Brand New Fox Babies!!!! :D
 */
public class AlienAI extends AbstractAI implements AI {

	public AlienAI() {
	}

	/**
	 * The details of this function is described above the class. It returns a
	 * command for the Alien to execute.
	 */
	@Override
	public Command act(World world, Actor actor) {
		Location current = world.getLocation(actor);
		Direction[] allDirection = Direction.values();
		Location adjPos;

		for (int i = 0; i < allDirection.length; i++) {
			adjPos = new Location(current, allDirection[i]);
			// Check for adjacent fox
			if (world.isValidLocation(adjPos)
					&& world.getThing(adjPos) instanceof Fox) {
				return new MoveCommand(allDirection[i]);
			}
		}
		return null;
	}

}
