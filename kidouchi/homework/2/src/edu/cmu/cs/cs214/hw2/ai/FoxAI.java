package edu.cmu.cs.cs214.hw2.ai;

import edu.cmu.cs.cs214.hw2.actors.FoxImpl;
import edu.cmu.cs.cs214.hw2.actors.RabbitImpl;
import edu.cmu.cs.cs214.hw2.commands.BreedCommand;
import edu.cmu.cs.cs214.hw2.commands.EatCommand;
import edu.cmu.cs.cs214.hw2.commands.MoveCommand;
import edu.cmu.cs.cs214.hw2.staff.interfaces.AI;
import edu.cmu.cs.cs214.hw2.staff.interfaces.AbstractAI;
import edu.cmu.cs.cs214.hw2.staff.interfaces.Actor;
import edu.cmu.cs.cs214.hw2.staff.interfaces.Command;
import edu.cmu.cs.cs214.hw2.staff.interfaces.Rabbit;
import edu.cmu.cs.cs214.hw2.staff.interfaces.World;
import edu.cmu.cs.cs214.hw2.staff.util.Direction;
import edu.cmu.cs.cs214.hw2.staff.util.Location;

/**
 * 
 * The Fox AI. This AI will eat Rabbits if it's within view, move towards
 * rabbits, breed if it has enough energy, or otherwise move around. (It's not a
 * very impressive AI)
 * 
 */
public class FoxAI extends AbstractAI implements AI {
	// begin

	public FoxAI() {
	}

	/**
	 * Will find nearest enemy to hunt
	 * 
	 * @param boundary
	 *            is an array of valid locations in the world
	 * @param world
	 *            is a valid world
	 * @param current
	 *            is a valid location in the world
	 * @return the nearest rabbit to the world
	 */
	public Direction shouldChase(Location[] boundary, World world,
			Location current) {
		if (world == null) {
			throw new NullPointerException("World must not be null");
		}
		if (!world.isValidLocation(current)) {
			throw new IllegalArgumentException("Invalid location");
		}

		int close_enough = 3;
		for (int i = 0; i < boundary.length; i++) {
			// lower view range of fox
			if (world.isValidLocation(boundary[i])
					&& world.getThing(boundary[i]) instanceof RabbitImpl
					&& current.distanceTo(boundary[i]) <= close_enough) {
				return current.dirTo(boundary[i]);
			}
		}
		return null;
	}

	/**
	 * If there is food adjacent to animal then return the direction of it
	 */
	public Direction getRabbit(Location current, World world) {
		Direction[] allDirections = Direction.values(); // all directions to
														// face
		Location adjPos;
		Object beside;
		for (int i = 0; i < allDirections.length; i++) {
			adjPos = new Location(current, allDirections[i]);
			if (world.isValidLocation(adjPos)) {
				beside = world.getThing(adjPos);
				if (beside instanceof Rabbit) {
					return allDirections[i];
				}
			}
		}
		return null;
	}

	/**
	 * Provides the brain of the Fox. The details are explained above the class.
	 * It returns a command for the Fox to execute
	 * 
	 * @param world
	 *            is a valid world
	 * @param actor
	 *            is valid and can exist in the world
	 */
	public Command act(World world, Actor actor) {
		if (world == null) {
			throw new NullPointerException("World can't be null");
		}
		if (actor == null) {
			throw new NullPointerException("Actor can't be null");
		}
		Direction[] allDirections = Direction.values(); // all directions to
														// face
		Location current = world.getLocation(actor); // current location of fox
		FoxImpl fox_actor = (FoxImpl) actor;
		int range = actor.getViewRange(); // view range of rabbit
		Location[] boundary = viewBounds(current, range, world);
		Direction hunt_dir = shouldChase(boundary, world, current); // direction
																	// towards
																	// nearest
																	// rabbit
		int energy = fox_actor.getEnergy(); // get fox's current energy
		int max_energy = fox_actor.getMaxEnergy(); // get fox's max energy
		int breed_limit = fox_actor.getBreedLimit(); // get fox's breed limit
		Location adjPos; // Location adjacent to fox's current position
		Object beside; // Get thing beside fox
		Direction eatRabbit;

		for (int i = 0; i < allDirections.length; i++) {
			adjPos = new Location(current, allDirections[i]);
			if (world.isValidLocation(adjPos)) {
				beside = world.getThing(adjPos);
				eatRabbit = getRabbit(current, world);
				// Eat rabbit if beside fox
				if (eatRabbit != null && energy < max_energy) {
					return new EatCommand(eatRabbit);
				}
				// Go towards rabbit
				else if (hunt_dir != null) {
					return new MoveCommand(hunt_dir);
				}
				// Breed if have enough energy and empty space
				else if (energy >= breed_limit && beside == null) {
					return new BreedCommand(allDirections[i]);
				}
			}
		}
		int dir = (int) (Math.random() * allDirections.length);
		return new MoveCommand(allDirections[dir]);
	}

	// end
}
