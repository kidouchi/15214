package edu.cmu.cs.cs214.hw2.staff.interfaces;

import edu.cmu.cs.cs214.hw2.staff.util.Direction;
import edu.cmu.cs.cs214.hw2.staff.util.Location;
import static edu.cmu.cs.cs214.hw2.staff.util.Direction.EAST;
import static edu.cmu.cs.cs214.hw2.staff.util.Direction.NORTH;
import static edu.cmu.cs.cs214.hw2.staff.util.Direction.SOUTH;
import static edu.cmu.cs.cs214.hw2.staff.util.Direction.WEST;

public abstract class AbstractAI {

	public AbstractAI() {
	}

	/**
	 * Returns all the locations that it can see (within view range)
	 * 
	 * @param current
	 *            is a valid location in the world
	 * @param view
	 *            is the range the animal can see
	 * @return an array containing all locations within view of animal
	 */
	public Location[] viewBounds(Location current, int view, World world) {
		if (!world.isValidLocation(current)) {
			throw new IllegalArgumentException("Invalid Location");
		}
		int x = current.getX();
		int y = current.getY();
		Location[] bounds = new Location[world.getWidth() * world.getHeight()];
		int bounds_num = 0;
		int start_view = 1;
		int max_view = view;
		Location check;
		if (view == 0) {
			return new Location[0];
		}
		while (start_view < max_view) {
			
			for (int i = 1; i < 2 * view; i++) {
				check = new Location(x - view, y - view + i);
				if (world.isValidLocation(check)) {
					bounds[bounds_num] = check;
					bounds_num++;
				}
			}
			for (int j = 1; j < 2 * view; j++) {
				check = new Location(x - view + j, y + view);
				if (world.isValidLocation(check)) {
					bounds[bounds_num] = check;
					bounds_num++;
				}
			}
			for (int k = 1; k < 2 * view; k++) {
				check = new Location(x + view, y + view - k);
				if (world.isValidLocation(check)) {
					bounds[bounds_num] = check;
					bounds_num++;
				}
			}
			for (int l = 1; l < 2 * view; l++) {
				check = new Location(x + view - l, y - view);
				if (world.isValidLocation(check)) {
					bounds[bounds_num] = check;
					bounds_num++;
				}
			}
			start_view++;
		}
		Location[] new_bounds = new Location[bounds_num];
		for (int j = 0; j < new_bounds.length; j++) {
			new_bounds[j] = bounds[j];
		}
		return new_bounds;
	}

	/**
	 * Checks whether the animal should die
	 * 
	 * @param energy
	 *            is an integer >= 0
	 * @return boolean
	 */
	public boolean shouldDie(int energy) {
		if (energy == 0) {
			return true;
		}
		return false;
	}

	/**
	 * Gives the opposite direction of what's given
	 * 
	 * @param dir
	 *            is a valid direction
	 * @return Direction
	 */
	public Direction oppositeDir(Direction dir) {
		if (dir == EAST) {
			return WEST;
		} else if (dir == WEST) {
			return EAST;
		} else if (dir == NORTH) {
			return SOUTH;
		}
		return NORTH;
	}

}
