package edu.cmu.cs.cs214.hw3;

import static org.junit.Assert.*;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AvlTreeSetTest {
	private AvlTreeSet mTestTree;

	/** Called before each test case method. */
	@Before
	public void setUp() throws Exception {
		// Start each test case method with a brand new AvlTreeSet object.
		mTestTree = new AvlTreeSet();
	}

	/** Called after each test case method. */
	@After
	public void tearDown() throws Exception {
		// Don't need to do anything here.
	}

	/** Tests that an empty tree has size 0. */
	@Test
	public void testEmptyTreeSize() {
		// First argument is the expected value.
		// Second argument is the actual returned value.
		assertEquals(0, mTestTree.size());
	}

	/**
	 * Test for tree size of 1 Bug: For the node constructor (w/ args), it
	 * didn't properly update the size - Failed to add the node and the left and
	 * right tree's nodes
	 */
	@Test
	public void testTreeSizeOne() {
		mTestTree.insert(1);

		assertEquals(1, mTestTree.size());
	}

	/** Just making sure that it returns right height of AVL tree */
	@Test
	public void testHeight() {
		assertEquals(-1, mTestTree.getHeight()); // Case of no elements in AVL
													// Tree
		mTestTree.insert(2);
		assertEquals(0, mTestTree.getHeight()); // Case of 1 element
		mTestTree.insert(3);
		assertEquals(1, mTestTree.getHeight()); // Case of 2 elements
		mTestTree.insert(1);
		assertEquals(1, mTestTree.getHeight()); // Case of 3 elements
		mTestTree.insert(4);
		assertEquals(2, mTestTree.getHeight()); // Case of 4 elements
	}

	/** Test for tree size of 2, just in case */
	@Test
	public void testTreeSizeTwo() {
		mTestTree.insert(1);
		mTestTree.insert(2);
		assertEquals(2, mTestTree.size());
		assertEquals(1, mTestTree.getHeight());
	}

	/** Test for general functions of insert */
	@Test
	public void testInsert() {
		mTestTree.insert(1);
		mTestTree.insert(2);
		assertEquals(1, mTestTree.getHeight());
		assertEquals(2, mTestTree.getMax());
		assertEquals(1, mTestTree.getMin());
		mTestTree.insert(3);
		assertEquals(1, mTestTree.getHeight());
		mTestTree.insert(4);
		assertEquals(2, mTestTree.getHeight());
	}

	/**
	 * Test insert doesn't add duplicates Bug: Insert didn't handle duplicate
	 * cases and did originally add duplicates to tree
	 */
	@Test
	public void testInsertDup() {
		mTestTree.insert(1);
		mTestTree.insert(1);
		assertEquals(1, mTestTree.size());
	}

	/** Test that remove throws an error when tree is empty */
	@Test(expected = IllegalStateException.class)
	public void testRemoveExcep() {
		mTestTree.remove(1);
	}

	/** Test that remove throws error when deleting nonexistent value */
	@Test(expected = IllegalStateException.class)
	public void testRemoveExcepTwo() {
		mTestTree.insert(2);
		mTestTree.remove(10);
	}

	/** Testing case of removing a value twice (Y'know, because why not) */
	@Test(expected = IllegalStateException.class)
	public void testRemoveTwice() {
		mTestTree.insert(1);
		mTestTree.insert(2);
		mTestTree.remove(1);
		mTestTree.remove(1);
	}

	/** Test general functions of remove */
	@Test
	public void testRemove() {
		mTestTree.insert(2);
		mTestTree.insert(1);
		mTestTree.insert(3);

		mTestTree.remove(1);
		assertEquals(2, mTestTree.getMin());
		assertEquals(1, mTestTree.getHeight());

		mTestTree.remove(2);
		assertEquals(3, mTestTree.getMax());
		assertEquals(0, mTestTree.getHeight());
	}

	/**
	 * Mostly for coverage purposes Test case when removing node with right
	 * subtree empty
	 */
	@Test
	public void testRemoveRight() {
		mTestTree.insert(3);
		mTestTree.insert(4);
		mTestTree.insert(5);
		mTestTree.insert(1);
		mTestTree.remove(3);
	}

	/** Test case when removing that node's subtrees aren't empty */
	@Test
	public void testRemoveBoth() {
		mTestTree.insert(2);
		mTestTree.insert(1);
		mTestTree.insert(3);
		mTestTree.remove(2);
	}

	/**
	 * Checks isEmpty function and whether it returns true when the tree is
	 * empty or false o/w
	 */
	@Test
	public void testIsEmpty() {
		mTestTree.insert(1);
		assertEquals(false, mTestTree.isEmpty());
		mTestTree.remove(1);
		assertEquals(true, mTestTree.isEmpty());
	}

	/**
	 * Checks the contains function returns the right boolean 
	 * Bug: The contains function failed to traverse the right side of the tree
	 */
	@Test
	public void testContains() {
		mTestTree.insert(1);
		mTestTree.insert(2);
		mTestTree.insert(3);
		mTestTree.insert(5);
		mTestTree.insert(7);

		assertEquals(true, mTestTree.contains(3));
		assertEquals(false, mTestTree.contains(8));
	}

	/** Checks the getMax function gives the max value in the AVL tree */
	@Test
	public void testMax() {
		mTestTree.insert(1);
		mTestTree.insert(3);
		mTestTree.insert(5);
		mTestTree.insert(7);
		assertEquals(7, mTestTree.getMax());
		mTestTree.remove(7);
		assertEquals(5, mTestTree.getMax());
	}

	/** Test that getMax throws exception when given empty AVL tree */
	@Test(expected = IllegalStateException.class)
	public void testMaxExcep() {
		mTestTree.getMax();
	}

	/** Checks the getMin function gives the min value in the AVL tree */
	@Test
	public void testMin() {
		mTestTree.insert(1);
		mTestTree.insert(2);
		mTestTree.insert(3);
		mTestTree.insert(5);
		mTestTree.insert(7);
		assertEquals(1, mTestTree.getMin());
		mTestTree.remove(1);
		assertEquals(2, mTestTree.getMin());
	}

	/** Test whether getMin throws exception when given empty tree */
	@Test(expected = IllegalStateException.class)
	public void testMinExcep() {
		mTestTree.getMin();
	}

	/** Try to test general functions of singleRotateLeft function */
	@Test
	public void testSingleRotateLeft() {
		mTestTree.insert(1);
		mTestTree.insert(2);
		mTestTree.insert(3);
		assertEquals(1, mTestTree.getHeight());
		mTestTree.insert(5);
		mTestTree.insert(7);
		assertEquals(2, mTestTree.getHeight());
	}

	/** Try to test general functions of doubleRotateleft */
	public void testDoubleRotateLeft() {
		mTestTree.insert(3);
		mTestTree.insert(1);
		mTestTree.insert(4);
		mTestTree.insert(2);
		mTestTree.insert(5);
		assertEquals(2, mTestTree.getHeight());
		mTestTree.insert(6);
		assertEquals(2, mTestTree.getHeight());
		mTestTree.remove(3);
		assertEquals(2, mTestTree.getHeight());
	}

	/** Try to test general function of singleRotateRight */
	public void testSingleRotateRight() {
		mTestTree.insert(2);
		mTestTree.insert(1);
		mTestTree.insert(5);
		mTestTree.insert(4);
		mTestTree.insert(3);
		assertEquals(2, mTestTree.getHeight());
	}

	/** Try to test general function of doubleRotateRight */
	@Test
	public void testDoubleRotateRight() {
		mTestTree.insert(3);
		mTestTree.insert(4);
		mTestTree.insert(5);
		mTestTree.insert(1);
		mTestTree.insert(2);
	}

}
