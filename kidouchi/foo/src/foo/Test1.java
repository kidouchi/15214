package foo;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;

public class Test1 {
	
	public Test1() {
		
	}

	public void scrabbleDict() throws Exception{
		String str_line;
		FileInputStream in = new FileInputStream("scrabble_words.txt");
		DataInputStream data_in = new DataInputStream(in);
		BufferedReader br = new BufferedReader(
								new InputStreamReader(data_in));
		
		while((str_line = br.readLine()) != null) {
			str_line = str_line.trim();
				System.out.println(str_line);
		}
	}
	
	
}
