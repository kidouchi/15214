hw0 Feedback
============

#### Submitting a compiling solution (20/20)

#### Working `getDistance` implementation (7/10)
-3: Failed two of our test cases

#### Fulfilling technical requirements (9/10)
-1: remember to use access modifiers (public, private, protected) in front of your class variables and methods

#### Documentation and code style (10/10)


#### Total (46/50)

Late days used: 0 (5 left)

---

#### Additional Notes

Graded by: Beth Anne Katz (bkatz@andrew.cmu.edu)

To view this file with formatting, visit the following page: <https://github.com/CMU-15-214/kidouchi/blob/master/grades/hw0.md>
