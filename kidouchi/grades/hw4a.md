hw4a Feedback
============

**IMPORTANT:** If we ask any questions about your design, briefly respond to them in a file called `{your repo}/homework/4/response.pdf`. This will count as part of your Milestone B grade!

#### Questions from the TA

 * Have you thought about other methods by which the GUI can send updates to the `Game` (as opposed to setting a new `Tile[][]` each time a new move is made)?
 
 * How do you plan on implementing `SpecialTile`s so that they can make modifications to the game's state when activated?

#### Design Model (23/30)

* -3, Unclear how the `SpecialTile`s will be able to make modifications to the game state. For example, how would a `BoomTile` remove some of the game board's tiles? How could a `ReverseTurnTile` modify the order in which turns are made?

* -2, It is a little odd that the `MorePointsTile`s are instances of the `Tile` interface. This implies that it is possible for the `Player` to hold a `MorePointsTile` as part of their tile rack, when in fact the `MorePointsTile`s are really just part of the board. I might be better to think about `Tile`s as things that a player can place on a board, and the double/triple letter/word score stuff as part of the board's implementation instead.

* -1, There's no need to create separate classes for `TripleWord` and `DoubleWord` (and same thing for `TripleLetter` and `DoubleLetter`). Just use an `int` multiplier (i.e. either `2` or `3`) instead!

* -1, Unclear about the purpose of the `drawTile()` method in the `Player` class. Do you mean `addTile(Tile) : void` or something like that?

* -0.1, It will probably be inefficient to set a new `Tile[][]` array for each new move that is made. Efficiency-wise, it would make more sense to update the board in place or something like that.

#### Rationale (10/10)

* Awesome! Thanks for all of the detailed descriptions! :)

#### Interfaces and Class Stubs (10/10)

---

#### Total (43/50)

Late days used: 0 (4 left)

---

#### Additional Notes

* Great job separating the `Game` and `Referee` classes!

Graded by: Alex Lockwood (alockwoo@andrew.cmu.edu)

To view this file with formatting, visit the following page: https://github.com/CMU-15-214/kidouchi/blob/master/grades/hw4a.md
