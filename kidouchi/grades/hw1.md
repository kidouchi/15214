hw1 Feedback
============

#### Correct Graph Implementations (35/35)
 
 * Great job! All tests passed.

#### Correct Algorithm Implementations (40/40)

 * All tests passed!

#### Good Style and Program Design (13/15)

 * Good comments.
 * -2 Use getters and setters for fields in your Tuple and Node classes instead of having them as default package private.

#### Miscellaneous (10/10)

---

#### Total (98/100)

Late days used: 0 (5 left)

---

#### Additional Notes

Graded by: Daniel Lu (dylu)

To view this file with formatting, visit the following page: https://github.com/CMU-15-214/kidouchi/tree/master/grades/hw1.md
