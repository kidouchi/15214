hw3 Feedback
============

#### `AvlTreeSet` Code Coverage (19/25)

 * -6, only 93.9% coverage--you didn't test the situation with `singleRotateRight()`

#### `AvlTreeSet` Bug Correction (15/15)

 * Hmm, you fixed things a little differently than how other people did it (mostly it was to change smaller stuff) but you accomplished the same things.

#### `AvlTreeSet` Class Invariants (15/15)

#### HW1 Code Coverage (30/30)

#### Documentation & Style (15/15)

---

#### Total (94/100)

Late days used: 0 (4 left)

---

#### Additional Notes

 * Your project initially didn't compile for me because you had two `AvlTreeSet` classes defined... It's because you left the `AvlTreeSet_orig.java` file in there. Next time you should make sure to remove it when you're done.

Graded by: Shannon Lee (sjl1@andrew.cmu.edu)

To view this file with formatting, visit the following page: https://github.com/CMU-15-214/kidouchi/blob/master/grades/hw3.md