hw5a Feedback
=============

### Framework Description (10/10)

---

### Design Model (35/35)

#### Design Structure (15/15)

#### Framework/Plugin Interaction (15/15)

#### Style (5/5)

---

### Presentation (30/30)

#### Description of Framework (10/10)

#### Talk Quality (10/10)

#### Timing (10/10)

---

### Additional Notes
Everything looks good :) Nice job!

---

### Total (75/75)

---

Graded by: Beth Anne Katz (bkatz)

To view this file with formatting, visit the following page: https://github.com/CMU-15-214/STUDENT_ANDREW_ID/blob/master/grades/hw5a.md
