hw4c Feedback
=============

#### Separation of GUI and Core (20/20)

 * My one suggestion is to make more use of the `GameChangeListener` pattern that we have been using in recitation so much. For example, if you look at the recitation 8 & 10 sample solutions, you'll notice that the `ActionListener`s are extremely small in length (i.e. basically one line). In your code, some of the `actionPerformed()` methods are extremely large (such as [this](https://github.com/CMU-15-214/kidouchi/blob/master/homework/4/src/edu/cmu/cs/cs214/hw4/gui/MoveListener.java#L106-L173) one). It would be better if you had kept the "event", the "analysis", and the "GUI response" separate from one another. See the follow-up discussion in [this Piazza post](https://piazza.com/class/hkqe3rh2nlz35d?cid=681) for an explanation of what I'm talking about and why it would be useful. 

#### GUI Implementation (5/10)

* -5, Your GUI sometimes throws `NullPointerException`s, causing the program to crash, when I place tiles onto the board.

#### Special Tiles (10/10)

#### Game Play (10/10)

#### Information Display (10/10)

#### Style (11/15)

 * -2, `RuntimeExceptions` (i.e. "unchecked exceptions") like [these](https://github.com/CMU-15-214/kidouchi/blob/master/homework/4/src/edu/cmu/cs/cs214/hw4/gui/MoveListener.java#L128-L135) are meant to signal _programmer_ errors (i.e. the programmer who wrote the code), not _user_ errors. To signal that the user has made an invalid move, you should be throwing and catching checked  exception instead. You rarely (if ever) want to catch `RuntimeException`s such as `IllegalArgumentException` and `IllegalStateException`.

 * -1, [These](https://github.com/CMU-15-214/kidouchi/blob/master/homework/4/src/edu/cmu/cs/cs214/hw4/gui/MoveListener.java#L29-L33) and [these](https://github.com/CMU-15-214/kidouchi/blob/master/homework/4/src/edu/cmu/cs/cs214/hw4/gui/PlayerPanel.java#L28-L34) should all be `static final` (do you see why?).

* -1, Variables should start with lowercase letters and should be `camelCase`. ([link](https://github.com/CMU-15-214/kidouchi/blob/master/homework/4/src/edu/cmu/cs/cs214/hw4/gui/PlayerPanel.java#L37-L38))

#### Discussion (25/25)

 * Thanks for referencing me in your discussion, hehe. I agree with your changes and think it's gotten a lot better since! :)

#### Points back for hw4a

 * Thanks for providing both the old and new design models... makes it much easier for me to give you back your points. :P

 * 7 points * 0.75 = 5.25 points
 * New hw4a grade: 48.25/50

---

#### Total (91/100)

Late days used: 2 (1 left)

---

#### Additional Notes

Graded by: Alex Lockwood (alockwoo@andrew.cmu.edu)

To view this file with formatting, visit the following page: https://github.com/CMU-15-214/kidouchi/blob/master/grades/hw4c.md
