hw4b Feedback
============

#### Response to questions from TA (25/25)

 * No need to be sorry. Sounds good to me. :)

#### Implementation of core game features (40/50)

 * -3, Avoid hard coding strings into your code like [this](https://github.com/CMU-15-214/kidouchi/blob/142eff9cb32ca60f51924f57fc66c0e905193ded/homework/4/src/edu/cmu/cs/cs214/hw4/core/Game.java#L214-L229) and [this](https://github.com/CMU-15-214/kidouchi/blob/142eff9cb32ca60f51924f57fc66c0e905193ded/homework/4/src/edu/cmu/cs/cs214/hw4/core/Game.java#L318-L329). Instead, reference them as `static final String` constants:

    ```java
    // Declare these constants public if they need to be referenced
    // from outside the class.
    public static final String NEGATE_TILE = "negate";
    public static final String START_AT_ZERO_TILE = "start_at_zero";
    public static final String EXCHANGE_TILE = "exchange";
        
    ...
        
    if (name.equals(NEGATE_TILE)) {
        ...
    } else if (name.equals(START_AT_ZERO_TILE)) {
        ...
    } else if (name.equals(EXCHANGE)) {
        ...
    }
    ```

    The idea is that writing the code in this way will make your code much easier to manage in the long run (i.e. making it less vulnerable to accidental typos/bugs, etc.). This is standard practice in industry. That said, I don't think you want to be casing on `String` constants like this anyway (see below).
    
 * -5, Avoid casing on special tile types using `String`s (like you do [here](https://github.com/CMU-15-214/kidouchi/blob/142eff9cb32ca60f51924f57fc66c0e905193ded/homework/4/src/edu/cmu/cs/cs214/hw4/core/Game.java#L214-L229)). This makes it difficult to add new types of special tiles in the future. It seems like you can just call the `SpecialTile`'s `specialAction()` method directly? Why is casting to the special tile's concrete subclass type necessary?

 * -2, It seems like passing in a `Tile[] tiles` argument to your [`Game#makeMove()`](https://github.com/CMU-15-214/kidouchi/blob/142eff9cb32ca60f51924f57fc66c0e905193ded/homework/4/src/edu/cmu/cs/cs214/hw4/core/Game.java#L177) method is making your code more messy than it needs to be. I would expect that the array would be able to hold both `SpecialTile`s and `LetterTile`s since you have declared it as `Tile[]`. However, you check in your code whether or not [`tiles instanceof LetterTile[]`](https://github.com/CMU-15-214/kidouchi/blob/142eff9cb32ca60f51924f57fc66c0e905193ded/homework/4/src/edu/cmu/cs/cs214/hw4/core/Game.java#L198), which makes it a little unclear what type of elements this argument is supposed to contain. What if, for example, you passed in a `Tile[] tiles = new Tile[]` that was filled with only `LetterTile`s? Then the method would throw an `IllegalArgumentException`, but it isn't clear from the method signature that this isn't allowed since the argument has type `Tile[]`.
 
   Overall, my suggestion is to try to break up the `makeMove()` method into separate independent methods (i.e. at the very least you could make methods like `makePassMove()`, `placeSpecialTiles`, etc. instead of combining all of that functionality into a single `makeMove()` method). I also recommend reading [Item 25: Prefer lists to arrays](https://drive.google.com/file/d/0B-5-QeQdT3FxSlk3SWRJRTctdGM/edit?usp=sharing) in Effective Java. Not to say that you shouldn't use arrays in your code at all, but I think it could help certain aspects of your implementation if you did use `List`s instead of arrays.

#### Tests for the core implementation (15/15)

 * Great tests!

#### Javadoc comments and style (7/10)

 * -1, Avoid hardcoding magic numbers like `7` ([link](https://github.com/CMU-15-214/kidouchi/blob/142eff9cb32ca60f51924f57fc66c0e905193ded/homework/4/src/edu/cmu/cs/cs214/hw4/core/Game.java#L280)) into your code directly. Instead declare a `static final int NUM_RACK_TILES = 7;` somewhere in your code and refer to the variable instead.

 * -1, Method names should start with lower case letters. ([link](https://github.com/CMU-15-214/kidouchi/blob/142eff9cb32ca60f51924f57fc66c0e905193ded/homework/4/src/edu/cmu/cs/cs214/hw4/core/Referee.java#L202))
 
 * -1, Variable names should be `camelCase`. ([link](https://github.com/CMU-15-214/kidouchi/blob/142eff9cb32ca60f51924f57fc66c0e905193ded/homework/4/src/edu/cmu/cs/cs214/hw4/core/Referee.java#L34))

 * -0.1, [This](https://github.com/CMU-15-214/kidouchi/blob/142eff9cb32ca60f51924f57fc66c0e905193ded/homework/4/src/edu/cmu/cs/cs214/hw4/core/ExchangeLetterTile.java#L12) should be declared `private static final int COST = 10`. And similarly for [this](https://github.com/CMU-15-214/kidouchi/blob/142eff9cb32ca60f51924f57fc66c0e905193ded/homework/4/src/edu/cmu/cs/cs214/hw4/core/StartAtZeroTile.java#L6).

---

#### Total (87/100)

Late days used: 1 (3 left)

---

#### Additional Notes

Graded by: Alex Lockwood (alockwoo@andrew.cmu.edu)

To view this file with formatting, visit the following page: https://github.com/CMU-15-214/kidouchi/blob/master/grades/hw4b.md
