hw2 Feedback
============

#### Correct working Rabbit and Fox implementations with working AIs (50/50)

Wahoo! Works!

#### Completion of Third Animal(10/10)

-3: your alien does not move

#### Design (30/30)

Why did you put the AbstractAI class inside of the staff.interfaces package?  I feel like it should be in hw2.ai package.

Good design :) Nice reuse of code.

#### Documentation and Style (10/10)

Please do not comment like this:
<https://github.com/CMU-15-214/kidouchi/blob/5ee6e61ed86bca46e2aecdd137123c104c615fb7/homework/2/src/edu/cmu/cs/cs214/hw2/ai/FoxAI.java#L98-L99>

Single line comments should go on their own line


Is this neccessary?

<https://github.com/CMU-15-214/kidouchi/blob/5ee6e61ed86bca46e2aecdd137123c104c615fb7/homework/2/src/edu/cmu/cs/cs214/hw2/ai/FoxAI.java#L138>

---

#### Total (100/100)

Late days used: 0 (4 left)

---

#### Additional Notes

Graded by: mhgray@andrew.cmu.edu (Mathew Gray)

To view this file with formatting, visit the following page: https://github.com/CMU-15-214/kidouchi/blob/master/grades/hw2.md
