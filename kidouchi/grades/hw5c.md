hw5c Feedback
=============

### Experience Report (10/10)

---

### Plugin Implementation (47/50)

### Style (15/15)
*-2, make sure to comment public methods
*-1, don't case on strings 

---

### Additional Notes

---

### Total: (72/75)

---

Graded by: Beth Anne Katz (bkatz@andrew.cmu.edu)

To view this file with formatting, visit the following page: https://github.com/CMU-15-214/azeng/blob/master/grades/hw5c.md
