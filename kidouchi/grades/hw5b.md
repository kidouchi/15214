hw5b Feedback
=============

### Framework Implementation (60/60)

#### Meets requirements (20/20)

#### Control flow (20/20)

#### Gathered Twitter data (10/10)

#### Framework design (10/10)

---

### Sample Plug-ins (20/20)

---

### Documentation (10/10)

### Style (10/10)
* -1, Please remove commented out code, your submissions should be finalized 

---

### Additional Notes

---

### Total: (100/100)

---

Graded by: Beth Anne Katz (bkatz@andrew.cmu.edu)

To view this file with formatting, visit the following page: https://github.com/CMU-15-214/STUDENT_ANDREW_ID/blob/master/grades/hw5b.md
