hw6 Feedback
============

#### `AbstractClient` Implementation (3/5)
+ -2, Does not write the final file locations back to the `AbstractClient`

#### `WorkerServer` Implementation (5/5)

#### `MasterServer` Implementation (10/10)

#### Map/Shuffle/Reduce Phases (18/20)
+ -2 Exceptions were thrown because reset was called on `ObjectOutputStream`
    - `MapCallable.java` line 41
    - `ReduceCallable.java` line 42

#### Correctness (10/10)

#### Robustness (0/10)
+ -10 Robuestness not implemented

#### `WordPrefixMapTask` Implementation (9/10)
+ -1 Concatenating strings in a loop is O(n^2), it would be better to use
`String#substring` or a `StringBuilder`

#### `WordPrefixReduceTask` Implementation (10/10)

#### Documentation & Style (10/10)

#### FindBugs Extra Credit
+ +10 points (0 bugs found)

---

#### Total score, including extra credit: (85/90)

Late days used: 1 (0 left)

---

#### Additional Notes

 * Please consider leaving feedback for your TA this semester! We would really
 * appreciate it! Thanks!

 * TA feedback (by Dec 19): https://www.ugrad.cs.cmu.edu/ta/F13/feedback

---

Graded by: bcforres (bcforres@andrew.cmu.edu)

To view this file with formatting, visit the following page:
https://github.com/CMU-15-214/STUDENT_ANDREW_ID/blob/master/grades/hw6.md
